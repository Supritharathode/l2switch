# CMake system name must be something like "Linux".
# This is important for cross-compiling.
set( CMAKE_SYSTEM_NAME Linux )
set( CMAKE_SYSTEM_PROCESSOR powerpc )
set( CMAKE_C_COMPILER powerpc-fsl-linux-gcc )
set( CMAKE_CXX_COMPILER powerpc-fsl-linux-g++ )
set( CMAKE_C_FLAGS " -m32 -mhard-float -mcpu=e5500  --sysroot=/home/sirius/yocto/poky/build_t1040rdb_release/tmp/sysroots/t1040rdb  -O2 -pipe -g -feliminate-unused-debug-types" CACHE STRING "CFLAGS" )
set( CMAKE_CXX_FLAGS " -m32 -mhard-float -mcpu=e5500  --sysroot=/home/sirius/yocto/poky/build_t1040rdb_release/tmp/sysroots/t1040rdb  -O2 -pipe -g -feliminate-unused-debug-types -fvisibility-inlines-hidden -fpermissive" CACHE STRING "CXXFLAGS" )
set( CMAKE_C_FLAGS_RELEASE "-O2 -pipe -g -feliminate-unused-debug-types  -O2 -pipe -g -feliminate-unused-debug-types -DNDEBUG" CACHE STRING "CFLAGS for release" )
set( CMAKE_CXX_FLAGS_RELEASE "-O2 -pipe -g -feliminate-unused-debug-types  -O2 -pipe -g -feliminate-unused-debug-types -fvisibility-inlines-hidden -DNDEBUG" CACHE STRING "CXXFLAGS for release" )
set( CMAKE_C_LINK_FLAGS " -m32 -mhard-float -mcpu=e5500  --sysroot=/home/sirius/yocto/poky/build_t1040rdb_release/tmp/sysroots/t1040rdb  -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed" CACHE STRING "LDFLAGS" )
set( CMAKE_CXX_LINK_FLAGS " -m32 -mhard-float -mcpu=e5500  --sysroot=/home/sirius/yocto/poky/build_t1040rdb_release/tmp/sysroots/t1040rdb  -O2 -pipe -g -feliminate-unused-debug-types -fvisibility-inlines-hidden -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed" CACHE STRING "LDFLAGS" )

# only search in the paths provided so cmake doesnt pick
# up libraries and tools from the native build machine
set( CMAKE_FIND_ROOT_PATH /home/sirius/yocto/poky/build_t1040rdb_release/tmp/sysroots/t1040rdb /home/sirius/yocto/poky/build_t1040rdb_release/tmp/sysroots/x86_64-linux    )
set( CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY )
set( CMAKE_FIND_ROOT_PATH_MODE_PROGRAM ONLY )
set( CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY )
set( CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY )

# Use qt.conf settings
set( ENV{QT_CONF_PATH} /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/qt.conf )

# We need to set the rpath to the correct directory as cmake does not provide any
# directory as rpath by default
set( CMAKE_INSTALL_RPATH  )

# Use native cmake modules
set( CMAKE_MODULE_PATH /home/sirius/yocto/poky/build_t1040rdb_release/tmp/sysroots/t1040rdb/usr/share/cmake/Modules/ )

# add for non /usr/lib libdir, e.g. /usr/lib64
set( CMAKE_LIBRARY_PATH /usr/lib /lib)

