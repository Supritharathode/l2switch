/*

 Vitesse API software.

 Copyright (c) 2002-2014 Vitesse Semiconductor Corporation ("Vitesse"). All
 Rights Reserved. Unpublished rights reserved under the copyright laws of the
 United States of America, other countries and international treaties.

 Freescale Semiconductor ("Freescale") is distributing this software under
 limited license rights from Vitesse.

 Permission to use, copy, store, modify, integrate into other products,
 disclose, transmit and distribute ("Use"), and any warranties (if any)
 relating to, the software are governed by an applicable license agreement
 with Freescale. This copyright notice must appear in any copy, modification,
 disclosure, transmission or distribution of the software. Vitesse retains all
 ownership, copyright, trade secret and proprietary rights in the software.
 
 $Id$
 $Revision$

*/

/**
 * \file
 * \brief HQoS API
 * \details This header file describes Hierarchical Quality of Service (HQoS) functions
 *
 * HQoS is enabled on a port when the port scheduling mode is set to hierarchical.
 *
 * HQoS parameters are configured using HQoS IDs.
 *
 * Traffic can be mapped to HQoS IDs by using the corresponding modules.
 */

#ifndef _VTSS_HQOS_API_H_
#define _VTSS_HQOS_API_H_

#include <vtss/api/types.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif
#endif /* _VTSS_HQOS_API_H_ */
