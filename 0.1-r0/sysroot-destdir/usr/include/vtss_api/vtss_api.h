/*

 Vitesse API software.

 Copyright (c) 2002-2014 Vitesse Semiconductor Corporation ("Vitesse"). All
 Rights Reserved. Unpublished rights reserved under the copyright laws of the
 United States of America, other countries and international treaties.

 Freescale Semiconductor ("Freescale") is distributing this software under
 limited license rights from Vitesse.

 Permission to use, copy, store, modify, integrate into other products,
 disclose, transmit and distribute ("Use"), and any warranties (if any)
 relating to, the software are governed by an applicable license agreement
 with Freescale. This copyright notice must appear in any copy, modification,
 disclosure, transmission or distribution of the software. Vitesse retains all
 ownership, copyright, trade secret and proprietary rights in the software.

 $Id$
 $Revision$

*/

/**
 * \file
 * \brief Vitesse API main header file
 * \details This is the only header file which must be included by the application
 */

#ifndef _VTSS_API_H_
#define _VTSS_API_H_

#include <vtss/api/options.h>

#include <vtss_os.h>

#include <vtss/api/types.h>
#include <vtss_init_api.h>

#include <vtss_misc_api.h>

#include <vtss_port_api.h>

#include <vtss_phy_api.h>

#include <vtss_qos_api.h>

#include <vtss_packet_api.h>

#include <vtss_security_api.h>
#include <vtss_l2_api.h>


#include <vtss_evc_api.h>

#endif /* _VTSS_API_H_ */
