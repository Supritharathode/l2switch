/*

 Vitesse API software.

 Copyright (c) 2002-2014 Vitesse Semiconductor Corporation ("Vitesse"). All
 Rights Reserved. Unpublished rights reserved under the copyright laws of the
 United States of America, other countries and international treaties.

 Freescale Semiconductor ("Freescale") is distributing this software under
 limited license rights from Vitesse.

 Permission to use, copy, store, modify, integrate into other products,
 disclose, transmit and distribute ("Use"), and any warranties (if any)
 relating to, the software are governed by an applicable license agreement
 with Freescale. This copyright notice must appear in any copy, modification,
 disclosure, transmission or distribution of the software. Vitesse retains all
 ownership, copyright, trade secret and proprietary rights in the software.

 $Id$
 $Revision$

*/

#ifndef _VTSS_L2_TYPES_H_
#define _VTSS_L2_TYPES_H_

#include <vtss/api/types.h>

/**
 * \file
 * \brief Layer 2 Public API Header for l2
 * \details This header file describes public Layer 2 datatypes
 */

/** \brief Aggregation traffic distribution mode */
typedef struct
{
    BOOL smac_enable;            /**< Source MAC address */
    BOOL dmac_enable;            /**< Destination MAC address */
    BOOL sip_dip_enable;         /**< Source and destination IP address */
    BOOL sport_dport_enable;     /**< Source and destination UDP/TCP port */
} vtss_aggr_mode_t;

/**
 * \brief sFlow sampler type.
 *
 * The API supports sampling ingress and egress separately, as well
 * as simultaneously.
 */
typedef enum {
    VTSS_SFLOW_TYPE_NONE = 0, /**< Sampler is not enabled on the port.                         */
    VTSS_SFLOW_TYPE_RX,       /**< Sampler is enabled for ingress on the port.                 */
    VTSS_SFLOW_TYPE_TX,       /**< Sampler is enabled for egress on the port.                  */
    VTSS_SFLOW_TYPE_ALL       /**< Sampler is enabled for both ingress and egress on the port. */
} vtss_sflow_type_t;

#endif /* #define _VTSS_L2_TYPES_H_ */
