/*

 Vitesse API software.

 Copyright (c) 2002-2014 Vitesse Semiconductor Corporation ("Vitesse"). All
 Rights Reserved. Unpublished rights reserved under the copyright laws of the
 United States of America, other countries and international treaties.

 Freescale Semiconductor ("Freescale") is distributing this software under
 limited license rights from Vitesse.

 Permission to use, copy, store, modify, integrate into other products,
 disclose, transmit and distribute ("Use"), and any warranties (if any)
 relating to, the software are governed by an applicable license agreement
 with Freescale. This copyright notice must appear in any copy, modification,
 disclosure, transmission or distribution of the software. Vitesse retains all
 ownership, copyright, trade secret and proprietary rights in the software.

 $Id$
 $Revision$

*/

/**
 * \file vtss_afi_api.h
 * \brief AFI API
 * \details This header file describes Automatic Frame Injector functions.
 */

#ifndef _VTSS_AFI_API_H_
#define _VTSS_AFI_API_H_

#endif // _VTSS_AFI_API_H_
