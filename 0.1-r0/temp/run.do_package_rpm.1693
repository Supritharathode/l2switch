
def do_package_rpm(d):
    # We need a simple way to remove the MLPREFIX from the package name,
    # and dependency information...
    def strip_multilib(name, d):
        ml = d.getVar("MLPREFIX", True)
        if ml and name and len(ml) != 0 and name.find(ml) >= 0:
            return "".join(name.split(ml))
        return name

    workdir = d.getVar('WORKDIR', True)
    tmpdir = d.getVar('TMPDIR', True)
    pkgd = d.getVar('PKGD', True)
    pkgdest = d.getVar('PKGDEST', True)
    if not workdir or not pkgd or not tmpdir:
        bb.error("Variables incorrectly set, unable to package")
        return

    packages = d.getVar('PACKAGES', True)
    if not packages or packages == '':
        bb.debug(1, "No packages; nothing to do")
        return

    # Construct the spec file...
    # If the spec file already exist, and has not been stored into
    # pseudo's files.db, it maybe cause rpmbuild src.rpm fail,
    # so remove it before doing rpmbuild src.rpm.
    srcname    = strip_multilib(d.getVar('PN', True), d)
    outspecfile = workdir + "/" + srcname + ".spec"
    if os.path.isfile(outspecfile):
        os.remove(outspecfile)
    d.setVar('OUTSPECFILE', outspecfile)
    bb.build.exec_func('write_specfile', d)

    perfiledeps = (d.getVar("MERGEPERFILEDEPS", True) or "0") == "0"
    if perfiledeps:
        outdepends, outprovides = write_rpm_perfiledata(srcname, d)

    # Setup the rpmbuild arguments...
    rpmbuild = d.getVar('RPMBUILD', True)
    targetsys = d.getVar('TARGET_SYS', True)
    targetvendor = d.getVar('TARGET_VENDOR', True)
    package_arch = (d.getVar('PACKAGE_ARCH', True) or "").replace("-", "_")
    if package_arch not in "all any noarch".split() and not package_arch.endswith("_nativesdk"):
        ml_prefix = (d.getVar('MLPREFIX', True) or "").replace("-", "_")
        d.setVar('PACKAGE_ARCH_EXTEND', ml_prefix + package_arch)
    else:
        d.setVar('PACKAGE_ARCH_EXTEND', package_arch)
    pkgwritedir = d.expand('/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/deploy-rpms/${PACKAGE_ARCH_EXTEND}')
    pkgarch = d.expand('${PACKAGE_ARCH_EXTEND}-fsl-linux')
    magicfile = d.expand('/home/sirius/yocto/poky/build_t1040rdb_release/tmp/sysroots/x86_64-linux/usr/share/misc/magic.mgc')
    bb.utils.mkdirhier(pkgwritedir)
    os.chmod(pkgwritedir, 0755)

    cmd = rpmbuild
    cmd = cmd + " --nodeps --short-circuit --target " + pkgarch + " --buildroot " + pkgd
    cmd = cmd + " --define '_topdir " + workdir + "' --define '_rpmdir " + pkgwritedir + "'"
    cmd = cmd + " --define '_build_name_fmt %%{NAME}-%%{VERSION}-%%{RELEASE}.%%{ARCH}.rpm'"
    cmd = cmd + " --define '_use_internal_dependency_generator 0'"
    if perfiledeps:
        cmd = cmd + " --define '__find_requires " + outdepends + "'"
        cmd = cmd + " --define '__find_provides " + outprovides + "'"
    else:
        cmd = cmd + " --define '__find_requires %{nil}'"
        cmd = cmd + " --define '__find_provides %{nil}'"
    cmd = cmd + " --define '_unpackaged_files_terminate_build 0'"
    cmd = cmd + " --define 'debug_package %{nil}'"
    cmd = cmd + " --define '_rpmfc_magic_path " + magicfile + "'"
    cmd = cmd + " --define '_tmppath " + workdir + "'"
    if d.getVarFlag('ARCHIVER_MODE', 'srpm', True) == '1' and bb.data.inherits_class('archiver', d):
        cmd = cmd + " --define '_sourcedir " + d.getVar('ARCHIVER_OUTDIR', True) + "'"
        cmdsrpm = cmd + " --define '_srcrpmdir " + d.getVar('ARCHIVER_OUTDIR', True) + "'"
        cmdsrpm = cmdsrpm + " -bs " + outspecfile
        # Build the .src.rpm
        d.setVar('SBUILDSPEC', cmdsrpm + "\n")
        d.setVarFlag('SBUILDSPEC', 'func', '1')
        bb.build.exec_func('SBUILDSPEC', d)
    cmd = cmd + " -bb " + outspecfile

    # Build the rpm package!
    d.setVar('BUILDSPEC', cmd + "\n")
    d.setVarFlag('BUILDSPEC', 'func', '1')
    bb.build.exec_func('BUILDSPEC', d)


do_package_rpm(d)
