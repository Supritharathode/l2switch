Summary: T1040 L2 Switch
Name: l2switch
Version: 0.1
Release: r0
License: Proprietary
Group: base
Packager: Freescale Yocto Team <yocto@linux.freescale.net>
BuildRequires: libtool-cross
BuildRequires: autoconf-native
BuildRequires: libtool-native
BuildRequires: virtual/powerpc-fsl-linux-compilerlibs
BuildRequires: update-rc.d-native
BuildRequires: initscripts
BuildRequires: gnu-config-native
BuildRequires: virtual/powerpc-fsl-linux-gcc
BuildRequires: virtual/libc
BuildRequires: cmake-native
BuildRequires: automake-native
Requires: rtld(GNU_HASH)
Requires: libc.so.6(GLIBC_2.7)
Requires: libc.so.6(GLIBC_2.1)
Requires: libc6 >= 2.15
Requires: libvtss-api0 >= 0.1
Requires: libc.so.6(GLIBC_2.0)
Requires: libc.so.6(GLIBC_2.3)
Requires: libvtss_api.so.0
Requires: /bin/sh
Requires: libc.so.6
Requires: libc.so.6(GLIBC_2.4)
Requires(pre): rtld(GNU_HASH)
Requires(pre): libc.so.6(GLIBC_2.7)
Requires(pre): libc.so.6(GLIBC_2.1)
Requires(pre): libc6 >= 2.15
Requires(pre): libvtss-api0 >= 0.1
Requires(pre): libc.so.6(GLIBC_2.0)
Requires(pre): libc.so.6(GLIBC_2.3)
Requires(pre): libvtss_api.so.0
Requires(pre): /bin/sh
Requires(pre): libc.so.6
Requires(pre): libc.so.6(GLIBC_2.4)
Requires(post): rtld(GNU_HASH)
Requires(post): libc.so.6(GLIBC_2.7)
Requires(post): libc.so.6(GLIBC_2.1)
Requires(post): libc6 >= 2.15
Requires(post): libvtss-api0 >= 0.1
Requires(post): libc.so.6(GLIBC_2.0)
Requires(post): libc.so.6(GLIBC_2.3)
Requires(post): libvtss_api.so.0
Requires(post): /bin/sh
Requires(post): libc.so.6
Requires(post): libc.so.6(GLIBC_2.4)
Requires(preun): rtld(GNU_HASH)
Requires(preun): libc.so.6(GLIBC_2.7)
Requires(preun): libc.so.6(GLIBC_2.1)
Requires(preun): libc6 >= 2.15
Requires(preun): libvtss-api0 >= 0.1
Requires(preun): libc.so.6(GLIBC_2.0)
Requires(preun): libc.so.6(GLIBC_2.3)
Requires(preun): libvtss_api.so.0
Requires(preun): /bin/sh
Requires(preun): libc.so.6
Requires(preun): libc.so.6(GLIBC_2.4)
Requires(postun): rtld(GNU_HASH)
Requires(postun): libc.so.6(GLIBC_2.7)
Requires(postun): libc.so.6(GLIBC_2.1)
Requires(postun): libc6 >= 2.15
Requires(postun): libvtss-api0 >= 0.1
Requires(postun): libc.so.6(GLIBC_2.0)
Requires(postun): libc.so.6(GLIBC_2.3)
Requires(postun): libvtss_api.so.0
Requires(postun): /bin/sh
Requires(postun): libc.so.6
Requires(postun): libc.so.6(GLIBC_2.4)
Suggests: update-rc.d
Provides: elf(buildid) = 410eb405a5162d9caf741051ac7dcabedea53615
Conflicts: smbstax

%description
Control application sample, headers and library

%package -n libvtss-api0
Summary: T1040 L2 Switch
Group: base
Requires: rtld(GNU_HASH)
Provides: libvtss_api.so.0
Provides: elf(buildid) = af928b39857b51d9cd7d4415b4f4f03156b6d50e

%description -n libvtss-api0
Control application sample, headers and library

%package -n libvtss-api-dev
Summary: T1040 L2 Switch
Group: base

%description -n libvtss-api-dev
Control application sample, headers and library

%package -n l2switch-dbg
Summary: T1040 L2 Switch - Debugging files
Group: devel
Suggests: libc6-dbg
Suggests: libvtss_api-dbg

%description -n l2switch-dbg
Control application sample, headers and library  This package contains ELF
symbols and related sources for debugging purposes.

%package -n l2switch-staticdev
Summary: T1040 L2 Switch - Development files (Static Libraries)
Group: devel
Requires: l2switch-dev = 0.1-r0

%description -n l2switch-staticdev
Control application sample, headers and library  This package contains
static libraries for software development.

%package -n l2switch-dev
Summary: T1040 L2 Switch - Development files
Group: devel
Requires: l2switch = 0.1-r0
Suggests: libc6-dev
Suggests: libvtss-api-dev
Suggests: initscripts-dev

%description -n l2switch-dev
Control application sample, headers and library  This package contains
symbolic links, header files, and related items necessary for software
development.

%package -n l2switch-doc
Summary: T1040 L2 Switch - Documentation files
Group: doc

%description -n l2switch-doc
Control application sample, headers and library  This package contains
documentation.

%package -n l2switch-locale
Summary: T1040 L2 Switch
Group: base

%description -n l2switch-locale
Control application sample, headers and library

%pre
# l2switch - preinst
#!/bin/sh
if [ -z "$D" -a -f "/etc/init.d/l2switch" ]; then
	/etc/init.d/l2switch stop
fi
if type update-rc.d >/dev/null 2>/dev/null; then
	if [ -n "$D" ]; then
		OPT="-f -r $D"
	else
		OPT="-f"
	fi
	update-rc.d $OPT l2switch remove
fi


%post
# l2switch - postinst
#!/bin/sh
if type update-rc.d >/dev/null 2>/dev/null; then
	if [ -n "$D" ]; then
		OPT="-r $D"
	else
		OPT="-s"
	fi
	update-rc.d $OPT l2switch defaults 10 85
fi


%preun
# l2switch - prerm
#!/bin/sh
if [ "$1" = "0" ] ; then
if [ -z "$D" ]; then
	/etc/init.d/l2switch stop
fi
fi

%postun
# l2switch - postrm
#!/bin/sh
if [ "$1" = "0" ] ; then
if type update-rc.d >/dev/null 2>/dev/null; then
	if [ -n "$D" ]; then
		OPT="-r $D"
	else
		OPT=""
	fi
	update-rc.d $OPT l2switch remove
fi
fi

%files
%defattr(-,-,-,-)
%dir "/usr"
%dir "/etc"
%dir "/usr/bin"
"/usr/bin/l2sw_bin"
"/usr/bin/l2switch-cfg"
%dir "/etc/init.d"
%dir "/etc/sysconfig"
"/etc/init.d/l2switch"
"/etc/sysconfig/l2switch"

%files -n libvtss-api0
%defattr(-,-,-,-)
%dir "/usr"
%dir "/usr/lib"
"/usr/lib/libvtss_api.so.4.61"
"/usr/lib/libvtss_api.so.0"

%files -n libvtss-api-dev
%defattr(-,-,-,-)
%dir "/usr"
%dir "/usr/include"
%dir "/usr/lib"
%dir "/usr/include/vtss_api"
%dir "/usr/include/vtss"
"/usr/include/COPYING"
%dir "/usr/include/vtss_api/boards"
"/usr/include/vtss_api/vtss_api.h"
"/usr/include/vtss_api/vtss_hqos_api.h"
"/usr/include/vtss_api/vtss_phy_api.h"
"/usr/include/vtss_api/vtss_qos_api.h"
"/usr/include/vtss_api/vtss_api_config.h"
"/usr/include/vtss_api/vtss_init_api.h"
"/usr/include/vtss_api/vtss_evc_api.h"
"/usr/include/vtss_api/vtss_security_api.h"
"/usr/include/vtss_api/vtss_port_api.h"
"/usr/include/vtss_api/vtss_afi_api.h"
"/usr/include/vtss_api/vtss_os.h"
"/usr/include/vtss_api/vtss_os_linux.h"
"/usr/include/vtss_api/vtss_packet_api.h"
"/usr/include/vtss_api/vtss_misc_api.h"
"/usr/include/vtss_api/vtss_l2_api.h"
"/usr/include/vtss_api/boards/port_custom_api.h"
%dir "/usr/include/vtss/api"
"/usr/include/vtss/api/types.h"
"/usr/include/vtss/api/port.h"
"/usr/include/vtss/api/l2_types.h"
"/usr/include/vtss/api/options.h"
"/usr/include/vtss/api/phy.h"
"/usr/lib/libvtss_api.so"

%files -n l2switch-dbg
%defattr(-,-,-,-)

%files -n l2switch-dev
%defattr(-,-,-,-)

