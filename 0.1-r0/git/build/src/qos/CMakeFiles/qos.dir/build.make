# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/build

# Include any dependencies generated for this target.
include src/qos/CMakeFiles/qos.dir/depend.make

# Include the progress variables for this target.
include src/qos/CMakeFiles/qos.dir/progress.make

# Include the compile flags for this target's objects.
include src/qos/CMakeFiles/qos.dir/flags.make

src/qos/CMakeFiles/qos.dir/qos.c.o: src/qos/CMakeFiles/qos.dir/flags.make
src/qos/CMakeFiles/qos.dir/qos.c.o: ../src/qos/qos.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object src/qos/CMakeFiles/qos.dir/qos.c.o"
	cd /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/build/src/qos && /home/sirius/yocto/poky/build_t1040rdb_release/tmp/sysroots/x86_64-linux/usr/bin/ppce5500-fsl-linux/powerpc-fsl-linux-gcc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/qos.dir/qos.c.o   -c /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/src/qos/qos.c

src/qos/CMakeFiles/qos.dir/qos.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/qos.dir/qos.c.i"
	cd /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/build/src/qos && /home/sirius/yocto/poky/build_t1040rdb_release/tmp/sysroots/x86_64-linux/usr/bin/ppce5500-fsl-linux/powerpc-fsl-linux-gcc  $(C_DEFINES) $(C_FLAGS) -E /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/src/qos/qos.c > CMakeFiles/qos.dir/qos.c.i

src/qos/CMakeFiles/qos.dir/qos.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/qos.dir/qos.c.s"
	cd /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/build/src/qos && /home/sirius/yocto/poky/build_t1040rdb_release/tmp/sysroots/x86_64-linux/usr/bin/ppce5500-fsl-linux/powerpc-fsl-linux-gcc  $(C_DEFINES) $(C_FLAGS) -S /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/src/qos/qos.c -o CMakeFiles/qos.dir/qos.c.s

src/qos/CMakeFiles/qos.dir/qos.c.o.requires:
.PHONY : src/qos/CMakeFiles/qos.dir/qos.c.o.requires

src/qos/CMakeFiles/qos.dir/qos.c.o.provides: src/qos/CMakeFiles/qos.dir/qos.c.o.requires
	$(MAKE) -f src/qos/CMakeFiles/qos.dir/build.make src/qos/CMakeFiles/qos.dir/qos.c.o.provides.build
.PHONY : src/qos/CMakeFiles/qos.dir/qos.c.o.provides

src/qos/CMakeFiles/qos.dir/qos.c.o.provides.build: src/qos/CMakeFiles/qos.dir/qos.c.o

qos: src/qos/CMakeFiles/qos.dir/qos.c.o
qos: src/qos/CMakeFiles/qos.dir/build.make
.PHONY : qos

# Rule to build all files generated by this target.
src/qos/CMakeFiles/qos.dir/build: qos
.PHONY : src/qos/CMakeFiles/qos.dir/build

src/qos/CMakeFiles/qos.dir/requires: src/qos/CMakeFiles/qos.dir/qos.c.o.requires
.PHONY : src/qos/CMakeFiles/qos.dir/requires

src/qos/CMakeFiles/qos.dir/clean:
	cd /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/build/src/qos && $(CMAKE_COMMAND) -P CMakeFiles/qos.dir/cmake_clean.cmake
.PHONY : src/qos/CMakeFiles/qos.dir/clean

src/qos/CMakeFiles/qos.dir/depend:
	cd /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/src/qos /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/build /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/build/src/qos /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/build/src/qos/CMakeFiles/qos.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : src/qos/CMakeFiles/qos.dir/depend

