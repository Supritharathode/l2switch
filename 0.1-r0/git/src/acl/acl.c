/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#include "acl.h"
#include "l2_switch_reg.h"

#define POLICER_INDEX	7
#define RATE		3
#define INGRESS_PORT	0
#define FM1_GB0_PORT	8
#define FM1_GB1_PORT	9
#define ENTRY_ID	1
#define SIP_FILTER	0x0D0D0D01
#define SMAC_FILTER	"68:05:CA:1A:06:41"

static int auth_usage()
{
	printf("Usage: set auth none|egress|both <port_no>\n");
	return -1;
}

int auth_command(const char *command)
{
	vtss_auth_state_t	state;
	unsigned int		port;

	if (strstr(command, "none ") != 0) {
		state = VTSS_AUTH_STATE_NONE;
		command += strlen("none ");
	} else if (strstr(command, "egress ") != 0) {
		state = VTSS_AUTH_STATE_EGRESS;
		command += strlen("egress ");
	} else if (strstr(command, "both ") != 0) {
		state = VTSS_AUTH_STATE_BOTH;
		command += strlen("both ");
	} else
		return auth_usage();

	/* Parse "port" keyword" */
	if (strncmp("port ", command, strlen("port ")))
		return auth_usage();
	command += strlen("port ");

	/* Parse the port value */
	if (get_next_value(&command, &port))
		return auth_usage();

	if (!is_valid_port(port)) {
		printf("Please enter a valid port number!\n");
		return -1;
	}

	/* Set state for port */
	if (vtss_auth_port_state_set(NULL, port, state) != VTSS_RC_OK) {
		printf("ERROR:%s:%d:Unable to set auth state!\n",
				__func__, __LINE__);
		return -1;
	}

	return 0;
}

int test_L2_ACL_1(bool restore)
{
	vtss_ace_t	entry;

	/* Initialize the entry with the default values */
	if (vtss_ace_init(NULL, VTSS_ACE_TYPE_IPV4, &entry) != VTSS_RC_OK) {
		printf("ERROR:%s:%d:Cannot initialize ACE!\n",
				__func__, __LINE__);
		return -1;
	}

	/* Assign an ID to this entry */
	entry.id = ENTRY_ID;

	/* Disable learning for frames hitting this action; the hardware
	 * bit is called LRN_DIS; setting it to 1 disables lerning.
	 */
	entry.action.learn = FALSE;

	/* Forward first frame hitting this action to CPU */
	entry.action.cpu_once = TRUE;

	/* Set the CPU extraction queue that is used when a frame is
	 * coppied to the CPU due to CPU_ONCE action */
	entry.action.cpu_queue = 0;

	/* Set the port for which this ACL entry applies */
	entry.port_list[FM1_GB0_PORT] = TRUE;

	/* Set ACL action: disable forwarding towards all active ports */
	entry.action.port_action = VTSS_ACL_PORT_ACTION_FILTER;
	entry.action.port_list[FM1_GB1_PORT] = FALSE;
	entry.action.port_list[INGRESS_PORT] = FALSE;

	/* Add this ACL entry */
	if (vtss_ace_add(NULL, VTSS_ACE_ID_LAST, &entry) != VTSS_RC_OK) {
		printf("ERROR:%s:%d:Cannot add new ACL entry!\n",
				__func__, __LINE__);
		return -1;
	}

	return 0;
}

/* Test uses two ACLs
 *
 * ARP requests coming from specific IP address (129.111.168.68) on
 * port specified in MONITORED_PORT are checked for correctness and
 * forwarded. Other ARP packets are dropped.
 */
int test_L2_ACL_2(bool restore)
{
#define ALLOWED_IP		0x816FA844 /* 129.111.168.68 */
#define ALLOWED_IP_MASK		0xFFFFFFFF
#define MONITORED_PORT		8
#define ENTRY_INSP_ID		255
#define ENTRY_DROP_ID		(ENTRY_INSP_ID - 1)

	vtss_ace_t		entry, entry_drop;
	vtss_ace_counter_t	counter;

	if (restore) {
		/* Display some statistics */
		vtss_ace_counter_get(NULL, ENTRY_INSP_ID, &counter);
		printf("ACE %d (inspection) got %d hits\n", ENTRY_INSP_ID,
				counter);

		vtss_ace_counter_get(NULL, ENTRY_DROP_ID, &counter);
		printf("ACE %d (drop) got %d hits\n", ENTRY_DROP_ID, counter);

		/* Delete entries */
		if (vtss_ace_del(NULL, ENTRY_INSP_ID) != VTSS_RC_OK) {
			printf("ERROR:%s:%d:Cannot delete ACL entry %d\n",
					__func__, __LINE__, ENTRY_ID);
			return -1;
		}

		if (vtss_ace_del(NULL, ENTRY_DROP_ID) != VTSS_RC_OK) {
			printf("ERROR:%s:%d:Cannot delete ACL entry %d\n",
					__func__, __LINE__, ENTRY_DROP_ID);
			return -1;
		}
	} else {
		printf("Configuring ACE %d (inspection)\n", ENTRY_INSP_ID);
		/* Initialize the entry with the default values */
		if (vtss_ace_init(NULL, VTSS_ACE_TYPE_ARP, &entry) != VTSS_RC_OK) {
			printf("ERROR:%s:%d:Cannot initialize ACE!\n",
					__func__, __LINE__);
			return -1;
		}

		entry.id = ENTRY_INSP_ID;

		/* Checks to perform */
		entry.frame.arp.arp		= VTSS_ACE_BIT_ANY;
		entry.frame.arp.req		= VTSS_ACE_BIT_1;
		entry.frame.arp.ip		= VTSS_ACE_BIT_1;
		entry.frame.arp.length		= VTSS_ACE_BIT_1;
		entry.frame.arp.smac_match	= VTSS_ACE_BIT_1;

		entry.frame.arp.sip.value	= ALLOWED_IP;
		entry.frame.arp.sip.mask	= ALLOWED_IP_MASK;

		/* Monitored ports */
		entry.port_list[MONITORED_PORT] = TRUE;

		entry.dmac_bc = VTSS_ACE_BIT_1;

		/* Add ACL entry */
		if (vtss_ace_add(NULL, VTSS_ACE_ID_LAST, &entry) != VTSS_RC_OK) {
			printf("ERROR:%s:%d:Cannot add new ACL entry!\n",
					__func__, __LINE__);
			return -1;
		}

		/* Drop other ARP requests */
		printf("Configuring ACE %d (drop)\n", ENTRY_DROP_ID);
		if (vtss_ace_init(NULL, VTSS_ACE_TYPE_ARP, &entry_drop) != VTSS_RC_OK) {
			printf("ERROR:%s:%d:Cannot initialize ACE!\n",
					__func__, __LINE__);
			return -1;
		}

		entry_drop.id = ENTRY_DROP_ID;
		entry_drop.dmac_bc = VTSS_ACE_BIT_1;

		/* Checks to perform */
		entry_drop.frame.arp.arp	= VTSS_ACE_BIT_ANY;

		entry_drop.frame.arp.sip.value	= ALLOWED_IP;
		entry_drop.frame.arp.sip.mask	= !ALLOWED_IP_MASK;

		/* Monitored ports */
		entry_drop.port_list[MONITORED_PORT] = TRUE;

		/* Drop matching ARP requests */
		entry_drop.action.port_action = VTSS_ACL_PORT_ACTION_FILTER;
		entry_drop.action.learn = FALSE;

		/* Add ACL entry */
		if (vtss_ace_add(NULL, VTSS_ACE_ID_LAST, &entry_drop) != VTSS_RC_OK) {
			printf("ERROR:%s:%d:Cannot add new ACL entry!\n",
					__func__, __LINE__);
			return -1;
		}
	}

	printf("ARP frame inspection %s\n", restore ? "restored" : "configured");
	return 0;
}

int test_L2_ACL_3(bool restore)
{
	vtss_acl_port_conf_t	conf;

	/* Get the ACL port configuration for port INGRESS_PORT */
	if (vtss_acl_port_conf_get(NULL, INGRESS_PORT, &conf) != VTSS_RC_OK) {
		printf("ERROR:%s:%d:Cannot get the ACL port conf!\n",
				__func__, __LINE__);
		return -1;
	}

	/* Action to use for the egress port list */
	conf.action.port_action = VTSS_ACL_PORT_ACTION_FILTER;

	/* Enable filtering on port FM1_GB1_PORT, no filtering on port
	 * FM1_GB0_PORT. The port list is ANDed with DEST mask. So FALSE,
	 * which is 0, means that egress port will be filtered,
	 * i.e cleared from the final DEST mask.*/
	conf.action.port_list[FM1_GB0_PORT] = TRUE;
	conf.action.port_list[FM1_GB1_PORT] = FALSE;

	/* Set the ACL port configuration for port INGRESS_PORT */
	if (vtss_acl_port_conf_set(NULL, INGRESS_PORT, &conf) != VTSS_RC_OK) {
		printf("ERROR:%s:%d:Cannot set the ACL port conf!\n",
				__func__, __LINE__);
		return -1;
	}

	return 0;
}

int test_L2_ACL_4(bool restore)
{
	vtss_acl_policer_conf_t		policer;
	vtss_ace_t			entry;
	u32 value;
	/* Get the desired policer */
	printf("AC_4\n");
u32 offset = (0xa8001c - 0x800000) >> 2;
l2sw_reg_read(0, offset, &value);
printf("%x\n", value);
	if (vtss_acl_policer_conf_get(NULL, POLICER_INDEX, &policer) !=
			VTSS_RC_OK) {
		printf("ERROR:%s:%d:Cannot get the %d policer!\n",
				__func__, __LINE__, POLICER_INDEX);
		return -1;
	}

	/* Modify the policer: use packet rate policing and permit only
	 * RATE packets per second. */
	policer.bit_rate_enable = FALSE;
	policer.rate = RATE;

	/* Set the policer accordingly */
	if (vtss_acl_policer_conf_set(NULL, POLICER_INDEX, &policer) !=
			VTSS_RC_OK) {
		printf("ERROR:%s:%d:Cannot set the %d policer!\n",
				__func__, __LINE__, POLICER_INDEX);
		return -1;
	}

	/* Initialize the entry with the default values */
	if (vtss_ace_init(NULL, VTSS_ACE_TYPE_IPV4, &entry) != VTSS_RC_OK) {
		printf("ERROR:%s:%d:Cannot initialize ACE!\n",
				__func__, __LINE__);
		return -1;
	}

	/* Assign an ID to this entry */
	entry.id = ENTRY_ID;

	/* L4 protocol is TCP */
	entry.frame.ipv4.proto.value = 0x06;
	entry.frame.ipv4.proto.mask = 0xFF;

	/* Inspect the SYN flag */
	entry.frame.ipv4.tcp_syn = VTSS_ACE_BIT_1;

	/* Set the port for which this ACL entry applies */
	entry.port_list[INGRESS_PORT] = TRUE;

	/* Use the previously configured policer */
	entry.action.police = TRUE;
	entry.action.policer_no = POLICER_INDEX;

	/* Add this ACL entry */
	if (vtss_ace_add(NULL, VTSS_ACE_ID_LAST, &entry) != VTSS_RC_OK) {
		printf("ERROR:%s:%d:Cannot add new ACL entry!\n",
				__func__, __LINE__);
		return -1;
	}
//u32 offset = (0xa8001c - 0x800000) >> 2;
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
	return 0;
}

int test_L2_ACL_5(bool restore)
{
	vtss_acl_policer_conf_t		acl_policer;
	vtss_acl_port_conf_t		conf;

	/* Get the desired policer */
	if (vtss_acl_policer_conf_get(NULL, POLICER_INDEX, &acl_policer) != VTSS_RC_OK) {
		printf("ERROR:%s:%d:Cannot get the 7th policer!\n",
				__func__, __LINE__);
		return -1;
	}

	/* Modify the policer */
	acl_policer.bit_rate_enable = FALSE; /* Use packet rate policing */
	acl_policer.rate = RATE; /* Permit only RATE packets per second */

	/* Set the policer accordingly */
	if (vtss_acl_policer_conf_set(NULL, POLICER_INDEX, &acl_policer) != VTSS_RC_OK) {
		printf("ERROR:%s:%d:Cannot set the 7th policer!\n",
				__func__, __LINE__);
		return -1;
	}

	/* Get the ACL port configuration for INGRESS_PORT */
	if (vtss_acl_port_conf_get(NULL, INGRESS_PORT, &conf) != VTSS_RC_OK) {
		printf("ERROR:%s:%d:Cannot get the ACL port conf!\n",
				__func__, __LINE__);
		return -1;
	}

	/* Use the policer in an ACL port configuration */
	conf.action.police = TRUE;
	conf.action.policer_no = POLICER_INDEX;

	/* Set the ACL port configuration for port INGRESS_PORT */
	if (vtss_acl_port_conf_set(NULL, INGRESS_PORT, &conf) != VTSS_RC_OK) {
		printf("ERROR:%s:%d:Cannot set the ACL port conf!\n",
				__func__, __LINE__);
		return -1;
	}

	return 0;
}

int test_L2_ACL_6(bool restore)
{
	vtss_ace_t	entry;

	/* Initialize the entry with the default values */
	if (vtss_ace_init(NULL, VTSS_ACE_TYPE_IPV4, &entry) != VTSS_RC_OK) {
		printf("ERROR:%s:%d:Cannot initialize ACE!\n",
				__func__, __LINE__);
		return -1;
	}

	/* Assign an ID to this entry */
	entry.id = ENTRY_ID;

	/* Enable SIP/SMAC filtering for IP source guard */
	entry.frame.ipv4.sip_smac.enable = TRUE;

	/* Define the SIP */
	entry.frame.ipv4.sip_smac.sip = SIP_FILTER;

	/* Configure the desired SMAC to match against */
	if (parse_mac(SMAC_FILTER,
				entry.frame.ipv4.sip_smac.smac.addr) !=
			MAC_ADDR_COUNT) {
		printf("ERROR:%s:%d:Invalid MAC address!\n",
				__func__, __LINE__);
		return -1;
	}

	/* Set the port for which this ACL entry applies */
	entry.port_list[FM1_GB0_PORT] = TRUE;

	/* Set ACL action: filter all traffic towards all active ports */
	entry.action.port_action = VTSS_ACL_PORT_ACTION_FILTER;
	entry.action.port_list[FM1_GB1_PORT] = FALSE;
	entry.action.port_list[INGRESS_PORT] = FALSE;

	/* Add this ACL entry */
	if (vtss_ace_add(NULL, VTSS_ACE_ID_LAST, &entry) != VTSS_RC_OK) {
		printf("ERROR:%s:%d:Cannot add new ACL entry!\n",
				__func__, __LINE__);
		return -1;
	}

	return 0;
}

int test_L2_ACL_8(bool restore)
{
	/* port numbers used by BOOTP */
	const vtss_udp_tcp_t	bootpc		= 67;
	const vtss_udp_tcp_t	bootps		= 68;

	const vtss_ace_id_t	entry_id4	= 2;
	const vtss_ace_id_t	entry_id6	= 3;

	vtss_ace_t		entry4, entry6;

	if (restore) {
		/* Remove ACL entries */
		if (vtss_ace_del(NULL, entry_id4) != VTSS_RC_OK) {
			printf("ERROR:%s:%d:Cannot delete ACL entry %d\n",
					__func__, __LINE__, entry_id4);
			return -1;
		}
		if (vtss_ace_del(NULL, entry_id6) != VTSS_RC_OK) {
			printf("ERROR:%s:%d:Cannot delete ACL entry %d\n",
					__func__, __LINE__, entry_id6);
			return -1;
		}
	} else {
		/* Initialize the entries with the default values */
		if (vtss_ace_init(NULL, VTSS_ACE_TYPE_IPV4, &entry4) != VTSS_RC_OK) {
			printf("ERROR:%s:%d:Cannot initialize ACE for IPv4!\n",
					__func__, __LINE__);
			return -1;
		}
		if (vtss_ace_init(NULL, VTSS_ACE_TYPE_IPV6, &entry6) != VTSS_RC_OK) {
			printf("ERROR:%s:%d:Cannot initialize ACE for IPv6!\n",
					__func__, __LINE__);
			return -1;
		}

		entry4.id = entry_id4;
		entry6.id = entry_id6;

		/* Set filtering ports */
		entry4.frame.ipv4.sport.in_range	= TRUE;
		entry4.frame.ipv4.sport.low 		= bootpc;
		entry4.frame.ipv4.sport.high 		= bootps;

		entry4.frame.ipv4.dport.in_range 	= TRUE;
		entry4.frame.ipv4.dport.low 		= bootpc;
		entry4.frame.ipv4.dport.high 		= bootps;

		entry6.frame.ipv6.sport.in_range	= TRUE;
		entry6.frame.ipv6.sport.low 		= bootpc;
		entry6.frame.ipv6.sport.high 		= bootps;

		entry6.frame.ipv6.dport.in_range 	= TRUE;
		entry6.frame.ipv6.dport.low 		= bootpc;
		entry6.frame.ipv6.dport.high 		= bootps;

		/* Monitor all ports */
		memset(entry4.port_list, TRUE,
			VTSS_PORT_ARRAY_SIZE * sizeof(entry4.port_list[0]));
		memset(entry6.port_list, TRUE,
			VTSS_PORT_ARRAY_SIZE * sizeof(entry6.port_list[0]));

		/* Enable CPU redirection */
		entry4.action.cpu = TRUE;
		entry6.action.cpu = TRUE;

		/* Do not learn filtered entries */
		entry4.action.learn = FALSE;
		entry6.action.learn = FALSE;

		/* Disable forwarding of the frames */
		entry4.action.port_action = VTSS_ACL_PORT_ACTION_FILTER;
		entry6.action.port_action = VTSS_ACL_PORT_ACTION_FILTER;
		memset(entry4.action.port_list, FALSE,
			VTSS_PORT_ARRAY_SIZE * sizeof(entry4.action.port_list[0]));
		memset(entry4.action.port_list, FALSE,
			VTSS_PORT_ARRAY_SIZE * sizeof(entry4.action.port_list[0]));

		/* Add ACL entries */
		if (vtss_ace_add(NULL, VTSS_ACE_ID_LAST, &entry4) != VTSS_RC_OK) {
			printf("ERROR:%s:%d:Cannot add new ACL entry!\n",
					__func__, __LINE__);
			return -1;
		}
		if (vtss_ace_add(NULL, VTSS_ACE_ID_LAST, &entry6) != VTSS_RC_OK) {
			printf("ERROR:%s:%d:Cannot add new ACL entry!\n",
					__func__, __LINE__);
			return -1;
		}
	}

	printf("DHCP snooping %s\n", restore ? "restored" : "configured");
	return 0;
}
//////////////////////////////////////// My code for ping policing ////////////////////////////////////////////////
//policer configuration
struct REG_ENTRIES {

	unsigned int reg;
	unsigned int count;
	unsigned int inc;
	unsigned int def;
	unsigned int value;
	} ;	

static struct REG_ENTRIES is2_tcam_data [] =
{

        {CAP_IS2_ENTRY_0, 1, 0, 0,  0x00},  // R0 
        {CAP_IS2_ENTRY_1, 1, 0, 0,  0x4000}, // R1 46TH BIT
	{CAP_IS2_ENTRY_2, 1, 0, 0,  0x00},//nothing to be set
        {CAP_IS2_ENTRY_3, 1, 0, 0,  0x10000000}, // R3 124TH 0X01 124-131
        {CAP_IS2_ENTRY_4, 1, 0, 0,  0x8}, // R4 0X8* 56 bits
        {CAP_IS2_ENTRY_5, 7, 4, 0,  0x00000000}, // R5-11        
        
	{CAP_IS2_MASK_0, 1, 0, 0,  0xFFFFFFFF},  // R0 R1  //Match any
        {CAP_IS2_MASK_1, 1, 0, 0,  0xFFFFBFFF}, // R2 1 bit match any last 9 match exact
	{CAP_IS2_MASK_2, 1, 0, 0,  0xFFFFFFFF},
        {CAP_IS2_MASK_3, 1, 0, 0,  0xFFFFFFF}, // R3
        {CAP_IS2_MASK_4, 1, 0, 0,  0x00}, // R4 // 7 bit match exact, remaining match any
        {CAP_IS2_MASK_5, 7, 4, 0,  0xFFFFFFFF}, // R5-11

        {CAP_IS2_ACT_0, 0, 0, 0,   0x3e00}, // action cache for policer index and policer enabling
	{CAP_IS2_ACT_1, 4, 4, 0,  0x00}, // Action cache


        {CAP_IS2_TG, 1, 0, 0, 0x0000000A} // Type Group - s1_normal 2 bits for each 1010



};
static void reg_update(unsigned int regbase, unsigned int count, unsigned int inc, unsigned int value)
{
	u32 offset, i;
	offset = 0;
	for (i = 1; i <= count ; i++)
	{
	    offset = (regbase - 0x800000) >> 2;
	    l2sw_reg_write(0, offset, value);
	    regbase += inc;
	}

}
//popul;ating the registers with the values and enabling the ports for lookups to match against the rules
int populate_values(bool restore){
int i, size ;
size = sizeof(is2_tcam_data)/ sizeof(struct REG_ENTRIES);
 for (i =0; i < size; i++)
    {
            reg_update (is2_tcam_data[i].reg, is2_tcam_data[i].count, is2_tcam_data[i].inc, is2_tcam_data[i].value);
    }
  u32 value ;
///Enable lookups in IS2 (CAP_S2_CFG.S2_ENA). If disabled, frames received by the
//port module are not matched against rules in CAP IS2.
  u32 offset = (0xa8001c - 0x800000) >> 2;
 l2sw_reg_read(0, offset, &value);
   value = value & 0xffffbfff;
   value = value | 0x4000;
  l2sw_reg_write(0, offset, value);

 offset = (0xa8011c - 0x800000) >> 2;
l2sw_reg_read(0, offset, &value);
   value = value & 0xffffbfff;
   value = value | 0x4000;
  l2sw_reg_write(0, offset, value);

 offset = (0xa8021c - 0x800000) >> 2;
l2sw_reg_read(0, offset, &value);
   value = value & 0xffffbfff;
   value = value | 0x4000;
  l2sw_reg_write(0, offset, value);

 offset = (0xa8031c - 0x800000) >> 2;
l2sw_reg_read(0, offset, &value);
   value = value & 0xffffbfff;
   value = value | 0x4000;
  l2sw_reg_write(0, offset, value);

 offset = (0xa8041c - 0x800000) >> 2;
l2sw_reg_read(0, offset, &value);
   value = value & 0xffffbfff;
   value = value | 0x4000;
  l2sw_reg_write(0, offset, value);

 offset = (0xa8051c - 0x800000) >> 2;
l2sw_reg_read(0, offset, &value);
   value = value & 0xffffbfff;
   value = value | 0x4000;
  l2sw_reg_write(0, offset, value);

 offset = (0xa8061c - 0x800000) >> 2;
l2sw_reg_read(0, offset, &value);
   value = value & 0xffffbfff;
   value = value | 0x4000;
  l2sw_reg_write(0, offset, value);



 offset = (0xa8071c - 0x800000) >> 2;
l2sw_reg_read(0, offset, &value);
   value = value & 0xffffbfff;
   value = value | 0x4000;
  l2sw_reg_write(0, offset, value);

 offset = (0xa8081c - 0x800000) >> 2;
l2sw_reg_read(0, offset, &value);
   value = value & 0xffffbfff;
   value = value | 0x4000;
  l2sw_reg_write(0, offset, value);

 offset = (0xa8091c - 0x800000) >> 2;
l2sw_reg_read(0, offset, &value);
   value = value & 0xffffbfff;
   value = value | 0x4000;
  l2sw_reg_write(0, offset, value);

 offset = (0xa80a1c - 0x800000) >> 2;
l2sw_reg_read(0, offset, &value);
   value = value & 0xffffbfff;
   value = value | 0x4000;
  l2sw_reg_write(0, offset, value);
return 0;
}

int ping_policing(bool restore){
		u32 value;
		u32 offset = (0xa8a300 - 0x800000) >> 2;
	        l2sw_reg_read(0, offset, &value);
	        printf("%x\n", value);
	/*vlue = value >> 21;
		value = value << 21;
		value = value | 128;
		l2sw_reg_write(0, offset, value);
		l2sw_reg_read(0, offset, &value);
		printf("%x\n", value);	
		value = value << 6;*/
		value = value & 0xffe0003f; 		//setting pol_pir_cfg.pir_rate = 128
		value = value | 0x2000;
		value = value | 1;			//setting the pol_pir_cfg.pir_burst = 1
		l2sw_reg_write(0, offset, value);
		l2sw_reg_read(0, offset, &value);
		printf("%x\n", value);
		u32 val;
		u32 offset_val = (0xa8a308 - 0x800000) >> 2;
	        l2sw_reg_read(0, offset_val, &val);
	        printf("mode:%x\n", val);
		val = val & 0xffffffef;	//setting pol_mode_cfg.frm_mode = 1
		val = val | 0x8;
		l2sw_reg_write(0, offset_val, val);
		l2sw_reg_read(0, offset_val, &val);
		printf("%x\n", val);
return 0;

}
