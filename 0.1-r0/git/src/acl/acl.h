/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#ifndef _ACL_H_
#define _ACL_H_

#include <fsl_utils/fsl_utils.h>

/* Set a single port into one of three 802.1X authorization modes: none,
 * egress, both.
 *
 * CLI usage: set auth none|egress|both port <port_no>
 */
int auth_command(const char *command);
static void reg_update(unsigned int regbase, unsigned int count, unsigned int inc, unsigned int value);
int populate_values(bool restore);


/* Disable learning and forwarding for packets arriving on one port, but
 * redirect the first frame that hits the action to a CPU queue using
 * hit-me-once feature.
 */
int test_L2_ACL_1(bool restore);

/* Usecase on ARP frame inspection - allow forwarding and learning of
 * ARP requests received on a port, if the source IP address is the one
 * specified.
 */
int test_L2_ACL_2(bool restore);

/* This use case is about discarding all egress traffic going towards
 * one port using an ACL port configuration with FILTER action set
 * on egress ports.
 */
int test_L2_ACL_3(bool restore);

/* This use case is about TCP SYN policing: limit the new TCP connections
 * per second to a configured limit.
 */
int test_L2_ACL_4(bool restore);

/* This use case is about setting one ACL rate limiter for a port.
 * Hence, we configure an ACL policer to allow rates no greater than
 * a certain rate. Then we apply an ACL port configuration on the port
 * that points to the previously created policer.
 */
int test_L2_ACL_5(bool restore);

/* Use case on IP source guard. An ACL is configured to drop packets
 * that match a specified SMAC+IP binding.
 */
int test_L2_ACL_6(bool restore);

/* Use case on DHCP Snooping. An ACL is configured to redirect to CPU frames
 * with L4 UDP ports 67 and 68.
 */
int test_L2_ACL_8(bool restore);
int ping_policing(bool restore);

#endif /* _ACL_H_ */
