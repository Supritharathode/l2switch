/*

# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#include "main.h"
#include <sys/types.h>
#include <sys/times.h>
#include <sys/stat.h>
#include <fcntl.h>

#define NPI_CDEV	"/dev/npi"

#include <port_counters/port_counters.h>

#define TIMEOUT_SEC		1
#define TIMEOUT_NSEC		0

#define EXIT_INDICATION		1
/* Number of control frames received per chunk */
#define CONTROL_FRAMES_RECV	100

#define COMMAND_LOG_MAX		50


#define XTR_GRP_CFG_REG		(0x080000 >> 2)
#define INJ_GRP_CFG_REG		(0x080024 >> 2)

#define CPU_FRAME_BYTE_SWAP		0x00000001
#define CPU_FRAME_EOF_WORD_POS_AFTER	0x00000002

static struct timespec		*timeout = NULL;
extern short			receive_control_frames;
static struct tms		init_cpu_time;
static char			*command_log[COMMAND_LOG_MAX];
static int			log_head;

/* Init array for logged commands */
static void command_log_init(void)
{
	int i;

	log_head = 0;
	for(i = 0; i < COMMAND_LOG_MAX; i++)
		command_log[i] = NULL;
}

/* Free all logged commands */
static void command_log_release_all(void)
{
	int i;

	for (i = 0; i < COMMAND_LOG_MAX; i++)
		if (command_log[i] != NULL) {
			free(command_log[i]);
			command_log[i] = NULL;
		}
	log_head = 0;
}

/* log a command */
static void command_log_add(const char *command)
{
	log_head = (log_head == 0 ? COMMAND_LOG_MAX - 1 : log_head - 1);

	/* If we already have logged COMMAND_LOG_MAX commands,
	 * release the oldest one
	 */
	if (command_log[log_head] != NULL) {
		free(command_log[log_head]);
		command_log[log_head] = NULL;
	}

	/* add the new command on the head of the list */
	if ((command_log[log_head] = strdup(command)) == NULL) {
		perror("No more memmory to add the new command\n");
		/* Delete all the logged commands to free memory */
		command_log_release_all();
		return;
	}
}

/* print the logged commands */
static void command_log_dump(void)
{
	int aux;

	if (command_log[log_head] == NULL) {
		printf("No logged commands\n");
		return;
	}

	printf("The latest commands:\n");

	/* print the commands in the reverse order */
	aux = log_head;
	do {
		aux = (aux == 0 ? COMMAND_LOG_MAX - 1 : aux - 1);
		if (command_log[aux] != NULL)
			printf("%s\n", command_log[aux]);
	} while (aux != log_head);
}

/* check for a "fail" reason */
static int check_for_errors(void)
{
	int i;

	vtss_port_counters_t counters;
	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++) {
		if (vtss_port_counters_get(NULL, i, &counters) != VTSS_RC_OK) {
			printf("ERROR: Failed to get counters for port %d\n", i);
			continue;
		}

		/* check for dropped or invalid packets */
		if (counters.rmon.rx_etherStatsDropEvents ||
				counters.rmon.tx_etherStatsDropEvents ||
				counters.rmon.rx_etherStatsCRCAlignErrors ||
				counters.if_group.ifOutErrors ||
				counters.rmon.rx_etherStatsUndersizePkts ||
				counters.rmon.rx_etherStatsOversizePkts ||
				counters.rmon.rx_etherStatsFragments ||
				counters.rmon.rx_etherStatsJabbers ||
				counters.if_group.ifInErrors ||
				counters.if_group.ifOutErrors ||
				counters.if_group.ifInDiscards ||
				counters.if_group.ifOutDiscards)
			return 1;
	}

	return 0;
}

/* create the setup for the debug file */
static int debug_log_init(int *stdout_fd_bk)
{
	int i;
	int tries = 100;
	char dbg_file_name[strlen(DEBUG_LOG_TEMP) + 1];
	int debug_log_fd;

	strcpy(dbg_file_name, DEBUG_LOG_TEMP);

	debug_log_fd = -1;
	for (i = 0; i < tries && debug_log_fd == -1; i++) {
		if (mktemp(dbg_file_name) == NULL) {
			perror("mktemp failed");
			return -1;
		}

		debug_log_fd = open(dbg_file_name, O_EXCL | O_CREAT | O_WRONLY,
				0644);
	}
	if (debug_log_fd == -1) {
		perror("failed to open a debug file");
		return -1;
	}

	printf("Opened log file %s\n", dbg_file_name);

	/* Temporary replacing stdout fd with fd used for the log file;
	 * this is done to avoid changing the headers for the used functions */
	if ((*stdout_fd_bk = dup(STDOUT_FILENO)) == -1) {
		perror("failed to duplicate stdout fd");
		goto __dup_stdin_out;
	}
	fsync(STDOUT_FILENO);
	close(STDOUT_FILENO);

	if (dup2(debug_log_fd, STDOUT_FILENO) == -1) {
		perror("failed to duplicate fd for debug file");
		goto __dup_file_out;
	}
	close(debug_log_fd);

	printf("Debug file %s created on "__DATE__", "__TIME__"\n\n",
			dbg_file_name);

	return 0;

__dup_file_out:
	if (dup2(*stdout_fd_bk, STDOUT_FILENO) == -1)
		perror("failed to restore stdout fd");
__dup_stdin_out:
	close(debug_log_fd);

	return -1;
}

/* Dump debug information in log file */
static void debug_log_dump(void)
{
	int i;
	struct tms total_cpu_time;

	/* Print total CPU time taken by the current process */
	if (times(&total_cpu_time) == -1)
		perror("failed to get total CPU time\n");
	else {
		printf("Total CPU time (clock ticks):\n");
		printf("\tuser time: %lu\n", total_cpu_time.tms_utime);
		printf("\tsystem time: %lu\n", total_cpu_time.tms_stime);
		printf("\tuser time of children: %lu\n",
				total_cpu_time.tms_cutime);
		printf("\tsystem time of children: %lu\n",
				total_cpu_time.tms_cstime);
	}

	/* Print init CPU time taken by the current process */
	if (init_cpu_time.tms_utime == 0 &&
			init_cpu_time.tms_stime == 0 &&
			init_cpu_time.tms_cutime == 0 &&
			init_cpu_time.tms_cstime == 0) {
		perror("failed to get init CPU time\n");
	} else {
		printf("from which, init CPU time (clock ticks) was:\n");
		printf("\tuser time: %lu\n", init_cpu_time.tms_utime);
		printf("\tsystem time: %lu\n", init_cpu_time.tms_stime);
		printf("\tuser time of children: %lu\n",
				init_cpu_time.tms_cutime);
		printf("\tsystem time of children: %lu\n",
				init_cpu_time.tms_cstime);
	}

	printf("\nPort configurations:\n");
	port_config_show_all();

	printf("\nFlow control:\n");
	port_flow_control_show_all();

	printf("\nMAC entries:\n");
	if (mac_command("dump"))
		printf("Failed to dump mac entries\n");

	printf("\nPort statistics:\n");
	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++) {
		if (port_counters(i, "all"))
			printf("Failed to get general counters for port %d\n",
					i);
		if (port_counters(i, "errors"))
			printf("Failed to get error counters for port %d\n",
					i);
		if (port_counters(i, "discards"))
			printf("Failed to get discard counters for port %d\n",
					i);
	}

	printf("\n");
	/* Dump the logged commands */
	command_log_dump();
}

/* close the debug file */
static int debug_log_close(int stdout_fd_bk)
{
	/* close the log file */
	fsync(STDOUT_FILENO);
	close(STDOUT_FILENO);

	/* Restore previous stdout fd */
	if (dup2(stdout_fd_bk, STDOUT_FILENO) == -1) {
		perror("failed to restore stdout fd");
		return -1;
	}
	close(stdout_fd_bk);

	return 0;
}

/* function to execute for cleanup */
static void clean(const char *fifo)
{
	int i;
	extern int iodev_fd;

	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		if (is_valid_port(i))
			vtss_port_state_set(NULL, i, FALSE);

	/* Cleanup PHY setup */
	phy_cleanup();

	close(iodev_fd);

	/* in the case we have a fifo for input */
	if (fifo != NULL) {
		close(STDIN_FILENO);
		unlink(fifo);
	}
	printf("exiting...\n");

	/* If we have an output file */
	fsync(STDOUT_FILENO);
	close(STDOUT_FILENO);
}

static void print_usage(const char *app_name)
{
	printf("Usage: %s\n", app_name);
	printf("       %s -i <fifo_name>\n", app_name);
	printf("       %s -i <fifo_name> -o <output_file>\n", app_name);
}

static void parse_args(const int argc, const char **argv)
{
	int fifo_fd, output_fd;

	/* if no parameters are sent */
	if (argc == 1)
		return;

	/* only 2 or 4 parameters are accepted for now*/
	if (argc != 3 && argc != 5) {
		print_usage(argv[0]);
		exit(EXIT_FAILURE);
	}
	if (strcmp(argv[1], "-i") != 0 ||
	    (argc == 5 && strcmp(argv[3], "-o") != 0)) {
		print_usage(argv[0]);
		exit(EXIT_FAILURE);
	}

	/* create the input fifo */
	if (mkfifo(argv[2], 0666)) {
		printf("fifo is %s\n", argv[2]);
		perror("Fifo not created\n");
		exit(EXIT_FAILURE);
	}

	/* open the fifo for reading
	 * we also open for writing to always have writing descriptor */
	fifo_fd = open(argv[2], O_RDWR|O_NONBLOCK);
	if (fifo_fd == -1) {
		perror("Cannot open fifo\n");
		unlink(argv[2]);
		exit(EXIT_FAILURE);
	}

	/* redirect stdin to be the fifo descriptor*/
	close(STDIN_FILENO);
	if (dup(fifo_fd) == -1) {
		perror("Cannot duplicate fifo descriptor\n, exiting");
		close(fifo_fd);
		unlink(argv[2]);
		exit(EXIT_FAILURE);
	}

	close(fifo_fd);
	printf("fifo %s is used for stdin\n", argv[2]);
	if (argc == 5) {
		output_fd = open(argv[4], O_CREAT|O_RDWR|O_TRUNC, 0666);
		if (output_fd == -1) {
			perror("Cannot create output file\n");
			return;
		}
		printf("stdout is redirected to %s\n", argv[4]);
		/* redirect stdout to be the output file*/
		close(STDOUT_FILENO);
		if (dup(output_fd) == -1) {
			perror("Cannot duplicate output file descriptor\n");
			close(output_fd);
			unlink(argv[4]);
			unlink(argv[2]);
			exit(EXIT_FAILURE);
		}
		close(output_fd);
	}
}

/* clean buff before parsing parameters */
static int process_input(char *buff)
{
	if ((buff != NULL) && (strlen(buff) > 1)) {
		if (buff[strlen(buff)-1] == '\n')
			buff[strlen(buff)-1] = '\0';

		/* parse exit/quit command */
		if (strcmp(buff, "exit") == 0 || strcmp(buff, "quit") == 0)
			return EXIT_INDICATION;

		/* check for polling commands */
		if (strcmp(buff, "poll on") == 0) {
			/* log the command */
			command_log_add(buff);

			if (timeout) {
				printf("polling is already turned on\n");
				return !EXIT_INDICATION;
			}

			/* poll on "timeout" interval */
			timeout = malloc(sizeof(*timeout));
			if (!timeout) {
				perror("Failed to allocate timeout struct\n");
				return EXIT_INDICATION;
			}
			timeout->tv_sec = TIMEOUT_SEC;
			timeout->tv_nsec = TIMEOUT_NSEC;
			printf("polling on every %lu second(s)\n",
					timeout->tv_sec);

			return !EXIT_INDICATION;
		}
		if (strcmp(buff, "poll off") == 0) {
			/* log the command */
			command_log_add(buff);

			if (!timeout) {
				printf("polling is already turned off\n");
				return !EXIT_INDICATION;
			}
			free(timeout);
			timeout = NULL;
			printf("polling disabled\n");

			return !EXIT_INDICATION;
		}

		if (strcmp(buff, "check") == 0) {
			if (!check_for_errors()) {
				printf("No dropped or invalid frames reported\n");
				return !EXIT_INDICATION;
			}
			printf("Errors have been found. Creating log file...\n");
			int stdout_fd_bk;
			if (debug_log_init(&stdout_fd_bk)) {
				printf("failed to create the log file\n");
				return !EXIT_INDICATION;
			}
			debug_log_dump();
			if (debug_log_close(stdout_fd_bk)) {
				printf("failed to close the log file\n");
				return !EXIT_INDICATION;
			}
			printf("Debug file dumpped\n");

			return !EXIT_INDICATION;
		}
		if (process_command(buff))
			printf("Unknown command or something bad happened!\n");
		else if (strcmp(buff, "reset output"))
			/* log the command */
			command_log_add(buff);
	}
	return !EXIT_INDICATION;
}

int main(int argc, const char **argv)
{
	vtss_inst_create_t		create;
	vtss_inst_t			inst;
	vtss_trace_conf_t		conf;
	vtss_trace_layer_t		layer;
	vtss_trace_group_t		group;

	int			port_no, rc;
	char			*buff;
	fd_set			readfds;
	short 			stop_program = 0;
	int			fd_npi;
	int			max_fd;
	u32			xtr_grp_cfg_old, inj_grp_cfg_old;

	parse_args(argc, argv);
	buff = malloc(255);
	if (!buff) {
		perror("Failed to allocate buff\n");
		goto __cleanup_return;
	}

	for (group = 0; group < VTSS_TRACE_GROUP_COUNT; group++) {
		(void)vtss_trace_conf_get(group, &conf);
		for (layer = 0; layer < VTSS_TRACE_LAYER_COUNT; layer++)
			conf.level[layer] = VTSS_TRACE_LEVEL_ERROR;
		(void)vtss_trace_conf_set(group, &conf);
	}

	vtss_inst_get(VTSS_TARGET_SEVILLE, &create);
	vtss_inst_create(&create, &inst);

	board_init();

	for (port_no = VTSS_PORT_NO_START; port_no < VTSS_PORT_NO_END; port_no++)
		if (is_valid_port(port_no)) {
			port_setup_init(port_no, 0);
			vtss_port_state_set(NULL, port_no, TRUE);
			vtss_mac_table_port_flush(NULL, port_no);
		}

	vlan_init();
	dscp_init();

	(void)ftruncate(STDOUT_FILENO, 0);
	(void)lseek(STDOUT_FILENO, 0, SEEK_SET);

	/* by default, polling is enabled and is done on "timeout" interval */
	timeout = malloc(sizeof(*timeout));
	if (!timeout) {
		perror("Failed to allocate timeout struct\n");
		goto __cleanup;
	}
	timeout->tv_sec = TIMEOUT_SEC;
	timeout->tv_nsec = TIMEOUT_NSEC;

	/* Init the command logs array */
	command_log_init();

	/* disable buffering */
	setbuf(stdout, NULL);

	/* init prompt */
	memset(prompt, '\0', PROMPT_LEN_MAX);
	memcpy(prompt, PROMPT_DEFAULT, strlen(PROMPT_DEFAULT));
	printf("%s", prompt);

	if (times(&init_cpu_time) == (clock_t)-1) {
		perror("Failed to get CPU init time for current process");
		init_cpu_time.tms_utime = 0;
		init_cpu_time.tms_stime = 0;
		init_cpu_time.tms_cutime = 0;
		init_cpu_time.tms_cstime = 0;
	}

	l2sw_reg_read(0, XTR_GRP_CFG_REG, &xtr_grp_cfg_old);
	l2sw_reg_read(0, INJ_GRP_CFG_REG, &inj_grp_cfg_old);
	/* Try to open NPI char device to use the cache-line support for
	 * CPU Extraction of control frames */
	fd_npi = open(NPI_CDEV, O_RDWR);
	if (fd_npi < 0) {
		perror(NPI_CDEV" not found. Using VTSS API for CPU frames\n");

		/* TODO: Remove these when changed in VTSS driver */
		l2sw_reg_write(0, XTR_GRP_CFG_REG, xtr_grp_cfg_old &
				~(CPU_FRAME_BYTE_SWAP |
						CPU_FRAME_EOF_WORD_POS_AFTER));
		l2sw_reg_write(0, INJ_GRP_CFG_REG, inj_grp_cfg_old &
				~(CPU_FRAME_BYTE_SWAP));
	} else {
		/* NPI uses HW Byte swapping and a different position for EOF */
		l2sw_reg_write(0, XTR_GRP_CFG_REG, xtr_grp_cfg_old |
				CPU_FRAME_BYTE_SWAP |
				CPU_FRAME_EOF_WORD_POS_AFTER);
		l2sw_reg_write(0, INJ_GRP_CFG_REG, inj_grp_cfg_old |
				CPU_FRAME_BYTE_SWAP);
	}

	/* main loop */
	while (!stop_program) {
		FD_ZERO(&readfds);
		FD_SET(STDIN_FILENO, &readfds);
		if (fd_npi >= 0 && receive_control_frames) {
			FD_SET(fd_npi, &readfds);
			max_fd = fd_npi;
		} else
			max_fd = STDIN_FILENO;

		/* wait for input with timeout */
		rc = pselect(max_fd + 1, &readfds,
				NULL, NULL, timeout, NULL);

		if (rc == -1) {
			perror("select error");
			break;
		} else if (rc && FD_ISSET(STDIN_FILENO, &readfds)) {
			/* process input */
			memset(buff, 0, 255);
			while (fgets(buff, 255, stdin) != NULL) {
				if (process_input(buff) == EXIT_INDICATION) {
					stop_program = 1;
					break;
				}

				memset(buff, 0, 255);
				printf("%s", prompt);

				/*FIXME: Hack for stdin input */
				/* Only one command per line */
				if (argc < 3)
					break;
			}
		} else if (receive_control_frames) {
			if (rc && fd_npi >= 0 && FD_ISSET(fd_npi, &readfds)) {
				/* will use NPI to read control frames */
				control_traffic_recv(fd_npi, 1);
			} else {
				/* will use VTSS api to read control frames */
				control_traffic_recv(-1, CONTROL_FRAMES_RECV);
			}
		} else {
			/* timeout waiting for input - process stuff */
			port_polling();
		}
	}

	/* restore CPU injection/extraction configuration */
	l2sw_reg_write(0, XTR_GRP_CFG_REG, xtr_grp_cfg_old);
	l2sw_reg_write(0, INJ_GRP_CFG_REG, inj_grp_cfg_old);

	if (fd_npi >= 0)
		close(fd_npi);

__cleanup:
	/* do cleanup */
	(argc >= 3) ? clean(argv[2]) : clean(NULL);
	if (timeout)
		free(timeout);
	command_log_release_all();
	free(buff);

__cleanup_return:
	return 0;
}
