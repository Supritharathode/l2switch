/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#ifndef _MAIN_H_
#define _MAIN_H_

#include <fsl_utils/fsl_utils.h>
#include <board_init/board_init.h>
#include <port_setup/port_setup.h>
#include <cli/cli.h>

#endif /* _MAIN_H_ */
