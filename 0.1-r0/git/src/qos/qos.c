/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

/*
 * qos.h
 */

#include "qos.h"

#define ENTRY_ID	1
#define PRIO		6
#define PROTO_VALUE	0x01
#define PROTO_MASK	0xFF
#define INGRESS_PORT	0

/* set storm control rate
 * use VTSS_PACKET_RATE_DISABLED to disable */
int storm_control_set(enum storm_control_type type, vtss_packet_rate_t rate)
{
	vtss_qos_conf_t    conf;

	if (vtss_qos_conf_get(NULL, &conf) != VTSS_RC_OK) {
		printf("Cannot get qos configuration\n");
		return 1;
	}

	switch (type) {
	case STORM_CONTROL_UC:
		conf.policer_uc = rate;
		break;
	case STORM_CONTROL_MC:
		conf.policer_mc = rate;
		break;
	case STORM_CONTROL_BC:
		conf.policer_bc = rate;
		break;
	default:
		printf("Unknown storm control type\n");
	}

	if (vtss_qos_conf_set(NULL, &conf) != VTSS_RC_OK) {
		printf("Cannot set qos configuration\n");
		return 1;
	}

	return 0;
}

/* get storm control rate */
int storm_control_get(enum storm_control_type type, vtss_packet_rate_t *rate)
{
	vtss_qos_conf_t    conf;

	if (vtss_qos_conf_get(NULL, &conf) != VTSS_RC_OK) {
		printf("Cannot get qos configuration\n");
		return 1;
	}

	switch (type) {
	case STORM_CONTROL_UC:
		*rate = conf.policer_uc;
		break;
	case STORM_CONTROL_MC:
		*rate = conf.policer_mc;
		break;
	case STORM_CONTROL_BC:
		*rate = conf.policer_bc;
		break;
	default:
		printf("Unknown storm control type\n");
	}

	return 0;
}

/* show all storm control configuration */
void storm_control_show(void)
{
	vtss_packet_rate_t rate_uc, rate_mc, rate_bc;

	if (storm_control_get(STORM_CONTROL_UC, &rate_uc))
		return;
	if (storm_control_get(STORM_CONTROL_MC, &rate_mc))
		return;
	if (storm_control_get(STORM_CONTROL_BC, &rate_bc))
		return;

	printf("Storm control\t\tRate\n");
	printf("unicast       \t\t");
	if (rate_uc == VTSS_PACKET_RATE_DISABLED)
		printf("off\n");
	else
		printf("%u\n", rate_uc);

	printf("multicast    \t\t");
	if (rate_mc == VTSS_PACKET_RATE_DISABLED)
		printf("off\n");
	else
		printf("%u\n", rate_mc);

	printf("broadcast     \t\t");
	if (rate_bc == VTSS_PACKET_RATE_DISABLED)
		printf("off\n");
	else
		printf("%u\n", rate_bc);
}

/* PCP functions */

/* set egress remark mode for PCP port */
int port_pcp_remark_emode_set(int port_no, vtss_tag_remark_mode_t emode)
{
	vtss_qos_port_conf_t  conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_qos_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't get port qos conf\n");
		return 1;
	}

	conf.tag_remark_mode = emode;

	if (vtss_qos_port_conf_set(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't set port qos conf\n");
		return 1;
	}

	return 0;
}

/* get egress remark mode for PCP port */
int port_pcp_remark_emode_get(int port_no, vtss_tag_remark_mode_t *emode)
{
	vtss_qos_port_conf_t  conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_qos_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't get port qos conf\n");
		return 1;
	}

	*emode = conf.tag_remark_mode;

	return 0;
}

/* set egress default PCP value used for remarking */
int port_pcp_egress_default_set(int port_no, vtss_tagprio_t pcp)
{
	vtss_qos_port_conf_t  conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_qos_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't get port qos conf\n");
		return 1;
	}

	conf.tag_default_pcp = pcp;

	if (vtss_qos_port_conf_set(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't set port qos conf\n");
		return 1;
	}

	return 0;
}

/* get egress default PCP value used for remarking */
int port_pcp_egress_default_get(int port_no, vtss_tagprio_t *pcp)
{
	vtss_qos_port_conf_t  conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_qos_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't get port qos conf\n");
		return 1;
	}

	*pcp = conf.tag_default_pcp;

	return 0;
}

/* set egress mapping of QoS and DP to PCP */
int port_pcp_egress_map_set(int		port_no,
		vtss_prio_t		qos,
		vtss_dp_level_t		dp,
		vtss_tagprio_t		pcp)
{
	vtss_qos_port_conf_t  conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_qos_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't get port qos conf\n");
		return 1;
	}

	conf.tag_pcp_map[qos][dp] = pcp;

	if (vtss_qos_port_conf_set(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't set port qos conf\n");
		return 1;
	}

	return 0;
}

/* get egress mapping of QoS and DP to PCP */
int port_pcp_egress_map_get(int		port_no,
		vtss_prio_t		qos,
		vtss_dp_level_t		dp,
		vtss_tagprio_t		*pcp)
{
	vtss_qos_port_conf_t  conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_qos_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't get port qos conf\n");
		return 1;
	}

	*pcp = conf.tag_pcp_map[qos][dp];

	return 0;
}

/* DSCP functions */

/* clear all previous configuration of dscp */
void dscp_init(void)
{
	int		port;
	vtss_dscp_t	i;

	for (port = VTSS_PORT_NO_START; port < VTSS_PORT_NO_END; port++)
		if (is_valid_port(port)) {
			port_dscp_remark_imode_set(port,
					VTSS_DSCP_MODE_NONE);
			port_dscp_remark_emode_set(port,
					VTSS_DSCP_EMODE_DISABLE);
			port_dscp_translate_set(port, FALSE);
			port_qos_dscp_set(port, FALSE);
			port_pcp_remark_emode_set(port,
					VTSS_TAG_REMARK_MODE_CLASSIFIED);
			port_pcp_egress_default_set(port, 0);
			for (i = 0; i < 8; i++) {
				port_pcp_egress_map_set(port, i, 0, 0);
				port_pcp_egress_map_set(port, i, 1, 0);
			}
		}

	for (i = 0; i < 64; i++) {
		dscp_translate_map(i, i);
		dscp_remap_set(0, i, i);
		dscp_remap_set(1, i, i);
		dscp_trust_set(i, FALSE);
		qos_dscp_map(i, 0);
	}
}

/* enable/disable DSCP remarking for all DSCP values */
int dscp_remark_ingress_val_set(vtss_dscp_t dscp, BOOL enable)
{
	vtss_qos_conf_t conf;

	if (vtss_qos_conf_get(NULL, &conf) != VTSS_RC_OK) {
		printf("Can't get chip qos conf\n");
		return 1;
	}

	conf.dscp_remark[dscp] = enable;

	if (vtss_qos_conf_set(NULL, &conf) != VTSS_RC_OK) {
		printf("Can't set chip qos conf\n");
		return 1;
	}

	return 0;
}

/* set DSCP remark ingress mode for port */
int port_dscp_remark_imode_set(int port_no, vtss_dscp_mode_t mode)
{
	vtss_qos_port_conf_t  conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_qos_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't get port qos conf\n");
		return 1;
	}

	conf.dscp_mode = mode;

	if (vtss_qos_port_conf_set(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't set port qos conf\n");
		return 1;
	}

	return 0;
}

/* set DSCP remark egress mode for port */
int port_dscp_remark_emode_set(int port_no, vtss_dscp_emode_t mode)
{
	vtss_qos_port_conf_t  conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_qos_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't get port qos conf\n");
		return 1;
	}

	conf.dscp_emode = mode;

	if (vtss_qos_port_conf_set(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't set port qos conf\n");
		return 1;
	}

	return 0;
}

/* enable/disable DSCP ingress translation */
int port_dscp_translate_set(int port_no, BOOL enable)
{
	vtss_qos_port_conf_t  conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_qos_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't get port qos conf\n");
		return 1;
	}

	conf.dscp_translate = enable;

	if (vtss_qos_port_conf_set(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't set port qos conf\n");
		return 1;
	}

	return 0;
}

/* map DSCP at ingress */
int dscp_translate_map(vtss_dscp_t dscp_parsed, vtss_dscp_t dscp_classified)
{
	vtss_qos_conf_t conf;

	if (dscp_parsed >= 64) {
		printf("Invalid DSCP value. Use [0..63]\n");
		return 1;
	}

	if (vtss_qos_conf_get(NULL, &conf) != VTSS_RC_OK) {
		printf("Can't get chip qos conf\n");
		return 1;
	}

	conf.dscp_translate_map[dscp_parsed] = dscp_classified;

	if (vtss_qos_conf_set(NULL, &conf) != VTSS_RC_OK) {
		printf("Can't set chip qos conf\n");
		return 1;
	}

	return 0;
}

/* remap DSCP at egress, depends on classified DP */
int dscp_remap_set(vtss_dp_level_t	dp,
		vtss_dscp_t		dscp,
		vtss_dscp_t		dscp_re)
{
	vtss_qos_conf_t conf;

	if (dscp >= 64 || dscp_re >= 64) {
		printf("Invalid DSCP value. Use [0..63]\n");
		return 1;
	}

	if (vtss_qos_conf_get(NULL, &conf) != VTSS_RC_OK) {
		printf("Can't get chip qos conf\n");
		return 1;
	}

	switch (dp) {
	case 0:
		conf.dscp_remap[dscp] = dscp_re;
		break;
	case 1:
		conf.dscp_remap_dp1[dscp] = dscp_re;
		break;
	default:
		printf("Bad value %d for DP\n", dp);
		return 1;
	}

	if (vtss_qos_conf_set(NULL, &conf) != VTSS_RC_OK) {
		printf("Can't set chip qos conf\n");
		return 1;
	}

	return 0;
}

/* enable/disable QoS classification based on DSCP value */
int port_qos_dscp_set(int port_no, BOOL enable)
{
	vtss_qos_port_conf_t  conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_qos_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't get qos conf for port %d\n", port_no);
		return 1;
	}

	conf.dscp_class_enable = enable;

	if (vtss_qos_port_conf_set(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Can't set qos conf for port %d\n", port_no);
		return 1;
	}

	return 0;
}

/* set trusted DSCP values for QoS classification */
int dscp_trust_set(vtss_dscp_t dscp_trust_val, BOOL enable)
{
	vtss_qos_conf_t conf;

	if (vtss_qos_conf_get(NULL, &conf) != VTSS_RC_OK) {
		printf("Can't get chip qos conf\n");
		return 1;
	}

	conf.dscp_trust[dscp_trust_val] = enable;


	if (vtss_qos_conf_set(NULL, &conf) != VTSS_RC_OK) {
		printf("Can't set chip qos conf\n");
		return 1;
	}

	return 0;
}

int qos_dscp_map(vtss_dscp_t dscp, vtss_prio_t qos)
{
	vtss_qos_conf_t conf;

	if (dscp >= 64) {
		printf("Invalid DSCP value. Use [0..63]\n");
		return 1;
	}

	if (vtss_qos_conf_get(NULL, &conf) != VTSS_RC_OK) {
		printf("Can't get chip qos conf\n");
		return 1;
	}

	conf.dscp_qos_class_map[dscp] = qos;

	if (vtss_qos_conf_set(NULL, &conf) != VTSS_RC_OK) {
		printf("Can't set chip qos conf\n");
		return 1;
	}

	return 0;
}

#define RATE 100

/* configure switch for L2_POLICER tests
 * Each port of the switch will have the ingress policer rate
 * configured @ RATE
 */
int test_L2_POLICER_cfg(bool restore)
{
	vtss_qos_port_conf_t pconf;
	static vtss_bitrate_t init_rate[VTSS_PORT_ARRAY_SIZE] = {
		[VTSS_PORT_NO_START ... VTSS_PORT_NO_END - 1] = VTSS_BITRATE_DISABLED};
	int p;

	for (p = VTSS_PORT_NO_START; p < VTSS_PORT_NO_END; p++) {
		if (vtss_qos_port_conf_get(NULL, p, &pconf) != VTSS_RC_OK) {
			printf("Cannot get port %d qos configuration\n", p);
			return 1;
		}

		/* save initial rate, if necessary */
		/*FIXME: can have multiple policers*/
		if (!restore) {
			init_rate[p] = pconf.policer_port[0].rate;
			pconf.policer_port[0].rate = RATE;
		} else
			pconf.policer_port[0].rate = init_rate[p];

		if (vtss_qos_port_conf_set(NULL, p, &pconf) != VTSS_RC_OK) {
			printf("Cannot set port %d qos configuration\n",
					p);
			return 1;
		}
	}

	printf("\tPolicer %s\n", restore ? "restored" : "configured");
	return 0;
}

/* configure switch for L2_EGR_SHPR tests
 * Each port of the switch will have the egress shaper rate
 * configured @ RATE
 */
int test_L2_EGR_SHPR_cfg(bool restore)
{
	vtss_qos_port_conf_t pconf;
	static vtss_shaper_t shaper[VTSS_PORT_ARRAY_SIZE];

	int p;

	for (p = VTSS_PORT_NO_START; p < VTSS_PORT_NO_END; p++) {
		if (vtss_qos_port_conf_get(NULL, p, &pconf) != VTSS_RC_OK) {
			printf("Cannot get port %d shaper configuration\n", p);
			return 1;
		}

		/* save shaper, if necessary */
		if (!restore) {
			memcpy(&shaper[p], &pconf.shaper_port,
					sizeof(vtss_shaper_t));

			/*FIXME: setup shaper */
			pconf.shaper_port.rate = RATE / 10;
			pconf.shaper_port.level_pwm = 100;
			pconf.shaper_port.rate_pwm_high = 1;
			pconf.shaper_port.eir = VTSS_BITRATE_DISABLED;


		} else
			memcpy(&pconf.shaper_port, &shaper[p],
					sizeof(vtss_shaper_t));

		if (vtss_qos_port_conf_set(NULL, p, &pconf) != VTSS_RC_OK) {
			printf("Cannot set port %d shaper configuration\n",
					p);
			return 1;
		}
	}

	printf("Shaper %s\n", restore ? "restored" : "configured");

	return 0;
}

/* Configures QoS per-port classification based on PCP and DEI
 * Usage:
 *	set qos pcp port <port> <pcp> <dei> <classified_QoS>
 */
int qos_command_pcp(const char *command)
{
	int			port, pcp, dei, qos;
	char			*copy, *tok;
	vtss_qos_port_conf_t	qos_conf;

	copy = strdup(command);

	/* Parse port */
	tok = strtok(copy, " ");
	if (tok == NULL)
		goto qos_cmd_pcp_parse_error;
	port = atoi(tok);
	if (port < 0 || port > 3) {
		printf("ERROR:%s: Wrong port index!\n", __func__);
		goto qos_cmd_pcp_parse_error;
	}

	/* Parse PCP */
	tok = strtok(NULL, " ");
	if (tok == NULL)
		goto qos_cmd_pcp_parse_error;
	pcp = atoi(tok);
	if (pcp < 0 || pcp > 7) {
		printf("ERROR:%s: Wrong PCP value!\n", __func__);
		goto qos_cmd_pcp_parse_error;
	}

	/* Parse DEI */
	tok = strtok(NULL, " ");
	if (tok == NULL)
		goto qos_cmd_pcp_parse_error;
	dei = atoi(tok);
	if (dei < 0 || dei > 1) {
		printf("ERROR:%s: Wrong DEI value!\n", __func__);
		goto qos_cmd_pcp_parse_error;
	}

	/* Parse classified QoS */
	tok = strtok(NULL, " ");
	if (tok == NULL)
		goto qos_cmd_pcp_parse_error;
	qos = atoi(tok);
	if (qos < 0 || qos > 7) {
		printf("ERROR:%s: Wrong QoS value!\n", __func__);
		goto qos_cmd_pcp_parse_error;
	}

	free(copy);

	/* Modify current QoS configuration accordingly */
	if (vtss_qos_port_conf_get(NULL, port, &qos_conf) != VTSS_RC_OK) {
		printf("ERROR:%s: Unable to get QoS port conf!\n",
				__func__);
		return -1;
	}
	qos_conf.tag_class_enable = TRUE;
	qos_conf.qos_class_map[pcp][dei] = qos;
	if (vtss_qos_port_conf_set(NULL, port, &qos_conf) != VTSS_RC_OK) {
		printf("ERROR:%s: Unable to set QoS port conf!\n",
				__func__);
		return -1;
	}

	return 0;

qos_cmd_pcp_parse_error:
	free(copy);
	return -1;
}

/* Configures per-port default QoS classification
 * Usage:
 *	set qos default port <port> <classified_QoS>
 */
int qos_command_default(const char *command)
{
	int			port, qos;
	vtss_qos_port_conf_t	qos_conf;
	char			*copy, *tok;

	copy = strdup(command);

	/* Parse port */
	tok = strtok(copy, " ");
	if (tok == NULL)
		goto qos_cmd_default_parse_error;
	port = atoi(tok);
	if (port < 0 || port > 3) {
		printf("ERROR:%s: Wrong port index!\n", __func__);
		goto qos_cmd_default_parse_error;
	}

	/* Parse classified QoS */
	tok = strtok(NULL, " ");
	if (tok == NULL)
		goto qos_cmd_default_parse_error;
	qos = atoi(tok);
	if (qos < 0 || qos > 7) {
		printf("ERROR:%s: Wrong QoS value!\n", __func__);
		goto qos_cmd_default_parse_error;
	}

	free(copy);

	/* Modify current QoS configuration */
	if (vtss_qos_port_conf_get(NULL, port, &qos_conf) != VTSS_RC_OK) {
		printf("ERROR:%s: Unable to get QoS port conf!\n",
				__func__);
		return -1;
	}
	qos_conf.default_prio = qos;
	if (vtss_qos_port_conf_set(NULL, port, &qos_conf) != VTSS_RC_OK) {
		printf("ERROR:%s: Unable to set QoS port conf!\n",
				__func__);
		return -1;
	}

	return 0;

qos_cmd_default_parse_error:
	free(copy);
	return -1;
}

int test_L2_QCL_1(bool restore)
{
	vtss_qce_t	entry;

	/* Initialize the entry to default values. */
	if (vtss_qce_init(NULL, VTSS_QCE_TYPE_IPV4, &entry) != VTSS_RC_OK) {
		printf("ERROR:%s:%d:Unable to initialize entry!\n",
				__func__, __LINE__);
		return -1;
	}

	/* Define an ID for this entry */
	entry.id = ENTRY_ID;

	/* Define the key: match against ICMP packets */
	entry.key.frame.ipv4.proto.value = PROTO_VALUE;
	entry.key.frame.ipv4.proto.mask = PROTO_MASK;

	/* Define the ingress ports */
	entry.key.port_list[INGRESS_PORT] = TRUE;

	/* Define the action: assign QoS priority PRIOto all packets */
	entry.action.prio_enable = TRUE;
	entry.action.prio = PRIO;

	/* Add this entry to a QCL. */
	if (vtss_qce_add(NULL, VTSS_QCL_ID_START, VTSS_QCE_ID_LAST, &entry) !=
			VTSS_RC_OK) {
		printf("%s:%d:Unable to add a new entry!\n",
				__func__, __LINE__);
		return -1;
	}

	return 0;
}
