/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#include "lag.h"

static void print_aggr_port_membership(int aggr_group);

/* Link Aggregation related commands
 *
 * Variants:
 * set lag mode - enables all contributions
 * set lag no <aggr number> members <port list>
 *
 * Note: if port list is empty, the aggregation group
 * will also be empty
 */
int lag_command(const char *command)
{
	vtss_aggr_mode_t	mode;
	vtss_aggr_no_t		aggr_group = -1;
	char *copy, *tok;
	int i = 0, port;
	BOOL member[VTSS_PORT_ARRAY_SIZE];

	/* Clear the member list */
	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		if (is_valid_port(i))
			member[i] = FALSE;

	if (strstr(command, "mode") != NULL) {
		if (vtss_aggr_mode_get(NULL, &mode) != VTSS_RC_OK) {
			printf("Unable to get aggregation mode!");
			return -1;
		}

		/* Enable all contributions */
		mode.smac_enable = TRUE;
		mode.dmac_enable = TRUE;
		mode.sip_dip_enable = TRUE;
		mode.sport_dport_enable = TRUE;

		if (vtss_aggr_mode_set(NULL, &mode) != VTSS_RC_OK) {
			printf("Unable to set aggregation mode!");
			return -1;
		}
	} else if (strstr(command, "no ") != NULL) {
		command += strlen("no ");
		copy = strdup(command);
		for (tok = strtok(copy, " "), i = 0;
				tok != NULL;
				tok = strtok(NULL, " "), i++) {
			if (i == 0)
				/* Parse aggregation number */
				aggr_group = atoi(tok);
			else if (i == 1)
				/* Skip over "members" keyword */;
			else { /* Parse member */
				port = atoi(tok);
				if (is_valid_port(port))
					member[port] = TRUE;
				else {
					printf("Invalid port number.\n");
					free(copy);
					return -1;
				}
			}
		}
		if (vtss_aggr_port_members_set(NULL, aggr_group,
					member) != VTSS_RC_OK) {
			printf("Unable to set aggregation port members!\n");
			free(copy);
			return -1;
		}
		print_aggr_port_membership(aggr_group);
		free(copy);
	}
	return 0;
}

static void print_aggr_port_membership(int aggr_group)
{
	BOOL member[VTSS_PORT_ARRAY_SIZE];
	int i;

	if (vtss_aggr_port_members_get(NULL, aggr_group,
				member) != VTSS_RC_OK) {
		printf("Unable to get aggregation port members!\n");
		return ;
	}
	printf("\tLAG NO %d members: ", aggr_group);
	for (i = 0; i < VTSS_PORT_NO_END; i++) {
		if (is_valid_port(i) &&
		    member[i] == TRUE)
			printf("%d ", i);
	}
	printf("\n");
}
