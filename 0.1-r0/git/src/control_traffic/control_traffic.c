/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#include "control_traffic.h"

#define CONTROL_FRAME_MAX_SIZE	2000
#define CONTROL_FRAME_HEADER_SIZE	16

#define CPU_CONTROL_PORT_DEFAULT	10

#define HEADER_BIT_SET(array, offset, bit) \
	array[15 - (offset) / 8] |= ((u8)(1 << (bit)) << ((offset) % 8))

#define HEADER_BIT_CHUNK_SET(array, offset, val) \
	array[15 - (offset) / 8] |= ((u8)(val) << ((offset) % 8))

#define HEADER_BIT_CHUNK_CLEAR(array, offset, size) \
	array[15 - (offset) / 8] &= ~(((u8)(1 << (size)) - 1) << ((offset) % 8))

#define HEADER_BIT_CHUNK_GET(array, offset, size) \
	(u8)((array[15 - (offset) / 8] >> ((offset) % 8)) & ((1 << size) - 1))

#define ETHTPE_C_TAG	0x8100
#define ETHTPE_S_TAG	0x88A8

/* Global variable counting received control traffic frames */
static u32	received_control_frames;
static short	dump_control_frames;
static short	reflect_control_frames_port = -1;
short		receive_control_frames = 0;

/*
 * Process 'port_npi' command parameters
 * Command options: get | set <port_no>
 */
int port_npi_command(const char *command)
{
	int port = -1;
	vtss_npi_conf_t npi_conf;
	vtss_rc rc;

#if DEBUG
	printf("DEBUG: %s(%s)\n", __func__, command);
#endif

	if (strncmp(command, "get", strlen("get")) == 0) {
		/* get NPI port info */
		rc = vtss_npi_conf_get(NULL, &npi_conf);
		if (rc == VTSS_RC_OK)
			printf("NPI port %d is %s\n", npi_conf.port_no,
				npi_conf.enable ? "enabled" : "disabled");
		else {
			printf("Error calling vtss_npi_conf_get (%d)", rc);
			return -1;
		}
	} else if (strncmp(command, "set ", strlen("set ")) == 0) {
		/* set NPI port */
		command += strlen("set ");
		if (command)
			port = atoi(command);
		if (port >= VTSS_PORT_NO_START &&
		    port < VTSS_PORT_NO_END &&
		    is_valid_port(port)) {
			/* call set */
			npi_conf.port_no = port;
			npi_conf.enable = TRUE;
			rc = vtss_npi_conf_set(NULL, &npi_conf);
			if (rc != VTSS_RC_OK) {
				printf("Error calling %s(%d)",
					__func__, rc);
				return -1;
			}
		}
	} else {
		printf("Error: Incorrect command\n");
		printf("Usage: port_npi get | set <port_no>\n");
		return -1;
	}
	return 0;
}

/*
 * Print vtss_packet_rx_conf_t struct content
 */
static void print_rx_conf(const vtss_packet_rx_conf_t *rx_conf)
{
	int i;

	if (!rx_conf)
		return;
	for (i = VTSS_PACKET_RX_QUEUE_START;
			i < VTSS_PACKET_RX_QUEUE_END; i++) {
		printf("Queue[%d] size: %4d enabled:%d\n",
				i,
				rx_conf->queue[i].size,
				rx_conf->queue[i].npi.enable);
	}

	printf("Registration\n");
	printf("\tBPDU    : %d\n", rx_conf->reg.bpdu_cpu_only);
	printf("\tGARP    : ");
	for (i = 0; i < 16; i++)
		printf("%d", rx_conf->reg.garp_cpu_only[i]);
	printf("\n");
	printf("\tIGMP    : %d\n", rx_conf->reg.igmp_cpu_only);
	printf("\tIPMC cpy: %d\n", rx_conf->reg.ipmc_ctrl_cpu_copy);
	printf("\tMLD     : %d\n", rx_conf->reg.mld_cpu_only);

	printf("Queue map:\n");
	printf("\tBPDU    : %d\n", rx_conf->map.bpdu_queue);
	printf("\tGARP    : %d\n", rx_conf->map.garp_queue);
	printf("\tLEARN   : %d\n", rx_conf->map.learn_queue);
	printf("\tIGMP    : %d\n", rx_conf->map.igmp_queue);
	printf("\tIPMC    : %d\n", rx_conf->map.ipmc_ctrl_queue);
	printf("\tMAC_VID : %d\n", rx_conf->map.mac_vid_queue);
	printf("\tSTACK   : %d\n", rx_conf->map.stack_queue);
	printf("\tSFLOW   : %d\n", rx_conf->map.sflow_queue);
	printf("\tLRN ALL : %d\n", rx_conf->map.lrn_all_queue);
}

/*
 * Print control traffic Rx configuration
 */
static int control_traffic_conf_print(void)
{
	vtss_packet_rx_conf_t rx_conf;
	vtss_rc rc;

	rc = vtss_packet_rx_conf_get(NULL, &rx_conf);
	if (rc != VTSS_RC_OK) {
		printf("Error calling %s(%d)",
				__func__, rc);
		return -1;
	}
	print_rx_conf(&rx_conf);

	printf("\nReceive control traffic: %s\n",
			receive_control_frames ? "on" : "off");
	printf("Verbose traffic recv   : %s\n",
			dump_control_frames ? "on" : "off");

	if (reflect_control_frames_port >= VTSS_PORT_NO_START &&
	    reflect_control_frames_port < VTSS_PORT_NO_END &&
	    is_valid_port(reflect_control_frames_port))
		printf("Reflect traffic on port: %d\n",
				reflect_control_frames_port);
	return 0;
}

/*
 * Modify control traffic Rx configuration - protocols
 */
static int control_traffic_conf_modify(const char *command)
{
	vtss_packet_rx_conf_t rx_conf;
	vtss_rc rc;
	short status = DISABLED; /* disabled */
	int i;

	rc = vtss_packet_rx_conf_get(NULL, &rx_conf);
	if (rc != VTSS_RC_OK) {
		printf("Error calling %s(%d)",
				__func__, rc);
		return -1;
	}

	if (strncmp(command, "ena ", strlen("ena ")) == 0)
		status = ENABLED;

	if (strstr(command, "bpdu"))
		rx_conf.reg.bpdu_cpu_only = status;
	else if (strstr(command, "garp"))
		for (i = 0; i < 16; i++)
			rx_conf.reg.garp_cpu_only[i] = status;
	else if (strstr(command, "igmp"))
		rx_conf.reg.igmp_cpu_only = status;
	else if (strstr(command, "ipmc"))
		rx_conf.reg.ipmc_ctrl_cpu_copy = status;
	else if (strstr(command, "mld"))
		rx_conf.reg.mld_cpu_only = status;
	else {
		printf("Unknown protocol\n");
		return -1;
	}

	rc = vtss_packet_rx_conf_set(NULL, &rx_conf);
	if (rc != VTSS_RC_OK) {
		printf("Error calling %s(%d)",
				__func__, rc);
		return -1;
	}

	return 0;
}

/* Dump frame */
static void dump_frame(vtss_packet_rx_header_t	*header,
		vtss_packet_rx_queue_t	queue,
		u8 *frame)
{
	char	buf[100], *p;
	u32	i;

	printf("Received frame on port: %u, queue: %u, length: %u\n",
			header->port_no, queue, header->length);
	for (i = 0, p = &buf[0]; i < header->length; i++) {
		if ((i % 16) == 0) {
			p = &buf[0];
			p += sprintf(p, "%04x: ", i);
		}

		p += sprintf(p, "%02x%c", frame[i],
					((i+9)%16) == 0 ? '-' : ' ');
		if (((i+1) % 16) == 0 || (i+1) == header->length)
					printf("%s\n", buf);
	}
	printf("\n");
}

/* decode the CPU extraction header from the control frame */
static void extraction_header_decode(vtss_packet_rx_header_t *header,
		u8 frame[CONTROL_FRAME_MAX_SIZE + CONTROL_FRAME_HEADER_SIZE])
{
	u8 wlen, llen;
	u16 ethtype;

	wlen = HEADER_BIT_CHUNK_GET(frame, 72, 8);
	llen = HEADER_BIT_CHUNK_GET(frame, 80, 6);
	header->length =  60 * wlen + llen - 80;

	/* Exclude FCS when calculating frame size*/
	header->length -= 4;

	header->tag.vid = HEADER_BIT_CHUNK_GET(frame, 0, 8) +
			(HEADER_BIT_CHUNK_GET(frame, 8, 4) << 8);
	header->tag.cfi = HEADER_BIT_CHUNK_GET(frame, 12, 1);
	header->tag.tagprio = HEADER_BIT_CHUNK_GET(frame, 13, 3);
	header->queue_mask = HEADER_BIT_CHUNK_GET(frame, 17, 3);
	header->learn = ((HEADER_BIT_CHUNK_GET(frame, 28, 2)) ? TRUE : FALSE);
	header->port_no = HEADER_BIT_CHUNK_GET(frame, 43, 4);

	ethtype = (frame[16 + 12] << 8) + frame[16 + 13];
	header->arrived_tagged = ((ethtype == ETHTPE_C_TAG ||
			ethtype == ETHTPE_S_TAG) ? TRUE : FALSE);
}

/* add the CPU injection header in the control frame */
static void injection_header_encode(int port,
		u8 frame[CONTROL_FRAME_MAX_SIZE + CONTROL_FRAME_HEADER_SIZE])
{
	int i;

	/* clear CPU queues */
	HEADER_BIT_CHUNK_CLEAR(frame, 20, 4);
	HEADER_BIT_CHUNK_CLEAR(frame, 24, 4);

	/* pop count; not tags must be popped but update the FCS */
	HEADER_BIT_CHUNK_SET(frame, 28, 3);

	/* src port */
	HEADER_BIT_CHUNK_CLEAR(frame, 43, 4);
	HEADER_BIT_CHUNK_SET(frame, 43, CPU_CONTROL_PORT_DEFAULT);

	/* dest port(s); CPU port */
	HEADER_BIT_CHUNK_CLEAR(frame, 57, 7);
	HEADER_BIT_CHUNK_CLEAR(frame, 64, 4);
	if (port < 7)
		HEADER_BIT_SET(frame, 57, port);
	else
		HEADER_BIT_SET(frame, 64, port - 7);

	/* no rewriting */
	HEADER_BIT_CHUNK_CLEAR(frame, 118, 2);
	HEADER_BIT_CHUNK_CLEAR(frame, 120, 7);

	/* enable bypass */
	HEADER_BIT_SET(frame, 127, 0);

	/* clear reserved bits from header*/
	HEADER_BIT_CHUNK_CLEAR(frame, 31, 1);
	HEADER_BIT_CHUNK_CLEAR(frame, 32, 8);
	HEADER_BIT_CHUNK_CLEAR(frame, 40, 3);
	HEADER_BIT_CHUNK_CLEAR(frame, 47, 1);
	HEADER_BIT_CHUNK_CLEAR(frame, 48, 8);
	HEADER_BIT_CHUNK_CLEAR(frame, 56, 1);
	HEADER_BIT_CHUNK_CLEAR(frame, 68, 4);
	for (i = 72; i <= 104; i += 8)
		HEADER_BIT_CHUNK_CLEAR(frame, i, 8);
	HEADER_BIT_CHUNK_CLEAR(frame, 112, 6);

	/* The rest of the values are kept from
	 * the previous extracted control frame */
}

/*
 * Receive at most <count> frames
 */
void control_traffic_recv(int fd_npi, u32 count)
{
	vtss_packet_rx_queue_t	queue;
	vtss_packet_rx_header_t	header;
	u8			frame[CONTROL_FRAME_MAX_SIZE +
	  			      CONTROL_FRAME_HEADER_SIZE];
	u32			frames = 0;
	int			length_tx, length_rx;
	short			nothing_received;

	while (count > frames) {
		nothing_received = 1;
		for (queue = VTSS_PACKET_RX_QUEUE_START;
				queue < VTSS_PACKET_RX_QUEUE_END; queue++) {
			if (fd_npi >= 0) {
				/* NPI extracts a frame independent of queue */
				queue = VTSS_PACKET_RX_QUEUE_END;

				/* Read the frame using NPI */
				length_rx = read(fd_npi, frame,
						sizeof(frame));
				if (length_rx <= 0)
					continue;

				/* we need to extract the 128b header */
				if (length_rx < CONTROL_FRAME_HEADER_SIZE)
					continue;

				extraction_header_decode(&header, frame);
			} else {
				/* Read the frame using VTSS API */
				if (vtss_packet_rx_frame_get(NULL, queue,
						&header, frame,
						CONTROL_FRAME_MAX_SIZE) !=
								VTSS_RC_OK)
					continue;
			}
			frames++;
			received_control_frames++;

			nothing_received = 0;

			if (dump_control_frames) {
				if (fd_npi >= 0)
					dump_frame(&header, 0, frame +
							CONTROL_FRAME_HEADER_SIZE);
				else
					dump_frame(&header, queue, frame);
			}

			if (is_valid_port(reflect_control_frames_port)) {
				if (fd_npi >= 0) {
					injection_header_encode(reflect_control_frames_port,
							frame);
					length_tx = write(fd_npi, frame,
							header.length + CONTROL_FRAME_HEADER_SIZE);
					if (length_tx != header.length + CONTROL_FRAME_HEADER_SIZE)
						printf("Failed to send all the data. Sent only %d B from %d B\n",
								length_tx,
								header.length +
								4 +
								CONTROL_FRAME_HEADER_SIZE);
				} else
					vtss_packet_tx_frame_port(NULL,
							reflect_control_frames_port,
							frame, header.length);
			}
		}

		if (nothing_received)
			break;
	}
}

/*
 * Process 'control traffic' command parameters
 * Command options:
 * conf - Display Rx configuration
 * stats [clear] - Display/clear control traffic frame counter
 * verbose [off] - Dump control traffic info
 * receive [off] - Receive control traffic
 * reflect <port_no> - Reflect control traffic on port
 * ena|dis <bpdu|garp|igmp|ipmc|mld> - enable/disable specified protocol
 */
int control_traffic_command(const char *command)
{
	if (strncmp(command, "conf", strlen("conf")) == 0) {
		return control_traffic_conf_print();
	} else if ((strncmp(command, "ena ", strlen("ena ")) == 0) ||
			   (strncmp(command, "dis ", strlen("dis ")) == 0)) {
		control_traffic_conf_modify(command);
	} else if (strncmp(command, "stats", strlen("stats")) == 0) {
		if (strstr(command, "clear"))
			received_control_frames = 0;
		else
			printf("Control frames received by CPU: %6d\n",
					received_control_frames);
	} else if (strncmp(command, "verbose", strlen("verbose")) == 0) {
		if (strstr(command, "off"))
			dump_control_frames = 0;
		else
			dump_control_frames = 1;
	} else if (strncmp(command, "receive", strlen("receive")) == 0) {
			command += strlen("receive");
			if (command[0] == '\0')
				receive_control_frames = 1;
			else if (strcmp(command, " off") == 0)
				receive_control_frames = 0;
			else {
				printf("Error: Incorrect parameter\n");
				return -1;
			}
	} else if (strncmp(command, "reflect ", strlen("reflect ")) == 0) {
		command += strlen("reflect ");
		if (command)
			reflect_control_frames_port = atoi(command);
		else {
			printf("Error: Incorrect port number: %s\n", command);
			return -1;
		}
	} else {
		printf("Error: Incorrect command\n");
		printf("Options:\n");
		printf("\tconf\n");
		printf("\tstats [clear]\n");
		printf("\tverbose [off]\n");
		printf("\treceive [off]\n");
		printf("\treflect <port_no>\n");
		printf("\tena|dis <proto>\n\n");
		return -1;
	}
	return 0;
}
