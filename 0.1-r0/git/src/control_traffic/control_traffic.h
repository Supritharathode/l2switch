/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#ifndef _CONTROL_TRAFFIC_H_
#define _CONTROL_TRAFFIC_H_
/* port counters header */

#include <fsl_utils/fsl_utils.h>

int port_npi_command(const char *command);
int control_traffic_command(const char *command);
void control_traffic_recv(int fd_npi, u32 count);

#endif /* _CONTROL_TRAFFIC_H_ */
