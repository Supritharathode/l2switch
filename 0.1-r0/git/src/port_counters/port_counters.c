/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#include "port_counters.h"
#include <inttypes.h>

#define PORT_CNTRS_PRINT_ALL		1
#define PORT_CNTRS_PRINT_PACKETS	2
#define PORT_CNTRS_PRINT_BYTES		3
#define PORT_CNTRS_PRINT_ERRORS		4
#define PORT_CNTRS_PRINT_DISCARDS	5
#define PORT_CNTRS_PRINT_FILTERED	6

#define RMON_FRAME_MAX 			1518

/* Clear port statistics */
static int port_counters_clear(int port_no)
{
	return (int) vtss_port_counters_clear(NULL, port_no);
}

/* Print port statistics for the specified port */
/* Command options: packets|bytes|errors|discards|filtered */
static int port_counters_print(int port_no, short cmd)
{
	vtss_rc rc;
	vtss_port_counters_t counters;

	int prio;

	printf("Port %d statistics:\n", port_no);
	printf("--------------------------------------------------------\n");

	rc = vtss_port_counters_get(NULL, port_no, &counters);
	if (rc != VTSS_RC_OK)
		return (int)rc;

	switch (cmd) {
	case PORT_CNTRS_PRINT_ALL:
		printf("Rx Packets:     %10"PRIu64"   Tx Packets:     %10"PRIu64"\n",
				counters.rmon.rx_etherStatsPkts,
				counters.rmon.tx_etherStatsPkts);
		printf("Rx Octets:      %10"PRIu64"   Tx Octets:      %10"PRIu64"\n",
				counters.rmon.rx_etherStatsOctets,
				counters.rmon.tx_etherStatsOctets);
		printf("Rx Unicast:     %10"PRIu64"   Tx Unicast:     %10"PRIu64"\n",
				counters.if_group.ifInUcastPkts,
				counters.if_group.ifOutUcastPkts);
		printf("Rx Multicast:   %10"PRIu64"   Tx Multicast:   %10"PRIu64"\n",
				counters.rmon.rx_etherStatsMulticastPkts,
				counters.rmon.tx_etherStatsMulticastPkts);
		printf("Rx Broadcast:   %10"PRIu64"   Tx Broadcast:   %10"PRIu64"\n",
				counters.rmon.rx_etherStatsBroadcastPkts,
				counters.rmon.tx_etherStatsBroadcastPkts);
		printf("RX Pause:       %10"PRIu64"   Tx Pause:       %10"PRIu64"\n",
				counters.ethernet_like.dot3InPauseFrames,
				counters.ethernet_like.dot3OutPauseFrames);
		printf("\n");

		printf("Rx 64:          %10"PRIu64"   Tx 64:          %10"PRIu64"\n",
				counters.rmon.rx_etherStatsPkts64Octets,
				counters.rmon.tx_etherStatsPkts64Octets);
		printf("Rx 65-127       %10"PRIu64"   Tx 65-127:      %10"PRIu64"\n",
				counters.rmon.rx_etherStatsPkts65to127Octets,
				counters.rmon.tx_etherStatsPkts65to127Octets);
		printf("Rx 128-255      %10"PRIu64"   Tx 128-255:     %10"PRIu64"\n",
				counters.rmon.rx_etherStatsPkts128to255Octets,
				counters.rmon.tx_etherStatsPkts128to255Octets);
		printf("Rx 256-511      %10"PRIu64"   Tx 256-511:     %10"PRIu64"\n",
				counters.rmon.rx_etherStatsPkts256to511Octets,
				counters.rmon.tx_etherStatsPkts256to511Octets);
		printf("Rx 512-1023     %10"PRIu64"   Tx 512-1023:    %10"PRIu64"\n",
				counters.rmon.rx_etherStatsPkts512to1023Octets,
				counters.rmon.tx_etherStatsPkts512to1023Octets);
		printf("Rx 1024-%u    %10"PRIu64"   Tx 1024-%u:   %10"PRIu64"\n",
				RMON_FRAME_MAX,
				counters.rmon.
					rx_etherStatsPkts1024to1518Octets,
				RMON_FRAME_MAX,
				counters.rmon.
					tx_etherStatsPkts1024to1518Octets);
		printf("Rx %u-        %10"PRIu64"   Tx %u-    :   %10"PRIu64"\n",
				RMON_FRAME_MAX + 1,
				counters.rmon.rx_etherStatsPkts1519toMaxOctets,
				RMON_FRAME_MAX + 1,
				counters.rmon.tx_etherStatsPkts1519toMaxOctets);
		printf("\n");

		for (prio = VTSS_PRIO_START; prio < VTSS_PRIO_END; prio++) {
			printf("Rx Packets_%d:   %10"PRIu64"   Tx Packets_%d:   %10"PRIu64"\n",
					prio,
					counters.prop.rx_prio[prio],
					prio,
					counters.prop.tx_prio[prio]);
		}
		printf("\n");

		printf("Rx Drops:       %10"PRIu64"   Tx Drops:       %10"PRIu64"\n",
				counters.rmon.rx_etherStatsDropEvents,
				counters.rmon.tx_etherStatsDropEvents);
		printf("Rx CRC/Align:   %10"PRIu64"   Tx Late/ExcColl:%10"PRIu64"\n",
				counters.rmon.rx_etherStatsCRCAlignErrors,
				counters.if_group.ifOutErrors);
		printf("Rx Undersize:   %10"PRIu64"\n",
				counters.rmon.rx_etherStatsUndersizePkts);
		printf("Rx Oversize:    %10"PRIu64"\n",
				counters.rmon.rx_etherStatsOversizePkts);
		printf("Rx Fragments:   %10"PRIu64"\n",
				counters.rmon.rx_etherStatsFragments);
		printf("Rx Jabbers:     %10"PRIu64"\n",
				counters.rmon.rx_etherStatsJabbers);
		printf("Rx Filtered:    %10"PRIu64"\n",
				counters.bridge.dot1dTpPortInDiscards);
		break;
	case PORT_CNTRS_PRINT_PACKETS:
		printf("Rx Packets: %10"PRIu64"   Tx Packets: %10"PRIu64"\n",
				counters.rmon.rx_etherStatsPkts,
				counters.rmon.tx_etherStatsPkts);
		break;
	case PORT_CNTRS_PRINT_BYTES:
		printf("Rx Octets: %10"PRIu64"   Tx Octets: %10"PRIu64"\n",
				counters.rmon.rx_etherStatsOctets,
				counters.rmon.tx_etherStatsOctets);
		break;
	case PORT_CNTRS_PRINT_ERRORS:
		printf("Rx Errors: %10"PRIu64"   Tx Errors: %10"PRIu64"\n",
				counters.if_group.ifInErrors,
				counters.if_group.ifOutErrors);
		break;
	case PORT_CNTRS_PRINT_DISCARDS:
		printf("Rx Discards: %10"PRIu64"   Tx Discards: %10"PRIu64"\n",
				counters.if_group.ifInDiscards,
				counters.if_group.ifOutDiscards);
		break;
	case PORT_CNTRS_PRINT_FILTERED:
		printf("Filtered: %10"PRIu64"\n",
				counters.bridge.dot1dTpPortInDiscards);
		break;
	default:
		printf("Error fetching port %d counters - error code %d\n",
			port_no, rc);
		return (int)VTSS_RC_ERROR;
	}
	printf("--------------------------------------------------------\n");
	return (int)VTSS_RC_OK;
}

/* Command options: clear|packets|bytes|errors|discards */
int port_counters(int port_no, const char *command)
{

#if DEBUG
	printf("DEBUG: %s(%d, %s)\n", __func__, port_no, command);
#endif

	if (port_no < VTSS_PORT_NO_START || port_no > VTSS_PORT_NO_END) {
		printf("ERROR: Incorrect port number %d\n", port_no);
		return -1;
	}

	if (strstr(command, "clear") != NULL)
		return port_counters_clear(port_no);
	else if (strstr(command, "packets") != NULL)
		return port_counters_print(port_no, PORT_CNTRS_PRINT_PACKETS);
	else if (strstr(command, "bytes") != NULL)
		return port_counters_print(port_no, PORT_CNTRS_PRINT_BYTES);
	else if (strstr(command, "errors") != NULL)
		return port_counters_print(port_no, PORT_CNTRS_PRINT_ERRORS);
	else if (strstr(command, "discards") != NULL)
		return port_counters_print(port_no, PORT_CNTRS_PRINT_DISCARDS);
	else if (strstr(command, "filtered") != NULL)
		return port_counters_print(port_no, PORT_CNTRS_PRINT_FILTERED);
	/*
	else
		printf("WARNING: Unknown command: <%s>", command);
	*/

	/* Default */
	return port_counters_print(port_no, PORT_CNTRS_PRINT_ALL);
}
