/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#ifndef _MULTICAST_H_
#define _MULTICAST_H_

#include <fsl_utils/fsl_utils.h>

int multicast_command(const char *command);

void dump_mac(u8 addr[6]);

#endif /* _MUTLICAST_H_ */
