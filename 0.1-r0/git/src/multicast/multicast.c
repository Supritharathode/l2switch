/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#include "multicast.h"

/* Process 'set multicast' command parameters
 * Command options: <mac_address> ports [<port list>]
 *
 * Note: if  no port list is supplied, destination
 * mask will be cleared.
 */
int multicast_command(const char *command)
{
	vtss_mac_table_entry_t	mte;
	char			*copy, *tok;
	int			i, port_no;

	/* Parse MAC address */
	if (parse_mac(command, mte.vid_mac.mac.addr) != MAC_ADDR_COUNT) {
		printf("ERROR:%s:%d:Invalid MAC address!\n",
				__FILE__, __LINE__);
		return -1;
	}
#ifdef MULTICAST_DEBUG
	dump_mac(mte.vid_mac.mac.addr);
#endif

	/* Initialize destination ports array */
	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		if (is_valid_port(i))
			mte.destination[i] = DISABLED;

	/* Step over MAC address string */
	command += MAC_ADDR_STR_LENGTH;

	/* Parse destination ports */
	command += strlen("ports ");
	copy = strdup(command);
	for (tok = strtok(copy, " ");
			tok != NULL;
			tok = strtok(NULL, " ")) {
		port_no = atoi(tok);
#ifdef MULTICAST_DEBUG
		printf("Enabling destination port %d\n", port_no);
#endif
		if (is_valid_port(port_no))
			mte.destination[port_no] = ENABLED;
		else
			printf("WARNING: Skipping over invalid port %d!\n",
					port_no);
	}
	free(copy);

	mte.vid_mac.vid = 1; /* Use native VLAN for now */
	mte.copy_to_cpu = DISABLED; /* No need to copy frames to CPU */
	mte.locked = ENABLED; /* This is a static entry not subject to aging */
	mte.aged = DISABLED; /* Don't care, disabled here for clarity */

	/* Add this MAC entry to FDB */
	if (vtss_mac_table_add(NULL, &mte) != VTSS_RC_OK) {
		printf("ERROR:%s:%d: Unable to add static multicast entry!",
				__FILE__, __LINE__);
		return -1;
	}
	return 0;
}

/* Prints a MAC address, using portable
 * formatting macros
 */
void dump_mac(u8 addr[6])
{
	printf("The MAC address is "MAC_ADDR_PRINT_FORMAT"\n",
			MAC_ADDR_PRINT_ARGS(addr));
}
