/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

/*
 * flow_control.c
 */
#include "flow_control.h"

extern BOOL phy_present[];

static BOOL fc_autoneg[VTSS_PORT_NO_END];

/* get flow control configuration for a port */
int port_flow_control_get(int port_no, vtss_port_flow_control_conf_t *fc_conf)
{
	vtss_port_conf_t conf_port;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_port_conf_get(NULL, port_no, &conf_port) != VTSS_RC_OK) {
		printf("Cannot get configuration for port %d\n", port_no);
		return 1;
	}

	memcpy(fc_conf, &(conf_port.flow_control), sizeof(*fc_conf));

	return 0;
}

/* set flow control configuration for a port */
int port_flow_control_set(int port_no, vtss_port_flow_control_conf_t *fc_conf)
{
	vtss_port_conf_t	conf_port;
	vtss_phy_conf_t		conf_phy;
	BOOL			aneg;

	if (!is_valid_port(port_no))
		return 1;

	if (port_flow_control_autoneg_get(port_no, &aneg) != 0) {
		printf("Cannot get flow control mode for port %d\n", port_no);
		return 1;
	}

	if (vtss_port_conf_get(NULL, port_no, &conf_port) != VTSS_RC_OK) {
		printf("Cannot get configuration for port %d\n", port_no);
		return 1;
	}

	/* flow control can not be configured when in autonegotiation mode */
	if (aneg != TRUE)
		memcpy(&(conf_port.flow_control), fc_conf, sizeof(*fc_conf));
	else
		memcpy(&(conf_port.flow_control.smac),
				&(fc_conf->smac),
				sizeof(vtss_mac_t));

	if (vtss_port_conf_set(NULL, port_no, &conf_port) != VTSS_RC_OK) {
		printf("Cannot set configuration for port %d\n", port_no);
		return 1;
	}

	if (aneg == TRUE)
		return 0;

	if (phy_present[port_no] == FALSE)
		return 0;

	/* we have to configure PHY to advertise
	 * our flow control configuration */
	if (vtss_phy_conf_get(NULL, port_no, &conf_phy) != VTSS_RC_OK) {
		printf("Cannot get status for port %d\n", port_no);
		return 1;
	}

	conf_phy.aneg.symmetric_pause = conf_port.flow_control.obey;
	conf_phy.aneg.asymmetric_pause = conf_port.flow_control.obey ^
			conf_port.flow_control.generate;

	if (vtss_phy_conf_set(NULL, port_no, &conf_phy) != VTSS_RC_OK)
		printf("Cannot set configuration for port %d\n", port_no);

	return 0;
}

/* set flow control configuration based(or not) on autonegotiation results */
int port_flow_control_autoneg_set(int port_no, BOOL aneg)
{
	vtss_phy_conf_t         phy;

	if (!is_valid_port(port_no))
		return 1;

	fc_autoneg[port_no] = aneg;

	/* if TRUE, fc configuration should
	 * change after port polling */
	if (aneg == FALSE)
		return 0;

	if (phy_present[port_no] == FALSE)
		return 0;

	if (vtss_phy_conf_get(NULL, port_no, &phy) != VTSS_RC_OK) {
		printf("Can't get phy configuration for port %d\n", port_no);
		return 1;
	}

	/* we support both obeying and receiving PAUSE frames,
	 * so we advertise both capabilities */
	phy.aneg.symmetric_pause = TRUE;
	phy.aneg.asymmetric_pause = FALSE;

	if (vtss_phy_conf_set(NULL, port_no, &phy) != VTSS_RC_OK) {
		printf("Can't set phy configuration for port %d\n", port_no);
		return 1;
	}

	return 0;
}

/* check if flow control is based on autonegotiation results */
int port_flow_control_autoneg_get(int port_no, BOOL *aneg)
{
	if (!is_valid_port(port_no))
		return 1;

	*aneg = fc_autoneg[port_no];

	return 0;
}

/* print flow control configuration for a port */
void port_flow_control_show(int port_no)
{
	vtss_port_flow_control_conf_t	fc_conf;
	BOOL				aneg;

	if (!is_valid_port(port_no))
		return;

	if (port_flow_control_get(port_no, &fc_conf) != 0)
		return;

	printf("Port: %d\t", port_no);
	if (port_flow_control_autoneg_get(port_no, &aneg) != 0)
		return;
	aneg == TRUE ?
			printf("Autonegotiation mode: enable\t") :
			printf("Autonegotiation mode: disable\t");

	fc_conf.obey == TRUE ?
			printf("obey PAUSE: enable\t") :
			printf("obey PAUSE: disable\t");

	fc_conf.generate == TRUE ?
			printf("generate PAUSE: enable\t\t") :
			printf("generate PAUSE: disable\t\t");

	printf("SMAC: "MAC_ADDR_PRINT_FORMAT"\n",
			MAC_ADDR_PRINT_ARGS(fc_conf.smac.addr));
}

/* print flow control configuration for all ports */
void port_flow_control_show_all(void)
{
	int i;

	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		if (is_valid_port(i))
			port_flow_control_show(i);

}
