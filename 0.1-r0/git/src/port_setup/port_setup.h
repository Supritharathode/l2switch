/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#ifndef _PORT_SETUP_H_
#define _PORT_SETUP_H_

#include <fsl_utils/fsl_utils.h>
#include <flow_control/flow_control.h>

/* port configuration initialisation */
void port_setup_init(int port_no, BOOL aneg);

/* check the PHY's for configuration changes */
void port_polling(void);

/* set/get/show the link configuration for a port */
int port_config_set(int port_no, int speed, int duplexity);
void port_config_show(int port_no);
void port_config_show_all(void);

/* enable/disable autonegotiation on a port
 * autonegotiation is also disabled when a
 * certain link speed or duplexity is wanted */
int port_autoneg_set(int port_no, BOOL aneg);

#endif /* _PORT_SETUP_H_ */
