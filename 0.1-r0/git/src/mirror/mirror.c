/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#include "mirror.h"
#include <fsl_utils/fsl_utils.h>

static int set_monitor_port(int monitor_port);
static int set_forwarding(BOOL type);
static int set_mirroring_off();
static int set_mirrored_port(int mirrored_port, int type);
static int clear_probed_ports();
static void print_mirror_usage();

int mirror_command(const char *command)
{
	unsigned int port_no;

	if (strstr(command, "monitor ") != NULL) {
		command += strlen("monitor ");
		if (get_next_value(&command, &port_no))
			return -1;
		return set_monitor_port(port_no);
	} else if (strstr(command, "forwarding on") != NULL) {
		return set_forwarding(TRUE);
	} else if (strstr(command, "forwarding off") != NULL) {
		return set_forwarding(FALSE);
	} else if (strstr(command, "off") != NULL) {
		return set_mirroring_off();
	} else if (strstr(command, "ingress cpu") != NULL) {
		return set_mirrored_port(VTSS_PORT_NO_NONE, INGRESS_CPU);
	} else if (strstr(command, "egress cpu") != NULL) {
		return set_mirrored_port(VTSS_PORT_NO_NONE, EGRESS_CPU);
	} else if (strstr(command, "ingress port ") != NULL) {
		command += strlen("ingress port ");
		if (get_next_value(&command, &port_no))
			return -1;
		return set_mirrored_port(port_no, INGRESS_PORT);
	} else if (strstr(command, "egress port ") != NULL) {
		command += strlen("egress port ");
		if (get_next_value(&command, &port_no))
			return -1;
		return set_mirrored_port(port_no, EGRESS_PORT);
	}

	print_mirror_usage();
	return -1;
}

/* Set the corresponding monitoring port
 */
static int set_monitor_port(int monitor_port)
{
	if (!is_valid_port(monitor_port))
		return -1;

	if (vtss_mirror_monitor_port_set(NULL, monitor_port) != VTSS_RC_OK) {
		printf("ERROR:%d:Unable to set monitor port!\n", __LINE__);
		return -1;
	}
	return 0;
}

/* Normal traffic forwarding can be enabled/disabled
 * using this method.
 */
static int set_forwarding(BOOL type)
{
	vtss_mirror_conf_t mconf;

	if (vtss_mirror_conf_get(NULL, &mconf) != VTSS_RC_OK) {
		printf("ERROR:%d:Unable to get mirror conf!\n", __LINE__);
		return -1;
	}

	mconf.fwd_enable = type;

	if (vtss_mirror_conf_set(NULL, &mconf) != VTSS_RC_OK) {
		printf("ERROR:%d:Unable to set mirror conf!\n", __LINE__);
		return -1;
	}

	return 0;
}

/* Disables mirroring; if you want to enable mirroring again,
 * use set_monitor_port() command with the desired port.
 */
static int set_mirroring_off()
{
	if (vtss_mirror_monitor_port_set(NULL, VTSS_PORT_NO_NONE) !=
			VTSS_RC_OK) {
		printf("ERROR:%d:Unable to set monitor port!\n", __LINE__);
		return -1;
	}
	return 0;
}

/* Set probing port; only one port is available for probing;
 * 'type' parameter should be one of the four values defined.
 */
static int set_mirrored_port(int mirrored_port, int type)
{
	BOOL	member[VTSS_PORT_ARRAY_SIZE];
	int	i;

	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		member[i] = FALSE;

	if (clear_probed_ports())
		return -1;

	if (!is_valid_port(mirrored_port) &&
			mirrored_port != VTSS_PORT_NO_NONE)
		return -1;

	switch (type) {
	case INGRESS_PORT:
		member[mirrored_port] = TRUE;
		if (vtss_mirror_ingress_ports_set(NULL, member) !=
				VTSS_RC_OK) {
			printf("ERROR:%d:Unable to set ingress port!\n",
					__LINE__);
			return -1;
		}
		break;
	case EGRESS_PORT:
		member[mirrored_port] = TRUE;
		if (vtss_mirror_egress_ports_set(NULL, member) !=
				VTSS_RC_OK) {
			printf("ERROR:%d:Unable to set egress port!\n",
					__LINE__);
			return -1;
		}
		break;
	case INGRESS_CPU:
		if (vtss_mirror_cpu_ingress_set(NULL, TRUE) !=
				VTSS_RC_OK) {
			printf("ERROR:%d:Cannot set CPU ingress mirroring!\n",
					__LINE__);
			return -1;
		}
		break;
	case EGRESS_CPU:
		if (vtss_mirror_cpu_egress_set(NULL, TRUE) !=
				VTSS_RC_OK) {
			printf("ERROR:%d:Cannot set CPU egress mirroring\n",
					__LINE__);
			return -1;
		}
		break;
	default:
		return -1;
	}
	return 0;
}

/* Clears all the masks regarding probed ports */
static int clear_probed_ports()
{
	int i;
	BOOL member[VTSS_PORT_ARRAY_SIZE];

	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		member[i] = FALSE;

	if (vtss_mirror_ingress_ports_set(NULL, member) != VTSS_RC_OK) {
		printf("ERROR:%d:Cannot set ingress ports!\n", __LINE__);
		return -1;
	}
	if (vtss_mirror_egress_ports_set(NULL, member) != VTSS_RC_OK) {
		printf("ERROR:%d:Cannot set egress ports!\n", __LINE__);
		return -1;
	}
	if (vtss_mirror_cpu_ingress_set(NULL, FALSE) != VTSS_RC_OK) {
		printf("ERROR:%d:Cannot set CPU ingress mirroring!\n",
				__LINE__);
		return -1;
	}
	if (vtss_mirror_cpu_egress_set(NULL, FALSE) != VTSS_RC_OK) {
		printf("ERROR:%d:Cannot set CPU egress mirroring!\n",
				__LINE__);
		return -1;
	}
	return 0;
}

/* Prints the correct usage for "set mirror" command */
static void print_mirror_usage()
{
	printf("\nUsage:\n");
	printf("\tset mirror ingress|egress port <port_no>\n");
	printf("\tset mirror ingress|egress cpu\n");
	printf("\tset mirror monitor <port_no>\n");
	printf("\tset mirror forwarding on|off\n");
	printf("\tset mirror off\n\n");
}
