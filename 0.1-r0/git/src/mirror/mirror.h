/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#ifndef _MIRROR_H_
#define _MIRROR_H_

/* Types of mirroring */
#define INGRESS_PORT	0
#define EGRESS_PORT	1
#define INGRESS_CPU	2
#define EGRESS_CPU	3

/* 'set mirror' command parser;
 * available commands are:
 *	set mirror ingress|egress port <port_no>
 *	set mirror ingress|egress cpu
 *	set mirror monitor <port_no>
 *	set mirror forwarding on|off
 *	set mirror off
 */
int mirror_command(const char *command);

#endif /* _MIRROR_H_ */
