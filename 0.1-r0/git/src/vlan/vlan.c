/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

/* used to configure VLANs */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "vlan.h"

/* set VLAN type for a port */
int arr[4096];
int port_vlan_type_set(int port_no, vtss_vlan_port_type_t type)
{
	vtss_vlan_port_conf_t conf;
	vtss_port_conf_t port_conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_vlan_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Couldn't get VLAN conf for port %d\n", port_no);
		return 1;
	}

	conf.port_type = type;

	if (vtss_vlan_port_conf_set(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Couldn't set VLAN conf for port %d\n", port_no);
		return 1;
	}

	if (vtss_port_conf_get(NULL, port_no, &port_conf)) {
		printf("ERROR: Failed to get configuration for port %d\n",
				port_no);
		return 1;
	}

	/* Adjust the maximum VLAN tags allowed based on VLAN type */
	switch (type) {
	case VTSS_VLAN_PORT_TYPE_UNAWARE:
	case VTSS_VLAN_PORT_TYPE_C:
		port_conf.max_tags = VTSS_PORT_MAX_TAGS_ONE;
		break;
	case VTSS_VLAN_PORT_TYPE_S:
	case VTSS_VLAN_PORT_TYPE_S_CUSTOM:
		port_conf.max_tags = VTSS_PORT_MAX_TAGS_TWO;
		break;
	default:
		printf("ERROR: Unknown VLAN type\n");
	}

	if (vtss_port_conf_set(NULL, port_no, &port_conf)) {
		printf("ERROR: Failed to set configuration for port %d\n",
				port_no);
		return 1;
	}

	return 0;
}

/* get VLAN type for a port */
int port_vlan_type_get(int port_no, vtss_vlan_port_type_t *type)
{
	vtss_vlan_port_conf_t conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_vlan_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Couldn't get VLAN conf for port %d\n", port_no);
		return 1;
	}

	*type = conf.port_type;

	return 0;
}

/* show VLAN type of a port */
void port_vlan_type_show(int port_no)
{
	vtss_vlan_port_type_t type = -1;

	if (!is_valid_port(port_no))
		return;

	if (port_vlan_type_get(port_no, &type) != 0)
		printf("Couldn't get VLAN type for port %d\n", port_no);

	switch (type) {
	case VTSS_VLAN_PORT_TYPE_UNAWARE:
		printf("Port: %d\tType: VLAN unaware\n", port_no);
		break;
	case VTSS_VLAN_PORT_TYPE_C:
		printf("Port: %d\tType: C (TPID 0x8100)\n", port_no);
		break;
	case VTSS_VLAN_PORT_TYPE_S:
		printf("Port: %d\tType: S (TPID 0x88a8)\n", port_no);
		break;
	case VTSS_VLAN_PORT_TYPE_S_CUSTOM:
		printf("Port: %d\tType: Custom S\n", port_no);
		break;
	default:
		printf("Port: %d\tType: Unknown\n", port_no);
	}
}

/* show VLAN type for all ports */
void port_vlan_type_show_all(void)
{
	int i;

	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		if (is_valid_port(i))
			port_vlan_type_show(i);
}

/* set untagged VLAN for a port */
int port_vlan_native_set(int port_no, int vid)
{
	vtss_vlan_port_conf_t conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vid < VTSS_VID_NULL || vid > VTSS_VID_ALL)
		return 1;

	if (vtss_vlan_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Couldn't get VLAN conf for port %d\n", port_no);
		return 1;
	}

	conf.untagged_vid = vid;

	if (vtss_vlan_port_conf_set(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Couldn't set VLAN conf for port %d\n", port_no);
		return 1;
	}

	return 0;
}

/* get untagged VLAN for a port */
int port_vlan_native_get(int port_no, int *vid)
{
	vtss_vlan_port_conf_t conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_vlan_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Couldn't get VLAN conf for port %d\n", port_no);
		return 1;
	}

	*vid = conf.untagged_vid;

	return 0;
}

/* show untagged VLAN for a port */
void port_vlan_native_show(int port_no)
{
	int vid;

	if (!is_valid_port(port_no))
		return;

	if (port_vlan_native_get(port_no, &vid) != 0) {
		printf("Couldn't get VLAN conf for port %d\n", port_no);
		return;
	}

	printf("Port: %d\tNative VLAN: %d\n", port_no, vid);
}

/* show untagged VLAN for all ports */
void port_vlan_native_show_all(void)
{
	int i;

	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		if (is_valid_port(i))
			port_vlan_native_show(i);
}

/* make all the ports VLAN aware, and classify them
 * based on the C tag */
static void vlan_aware_switch(void)
{
	int i;

	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		if (is_valid_port(i) && (port_vlan_type_set(i,
						VTSS_VLAN_PORT_TYPE_C) != 0))
			printf("Couldn't make the port %d VLAN aware\n", i);
}

/* make VLAN 1 the default untagged vlan */
static void vlan_native_default(void)
{
	int i;

	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		if (is_valid_port(i) && (port_vlan_native_set(i,
						VTSS_VID_DEFAULT) != 0))
			printf("Couldn't set default native VLAN for port %d\n",
					i);
}

/* make VLAN 1 the default pvid */
static void vlan_pvid_default(void)
{
	int i;

	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		if (is_valid_port(i) && (vlan_pvid_set(i,
						VTSS_VID_DEFAULT) != 0))
			printf("Couldn't set default PVID for port %d\n", i);
}

void vlan_init(void)
{
	vlan_aware_switch();
	vlan_pvid_default();
	vlan_native_default();
}

/* show members for a VLAN */
void vlan_members_show(int vid)
{
	int i;
	BOOL member[VTSS_PORT_ARRAY_SIZE], clear;

	if (!is_valid_vid(vid))
		return;

	if (vtss_vlan_port_members_get(NULL, vid, member) != VTSS_RC_OK)
		printf("Could not get members for VLAN %d\n", vid);

	clear = TRUE;
	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		if (is_valid_port(i) &&  member[i] == TRUE) {
			if (clear == TRUE) {
				clear = FALSE;
				printf("VLAN %d: ", vid);
			}
			printf("%d, ", i);
		}

	if (clear == FALSE)
		printf("\n");
}

/* show all members for a VLAN */
void vlan_members_show_all(void)
{
	int i;

	printf("Printing all VLANs...\n\n");
	for (i = VTSS_VID_DEFAULT; i < VTSS_VIDS; i++)
		vlan_members_show(i);
}
/*----Mycode------READ COMMAND*/
int vlan_table_read(int vid){
	u32 value,acc_val,read_val,final_val,temp,test;
	printf("%d:",vid);
		u32 offset = (0xa8b4c4 - 0x800000) >> 2;
		u32 acc_offset = (0xa8b4c0 - 0x800000) >> 2;
		l2sw_reg_read(0,offset,&value);
		
		
		value = value >> 12;
		value = value << 12;
		value = value | vid;
		l2sw_reg_write(0, offset, value);
		l2sw_reg_write(0,acc_offset,arr[vid]);
		l2sw_reg_read(0,acc_offset,&acc_val);
		printf("%x\n",acc_val);
		
		/*temp = acc_val;
		temp = temp << 30;
		temp = temp >> 30;
		if(temp==0){
		acc_val = acc_val >> 2;
		acc_val = acc_val << 2;
		acc_val = acc_val | 1;//read
		printf("%d\n",acc_val);
		}
		l2sw_reg_write(0, acc_offset, acc_val);//writing it to register vlan access
		l2sw_reg_read(0,acc_offset,&test);
		printf("%d\n",test);
		l2sw_reg_write(0, offset, value);
		l2sw_reg_read(0,acc_offset,&read_val);
		//read_val = acc_val;
		read_val = read_val << 30;
		read_val = read_val >> 30;
		while(read_val !=0){
			l2sw_reg_read(0,acc_offset,&read_val);
			read_val = read_val << 30;
			read_val = read_val >> 30;
			//printf("while");
		//value = value | vid;

		}
		l2sw_reg_read(0,acc_offset,&final_val);
		printf("%d\n",final_val);
		acc_val = acc_val << 2;
		acc_val = acc_val >> 2;
		acc_val = acc_val >> 19;
		acc_val = acc_val << 19;*/
	//l2sw_reg_read(0,acc_offset,&acc_val);*/
	
	return 0;

}
void vlan_range_add(void){
	char *line=NULL;
	ssize_t read;
	size_t len =0;
	//printf("range\n");
	 FILE *file = fopen ( "filenew.txt", "r" );
	 printf("Reading 4096 vlan id's...\n");
	int i,temp=0;
	//arr[0] =0;
	for(i=0;i<4096;i++){
	 arr[i]=0;
	}
	while ( (read = getline(&line,&len,file )) != -1) 
	{
		//printf("first while\n");
		i = 0;
		temp = 0;
		while(line[i] != ':'){
			    	temp=temp*10 + (line[i++]-'0') ;
			    	//printf("second while\n");
			}
		int init_no = temp;
		//printf("init:%d\n",init_no);
		while(line[i]!='.')
		{
		    temp=0;
		    i++;
		    //printf("i:%d\n",i);
		    while(line[i]!=' ' && line[i]!='.'){
		    	
			temp=temp*10 + (line[i++]-'0') ;
		
		    }
		    int pow_val = 1,m;
		    for(m=0;m<2+temp;m++){
		    	pow_val = pow_val*2;
		    }
		    arr[init_no]=arr[init_no]+pow_val;
		}
	}
//return 0;
}

/* add a member for a VLAN */
int vlan_member_add(int port_no, int vid)
{

		u32 value;
		//printf("first\n");	
	BOOL member[VTSS_PORT_ARRAY_SIZE];
	if (!is_valid_port(port_no) || !is_valid_vid(vid))
		return 1;
		if(port_no == 0)
		{
			u32 offset = (0xa80000 - 0x800000) >> 2;
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
			value = value >> 12;
			value = value << 12;
			value = value | vid;
			l2sw_reg_write(0, offset, value);
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
		}
		else if(port_no == 1)
		{
			u32 offset = (0xa80100 - 0x800000) >> 2;
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
			value = value >> 12;
			value = value << 12;
			value = value | vid;
			l2sw_reg_write(0, offset, value);
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
		}
		else if(port_no == 2)
		{
			u32 offset = (0xa80200 - 0x800000) >> 2;
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
			value = value >> 12;
			value = value << 12;
			value = value | vid;
			l2sw_reg_write(0, offset, value);
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
		}
		else if(port_no == 3)
		{
			u32 offset = (0xa80300 - 0x800000) >> 2;
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
			value = value >> 12;
			value = value << 12;
			value = value | vid;
			l2sw_reg_write(0, offset, value);
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
		}
		else if(port_no == 4)
		{
			u32 offset = (0xa80400 - 0x800000) >> 2;
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
			value = value >> 12;
			value = value << 12;
			value = value | vid;
			l2sw_reg_write(0, offset, value);
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
		}
		else if(port_no == 5)
		{
			u32 offset = (0xa80500 - 0x800000) >> 2;
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
			value = value >> 12;
			value = value << 12;
			value = value | vid;
			l2sw_reg_write(0, offset, value);
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
		}
		else if(port_no == 6)
		{
			u32 offset = (0xa80600 - 0x800000) >> 2;
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
			value = value >> 12;
			value = value << 12;
			value = value | vid;
			l2sw_reg_write(0, offset, value);
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
		}
		else if(port_no == 7)
		{
			u32 offset = (0xa80700 - 0x800000) >> 2;
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
			value = value >> 12;
			value = value << 12;
			value = value | vid;
			l2sw_reg_write(0, offset, value);
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
		}
		else if(port_no == 8)
		{
			u32 offset = (0xa80800 - 0x800000) >> 2;
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
			value = value >> 12;
			value = value << 12;
			value = value | vid;
			l2sw_reg_write(0, offset, value);
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
		}
		else
		{
			u32 offset = (0xa80900 - 0x800000) >> 2;
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
			value = value >> 12;
			value = value << 12;
			value = value | vid;
			l2sw_reg_write(0, offset, value);
			l2sw_reg_read(0, offset, &value);
			printf("%x\n", value);
		}
		
		
		printf("Adding port %d as member for VLAN %d\n", port_no, vid);
	/* add port in VLAN's members */
	if (vtss_vlan_port_members_get(NULL, vid, member) != VTSS_RC_OK) {
		printf("Could not get members for VLAN %d\n", vid);
		return 1;
	}
	member[port_no] = TRUE;	
	if (vtss_vlan_port_members_set(NULL, vid, member) != VTSS_RC_OK) {
		printf("Could not set members for VLAN %d\n", vid);
		return 1;
	}
	vlan_members_show(vid);
	return 0;
}

/* remove a member from a VLAN */
int vlan_member_remove(int port_no, int vid)
{
	BOOL member[VTSS_PORT_ARRAY_SIZE];

	if (!is_valid_port(port_no) || !is_valid_vid(vid))
		return 1;

	printf("Removing port %d from VLAN %d members\n", port_no, vid);

	/* add port in VLAN's members */
	if (vtss_vlan_port_members_get(NULL, vid, member) != VTSS_RC_OK) {
		printf("Could not get members for VLAN %d\n", vid);
		return 1;
	}

	member[port_no] = FALSE;
	if (vtss_vlan_port_members_set(NULL, vid, member) != VTSS_RC_OK) {
		printf("Could not set members for VLAN %d\n", vid);
		return 1;
	}

	/* show members for the current VLAN */
	vlan_members_show(vid);
	return 0;
}

/* get VID for the requested port */
int vlan_pvid_get(int port_no, int *vid)
{
	vtss_vlan_port_conf_t conf;

	if (!is_valid_port(port_no))
		return 1;

	if (vtss_vlan_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Couldn't get VLAN conf for port %d\n", port_no);
		return 1;
	}

	*vid = conf.pvid;

	return 0;
}

/* print the PVID of a port */
void vlan_pvid_show(int port_no)
{
	int vid;

	if (!is_valid_port(port_no))
		return;

	if (vlan_pvid_get(port_no, &vid) != 0)
		printf("Couldn't get PVID for port %d\n", port_no);
	else
		printf("%d - VID %u\n", port_no, vid);
}

/* show the PVID for each port */
void vlan_pvid_show_all(void)
{
	int i;

	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		if (is_valid_port(i))
			vlan_pvid_show(i);
}

/* set VID for the requested port */
int vlan_pvid_set(int port_no, int vid)
{
	vtss_vlan_port_conf_t conf;

	if (!is_valid_port(port_no) || !is_valid_vid(vid))
		return 1;

	if (vtss_vlan_port_conf_get(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Couldn't get VLAN conf for port %d\n", port_no);
		return 1;
	}
	conf.pvid = vid;

	/* for now, PVID is also the untagged VLAN */
	conf.untagged_vid = vid;
	if (vtss_vlan_port_conf_set(NULL, port_no, &conf) != VTSS_RC_OK) {
		printf("Couldn't set VLAN conf for port %d\n", port_no);
		return 1;
	}

	return 0;
}

/* show members for a Private VLAN */
static void private_vlan_members_show(const int pvid)
{
	int i;
	BOOL member[VTSS_PORT_ARRAY_SIZE], clear;

	if (!is_valid_private_vid(pvid))
		return;

	if (vtss_pvlan_port_members_get(NULL, pvid, member) != VTSS_RC_OK) {
		printf("Could not get members for PVLAN %d\n", pvid);
		return;
	}

	clear = TRUE;
	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++) {
		if (member[i] == FALSE)
			continue;

		if (clear == TRUE) {
			clear = FALSE;
			printf("Private VLAN %d: ", pvid);
		}
		printf("%d, ", i);
	}
	if (clear == FALSE)
		printf("\n");
}

/* show all members for a Private VLAN */
void private_vlan_members_show_all(void)
{
	int i;

	printf("Printing all Private VLANs...\n\n");
	for (i = VTSS_PVLAN_NO_START; i < VTSS_PVLAN_NO_END; i++)
		private_vlan_members_show(i);
}

/* add a member for a Private VLAN */
int private_vlan_member_add(const int port_no, const int pvid)
{
	BOOL member[VTSS_PORT_ARRAY_SIZE];

	if (!is_valid_port(port_no) || !is_valid_private_vid(pvid))
		return -1;

	printf("Adding port %d as member for PVLAN %d\n", port_no, pvid);
	/* add port in Private VLAN's members */
	if (vtss_pvlan_port_members_get(NULL, pvid, member) != VTSS_RC_OK) {
		printf("Could not get members for PVLAN %d\n", pvid);
		return -1;
	}

	member[port_no] = TRUE;
	if (vtss_pvlan_port_members_set(NULL, pvid, member) != VTSS_RC_OK) {
		printf("Could not set members for PVLAN %d\n", pvid);
		return -1;
	}

	/* check members for the current Private VLAN */
	private_vlan_members_show(pvid);

	return 0;
}

/* remove a member from a Private VLAN */
int private_vlan_member_remove(const int port_no, const int pvid)
{
	BOOL member[VTSS_PORT_ARRAY_SIZE];

	if (!is_valid_port(port_no) || !is_valid_private_vid(pvid))
		return -1;

	printf("Removing port %d from PVLAN %d members\n", port_no, pvid);

	/* add port in VLAN's members */
	if (vtss_pvlan_port_members_get(NULL, pvid, member) != VTSS_RC_OK) {
		printf("Could not get members for PVLAN %d\n", pvid);
		return -1;
	}

	member[port_no] = FALSE;
	if (vtss_pvlan_port_members_set(NULL, pvid, member) != VTSS_RC_OK) {
		printf("Could not set members for PVLAN %d\n", pvid);
		return -1;
	}

	/* show members for the current VLAN */
	private_vlan_members_show(pvid);
	return 0;
}

static int is_valid_vce_id(const int vce_id)
{
	if (vce_id < VTSS_VCL_ID_START || vce_id >= VTSS_VCL_ID_END) {
		printf("Please set a VCL ID from [%d - %d]\n",
				VTSS_VCL_ID_START, VTSS_VCL_ID_END - 1);
		return FALSE;
	}
	return TRUE;
}

/* Add a MAC-based VLAN CLassification Entry */
int vce_mac_entry_add(const unsigned int vce_id, u8 addr[MAC_ADDR_COUNT],
		u8 mask[MAC_ADDR_COUNT], const unsigned int classified_vid)
{
	vtss_vce_t vce;
	int i;

	if(!is_valid_vce_id(vce_id))
		return -1;

	if (!is_valid_vid(classified_vid))
		return -1;

	if (vtss_vce_init(NULL, VTSS_VCE_TYPE_ANY, &vce) != VTSS_RC_OK) {
		printf("Couldn't initialize a VCE to default values\n ");
		return -1;
	}
	vce.id = vce_id;
	vce.key.mac.dmac_bc = VTSS_VCAP_BIT_ANY;
	vce.key.mac.dmac_mc = VTSS_VCAP_BIT_ANY;
	memcpy(vce.key.mac.smac.value, addr, sizeof(vce.key.mac.smac.value));
	memcpy(vce.key.mac.smac.mask, mask, sizeof(vce.key.mac.smac.mask));
	vce.key.tag.vid.mask = 0;

	/* Frame must not be tagged */
	vce.key.tag.tagged = VTSS_VCAP_BIT_0;

	/* Entry must apply on all ports */
	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		vce.key.port_list[i] = TRUE;

	/* set CLassified VLAN ID */
	vce.action.vid = classified_vid;

	if (vtss_vce_add(NULL, VTSS_VCE_ID_LAST, &vce) != VTSS_RC_OK)
		printf("Couldn't add VCE entry %u using the Seville API\n",
				vce_id);
	else
		printf("Added MAC-based VCE entry %u\n", vce_id);

	return 0;
}

/* Add a Protocol-based VLAN CLassification Entry */
int vce_proto_entry_add(const unsigned int vce_id, const vtss_vce_type_t  type,
		const unsigned int classified_vid)
{
	vtss_vce_t vce;
	int i;

	if(!is_valid_vce_id(vce_id))
		return -1;

	if (!is_valid_vid(classified_vid))
		return -1;

	if (vtss_vce_init(NULL, type, &vce) != VTSS_RC_OK) {
		printf("Couldn't initialize a VCE to default values\n ");
		return -1;
	}

	vce.id = vce_id;
	vce.key.mac.dmac_bc = VTSS_VCAP_BIT_ANY;
	vce.key.mac.dmac_mc = VTSS_VCAP_BIT_ANY;

	/* Match any SMAC */
	memset(vce.key.mac.smac.mask, 0, sizeof(vce.key.mac.smac.mask));

	/* Match any VID */
	vce.key.tag.vid.mask = 0;

	/* Frame must not be tagged */
	vce.key.tag.tagged = VTSS_VCAP_BIT_0;

	/* Entry must apply on all ports */
	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		vce.key.port_list[i] = TRUE;

	/* set CLassified VLAN ID */
	vce.action.vid = classified_vid;

	switch(type) {
	case VTSS_VCE_TYPE_ANY:
		/* nothing to do */
		break;
	case VTSS_VCE_TYPE_ETYPE:
		memset(vce.key.frame.etype.data.mask, 0,
				sizeof(vce.key.frame.etype.data.mask));
		memset(vce.key.frame.etype.etype.mask, 0,
				sizeof(vce.key.frame.etype.etype.mask));
		break;
	case VTSS_VCE_TYPE_LLC:
		memset(vce.key.frame.llc.data.mask, 0,
				sizeof(vce.key.frame.llc.data.mask));
		break;
	case VTSS_VCE_TYPE_SNAP:
		memset(vce.key.frame.snap.data.mask, 0,
				sizeof(vce.key.frame.snap.data.mask));
		break;
	case VTSS_VCE_TYPE_IPV4:
		memset(&vce.key.frame.ipv4.sip.mask, 0,
				sizeof(vce.key.frame.ipv4.sip.mask));
		memset(&vce.key.frame.ipv4.proto.mask, 0,
				sizeof(vce.key.frame.ipv4.proto.mask));
		vce.key.frame.ipv4.options = VTSS_VCE_TYPE_ANY;
		vce.key.frame.ipv4.fragment = VTSS_VCE_TYPE_ANY;
		break;
	case VTSS_VCE_TYPE_IPV6:
		memset(&vce.key.frame.ipv6.proto.mask, 0,
				sizeof(vce.key.frame.ipv6.proto.mask));
		memset(&vce.key.frame.ipv6.sip.mask, 0,
				sizeof(vce.key.frame.ipv6.sip.mask));
		break;
	default:
		printf("Unknown frame type %d\n", type);
		return -1;
	}

	/* add the VCE entry in IS1 */
	if (vtss_vce_add(NULL, VTSS_VCE_ID_LAST, &vce) != VTSS_RC_OK)
		printf("Couldn't add VCE entry %u using the Seville API\n",
				vce_id);
	else
		printf("Added Protocol-based VCE entry %u\n", vce_id);

	return 0;
}

/* Add a Protocol-based VLAN CLassification Entry based on a known EtherType*/
int vce_proto_ethtype_entry_add(const unsigned int vce_id,
		const u16 ethtype,
		const unsigned int classified_vid)
{
	vtss_vce_t vce;
	int i;

	if(!is_valid_vce_id(vce_id))
		return -1;

	if (!is_valid_vid(classified_vid))
		return -1;

	if (vtss_vce_init(NULL, VTSS_VCE_TYPE_ETYPE, &vce) != VTSS_RC_OK) {
		printf("Couldn't initialize a VCE to default values\n ");
		return -1;
	}

	vce.id = vce_id;
	vce.key.mac.dmac_bc = VTSS_VCAP_BIT_ANY;
	vce.key.mac.dmac_mc = VTSS_VCAP_BIT_ANY;

	/* Match any SMAC */
	memset(vce.key.mac.smac.mask, 0, sizeof(vce.key.mac.smac.mask));

	/* Match any VID */
	vce.key.tag.vid.mask = 0;

	/* Frame must not be tagged */
	vce.key.tag.tagged = VTSS_VCAP_BIT_0;

	/* Entry must apply on all ports */
	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		vce.key.port_list[i] = TRUE;

	/* set CLassified VLAN ID */
	vce.action.vid = classified_vid;

	memset(vce.key.frame.etype.data.mask, 0,
			sizeof(vce.key.frame.etype.data.mask));
	memset(vce.key.frame.etype.etype.mask, 1,
			sizeof(vce.key.frame.etype.etype.mask));
	vce.key.frame.etype.etype.value[0] = (ethtype & 0xFF00) >> 16;
	vce.key.frame.etype.etype.value[1] = ethtype & 0x00FF;

	/* add the VCE entry in IS1 */
	if (vtss_vce_add(NULL, VTSS_VCE_ID_LAST, &vce) != VTSS_RC_OK)
		printf("Couldn't add VCE entry %u using the Seville API\n",
				vce_id);
	else
		printf("Added Protocol-based VCE entry %u\n", vce_id);

	return 0;
}

/* Add a Subnet-based VLAN CLassification Entry */
int vce_subnet_entry_add(const unsigned int vce_id, const u32 ip,
		const u32 mask, const unsigned int classified_vid)
{
	vtss_vce_t vce;
	int i;

	if(!is_valid_vce_id(vce_id))
		return -1;

	if (!is_valid_vid(classified_vid))
		return -1;

	if (vtss_vce_init(NULL, VTSS_VCE_TYPE_IPV4, &vce) != VTSS_RC_OK) {
		printf("Couldn't initialize a VCE to default values\n ");
		return -1;
	}

	vce.id = vce_id;
	vce.key.mac.dmac_bc = VTSS_VCAP_BIT_ANY;
	vce.key.mac.dmac_mc = VTSS_VCAP_BIT_ANY;

	/* Match any SMAC */
	memset(vce.key.mac.smac.mask, 0, sizeof(vce.key.mac.smac.mask));

	/* Match any VID */
	vce.key.tag.vid.mask = 0;

	/* Frame must not be tagged */
	vce.key.tag.tagged = VTSS_VCAP_BIT_0;

	/* Entry must apply on all ports */
	for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
		vce.key.port_list[i] = TRUE;

	/* Match IP Subnet */
	vce.key.frame.ipv4.sip.value = ip;
	vce.key.frame.ipv4.sip.mask = mask;

	/* set CLassified VLAN ID */
	vce.action.vid = classified_vid;

	/* add the VCE entry in IS1 */
	if (vtss_vce_add(NULL, VTSS_VCE_ID_LAST, &vce) != VTSS_RC_OK)
		printf("Couldn't add VCE entry %u using the Seville API\n",
				vce_id);
	else
		printf("Added Protocol-based VCE entry %u\n", vce_id);

	return 0;
}

/* Remove a VLAN CLassification Entry */
int vce_entry_remove(const unsigned int vce_id)
{
	if(!is_valid_vce_id(vce_id))
		return -1;

	if (vtss_vce_del(NULL, vce_id) != VTSS_RC_OK)
		printf("Couldn't remove VCE entry %u using the Seville API\n",
				vce_id);

	printf("Removed VCE entry %u\n", vce_id);

	return 0;
}

/* Add/remove a port for a group used for VLAN translation
 * NOTE: A port may be added in a single group only */
int vlan_trans_group_port_set(int group, int port_no, BOOL add)
{
	int i;
	vtss_vlan_trans_port2grp_conf_t group_conf;

	bzero(&group_conf, sizeof(group_conf));

	if (!is_valid_port(port_no))
		return -1;

	if (VTSS_VLAN_TRANS_VALID_GROUP_CHECK(group) == FALSE)
		return -1;

	if (port_no / 8 >= VTSS_VLAN_TRANS_PORT_BF_SIZE) {
		printf("ERROR: The port number %d is too big\n", port_no);
		return -1;
	}

	group_conf.group_id = group;

	if (vtss_vlan_trans_group_to_port_get(NULL, &group_conf, FALSE) !=
			VTSS_RC_OK) {
		/* might be a new group */
		for (i = 0; i < VTSS_VLAN_TRANS_PORT_BF_SIZE; i++)
			group_conf.ports[i] = 0;
	}

	if (add == TRUE)
		group_conf.ports[port_no / 8] |= (1 << (port_no % 8));
	else
		group_conf.ports[port_no / 8] &= ~(1 << (port_no % 8));

	if (vtss_vlan_trans_group_to_port_set(NULL, &group_conf) !=
			VTSS_RC_OK) {
		printf("Failed to set port list for group %d\n", group);
		return -1;
	}

	return 0;
}

/* Add a map between a VLAN translation and a group */
int vlan_trans_group_vlan_add(const int group, const int vid,
		const int classified_vid)
{
	if (VTSS_VLAN_TRANS_VALID_GROUP_CHECK(group) == FALSE)
		return -1;

	if (!is_valid_vid(vid) || !is_valid_vid(classified_vid))
		return -1;

	if (vtss_vlan_trans_group_add(NULL, (u16)group, (vtss_vid_t)vid,
			(vtss_vid_t)classified_vid) != VTSS_RC_OK) {
		printf("ERROR: Failed to map VLAN %d to group %d\n",
				vid, group);
		return -1;
	}

	return 0;
}

/* Remove a map between a VLAN translation and a group */
int vlan_trans_group_vlan_del(const int group, const int vid)
{
	if (VTSS_VLAN_TRANS_VALID_GROUP_CHECK(group) == FALSE)
		return -1;

	if (!is_valid_vid(vid))
		return -1;

	if (vtss_vlan_trans_group_del(NULL, (u16)group, (vtss_vid_t)vid)
			!= VTSS_RC_OK) {
		printf("ERROR: Failed to remove VLAN %d mapped to group %d\n",
				vid, group);
		return -1;
	}

	return 0;
}

/* Show all VLAN Translation configuration */
void vlan_trans_show(void)
{
	int i, j;
	u16 old_group;
	vtss_vid_t old_vid;
	vtss_rc rc;
	vtss_vlan_trans_grp2vlan_conf_t grp2vlan_conf;
	vtss_vlan_trans_port2grp_conf_t port2grp_conf;

	port2grp_conf.group_id = 0;

	if (vtss_vlan_trans_group_to_port_get(NULL, &port2grp_conf,
			FALSE) != VTSS_RC_OK) {
		printf("No Translation maps between groups and ports\n");
		goto __skip_port2grp;
	}
	printf("Group to port map:\n");
	do {
		old_group = port2grp_conf.group_id;
		printf("Group %d\t Ports: ", port2grp_conf.group_id);
		for (i = 0; i < VTSS_VLAN_TRANS_PORT_BF_SIZE; i++)
			for (j = 0; j < 8; j++)
				if ((port2grp_conf.ports[i] & (1 << j))
						== (1 << j))
					printf("%d, ", i * 8 + j);
		printf("\n");
		rc = vtss_vlan_trans_group_to_port_get(NULL, &port2grp_conf,
					TRUE);
		if (rc == VTSS_RC_OK && port2grp_conf.group_id == old_group) {
			printf("ERROR: Same group %d returned\n", old_group);
			break;
		}
	} while (rc == VTSS_RC_OK);
	printf("\n");
__skip_port2grp:
	grp2vlan_conf.group_id = 0;
	grp2vlan_conf.vid = 0 ;
	if (vtss_vlan_trans_group_get(NULL, &grp2vlan_conf, FALSE) !=
			VTSS_RC_OK) {
		/* no entry has been found */
		printf("No groups created for VLAN Translation\n");
		return;
	}

	printf("Group to VLAN map:\n");
	do {
		old_group = grp2vlan_conf.group_id;
		old_vid = grp2vlan_conf.vid;
		printf("Group %d\t VLAN %d\t translated to VLAN %d\n",
				grp2vlan_conf.group_id, grp2vlan_conf.vid,
				grp2vlan_conf.trans_vid);
		rc = vtss_vlan_trans_group_get(NULL, &grp2vlan_conf, TRUE);
		if (rc == VTSS_RC_OK && old_group == grp2vlan_conf.group_id &&
				old_vid == grp2vlan_conf.vid) {
			printf("ERROR: Same group %d returned\n", old_group);
			break;
		}
	} while (rc == VTSS_RC_OK);

	printf("\n");
}
