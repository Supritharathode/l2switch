/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#ifndef _VLAN_H_
#define _VLAN_H_

#include <fsl_utils/fsl_utils.h>

/* initializing configuration for VLANs */
void vlan_init(void);

/* add/remove/show members for a VLAN */
void vlan_members_show(int vid);
void vlan_members_show_all(void);
int vlan_member_add(int port_no, int vid);
int vlan_member_remove(int port_no, int vid);
int vlan_table_read(int vid);
void vlan_range_add(void);

/* get/set/show the Port-based VLAN of a port */
int vlan_pvid_set(int port_no, int vid);
int vlan_pvid_get(int port_no, int *vid);
void vlan_pvid_show_all(void);
void vlan_pvid_show(int port_no);

/* get/set/show the native VLAN for a port */
int port_vlan_native_set(int port_no, int vid);
int port_vlan_native_get(int port_no, int *vid);
void port_vlan_native_show(int port_no);
void port_vlan_native_show_all(void);

/* get/set/show the VLAN type of a port
 * this is used for both ingress(PVID)
 * and egress(tagging) */
int port_vlan_type_set(int port_no, vtss_vlan_port_type_t type);
int port_vlan_type_get(int port_no, vtss_vlan_port_type_t *type);
void port_vlan_type_show(int port_no);
void port_vlan_type_show_all(void);


/* add/remove/show members for a Private VLAN */
void private_vlan_members_show_all(void);
int private_vlan_member_add(int port_no, int vid);
int private_vlan_member_remove(int port_no, int vid);

/* Add/remove/show VLAN Classification Entries */
int vce_mac_entry_add(unsigned int vce_id, u8 addr[], u8 mask[],
		unsigned int classified_vid);
int vce_proto_entry_add(unsigned int vce_id, vtss_vce_type_t  type,
		unsigned int classified_vid);
int vce_proto_ethtype_entry_add(unsigned int vce_id,
		u16 ethtype,
		unsigned int classified_vid);
int vce_subnet_entry_add(unsigned int vce_id, u32 ip,
		u32 mask, unsigned int classified_vid);
int vce_entry_remove(unsigned int vce_id);

/* VLAN translation */
int vlan_trans_group_port_set(int group, int port_no, BOOL add);
int vlan_trans_group_vlan_add(const int group, const int vid,
		const int classified_vid);
int vlan_trans_group_vlan_del(const int group, const int vid);
void vlan_trans_show(void);

#endif /* _VLAN_H_ */
