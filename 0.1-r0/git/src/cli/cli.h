/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#ifndef _CLI_H_
#define _CLI_H_

#include <fsl_utils/fsl_utils.h>
#include "test_cmd.h"
#include <port_counters/port_counters.h>
#include <vlan/vlan.h>
#include <port_setup/port_setup.h>
#include <port_filters/port_filters.h>
#include <control_traffic/control_traffic.h>
#include <mac/mac.h>
#include <mirror/mirror.h>
#include <multicast/multicast.h>
#include <flow_control/flow_control.h>
#include <port_isolation/port_isolation.h>
#include <qos/qos.h>
#include <lag/lag.h>
#include <acl/acl.h>
#include <board_init/board_init.h>

#define PROMPT_DEFAULT	"l2switch>"
#define PROMPT_LEN_MAX	12

extern char		prompt[PROMPT_LEN_MAX+1];

int process_command(const char *command);

#endif /* _CLI_H_ */

