/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#include "test_cmd.h"
#include <qos/qos.h>
#include <acl/acl.h>

/* Populate this as more tests with hardcoded settings are added*/
struct l2sw_test tests[] = {
	/* {label,	description	, toggle,	handle} */
	{"L2_POLICER", "traffic policing", FALSE, &test_L2_POLICER_cfg},
	{"L2_EGR_SHPR", "egress shaping", FALSE, &test_L2_EGR_SHPR_cfg},
	{"L2_ACL_1", "Disable learning and forwarding, use hit-me-once",
		FALSE, &test_L2_ACL_1},
	{"L2_ACL_2", "ARP frame inspection", FALSE, &test_L2_ACL_2},
	{"L2_ACL_3", "Discard all traffic on port", FALSE, &test_L2_ACL_3},
	{"L2_ACL_4", "TCP SYN policing", FALSE, &test_L2_ACL_4},
	{"L2_ACL_5", "ACL rate limiter", FALSE, &test_L2_ACL_5},
	{"L2_ACL_6", "Drop traffic from a specific SMAC+IP binding", FALSE,
		&test_L2_ACL_6},
	{"L2_ACL_8", "DHCP Snooping", FALSE, &test_L2_ACL_8},
	{"L2_QCL_1", "QoS Classification via QCLs", FALSE, &test_L2_QCL_1},
	{"ping","ping_policing",FALSE,&ping_policing},////// to configure the policer
	{"is2cap","is2_cap_config",FALSE,&populate_values} ////// to configure the is2cap entries
};

/* Hopefully you don't need to edit code below */

/* commands starting with "test"
 *
 * Search in the test array above for a test with a matching label and
 * call its handle. Second call to the test will restore the settings.
 *
 * If no match is found a listing all the available test is printed.
 */
int process_test_command(const char *command)
{
	int incorrect_command = 1;
	int i, tests_no = sizeof(tests)/sizeof(tests[0]);

	if (strlen(command) < 2)
		goto wrong_input;

	if (command[0] == ' ')
		command++;

	for (i = 0; i < tests_no; i++) {
		if (strncmp(command, tests[i].label,
				strlen(tests[i].label)) == 0) {
			command += strlen(tests[i].label);
			incorrect_command = tests[i].handle(tests[i].restore);
			if (!incorrect_command)
				tests[i].restore = !tests[i].restore;
			break;
		}
	}

wrong_input:
	if (incorrect_command) {
		printf("Error: Incorrect command\n");
		printf("Options:\n");
		for (i = 0; i < tests_no; i++)
			printf("\t%s\t- %s\n", tests[i].label, tests[i].desc);
	}

	return incorrect_command ? -1 : 0;
}
