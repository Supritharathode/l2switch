/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#include "cli.h"
#include <sys/types.h>

char	prompt[PROMPT_LEN_MAX+1];

/* command started with "qos " */
static int parse_qos_command(const char *command)
{
	vtss_prio_t qos;
	vtss_dscp_t dscp;
	int aux;
	char *endptr;

	/* get QoS value */
	aux = strtol(command, &endptr, 10);
	if (command == endptr || *endptr != ' ' ||  aux < 0 ||
			aux > 7)
		goto __print_help;
	qos = aux;
	command = endptr;
	command++;

	if (strncmp(command, "map dscp ", strlen("map dscp ")) == 0) {
		command += strlen("map dscp ");

		/* get DSCP value */
		aux = strtol(command, &endptr, 10);
		if (command == endptr || *endptr != '\0' ||  aux < 0 ||
				aux > 64)
			goto __print_help;
		dscp = aux;
		command = endptr;

		if (qos_dscp_map(dscp, qos))
			printf("Cannot set QoS %d to mapped DSCP %d\n",
					qos, dscp);

		return 0;
	}

__print_help:
	printf("qos' commands:\n\tqos <qos_val> map dscp <dscp_val>\n");
	printf("\thelp - prints this message\n\n");

	return -1;
}

/* command started with "dscp " */
static int parse_dscp_command(const char *command)
{
	vtss_dscp_t	dscp;
	unsigned int	dscp_parsed, dscp_clsfd;
	int		aux;
	char		*endptr;
	BOOL		en;

	if (strncmp(command, "translate ", strlen("translate ")) == 0) {
		command += strlen("translate ");

		/* DSCP */
		if (get_next_value(&command, &dscp_parsed))
			return -1;
		if (command[0] != ' ')
			return -1;
		command++;

		/* DSCP */
		if (get_next_value(&command, &dscp_clsfd))
			return -1;
		if (command[0] != '\0')
			return -1;

		if (dscp_translate_map(dscp_parsed, dscp_clsfd))
			printf("Can't create DSCP map\n");
		return 0;
	}

	if (strncmp(command, "remap ", strlen("remap ")) == 0) {
		command += strlen("remap ");

		/* for now we set for both DP values */
		/* DSCP */
		if (get_next_value(&command, &dscp_parsed))
			return -1;
		if (command[0] != ' ')
			return -1;
		command++;

		/* DSCP */
		if (get_next_value(&command, &dscp_clsfd))
			return -1;
		if (command[0] != '\0')
			return -1;

		/* for now only DP 0 or DP unaware */
		if (dscp_remap_set(0, dscp_parsed, dscp_clsfd))
			printf("Can't remap DSCP for DP 0\n");

		return 0;
	}

	if (strncmp(command, "trust ", strlen("trust ")) == 0) {
		command += strlen("trust ");

		/* get DSCP value */
		aux = strtol(command, &endptr, 10);
		if (command == endptr ||
				*endptr != ' ' ||
				aux < 0 ||
				aux > 63)
			goto __print_help;
		dscp = aux;
		command = endptr;
		command++;

		if (strcmp(command, "enable") == 0)
			en = TRUE;
		else if (strcmp(command, "disable") == 0)
			en = FALSE;
		else
			goto __print_help;

		if (dscp_trust_set(dscp, en))
			printf("Cannot trust in dscp %d\n", dscp);

		return 0;
	}

__print_help:
	printf("'dscp' commands:\n");
	printf("\ttranslate <dscp_val> <dscp_val>\t\t");
	printf("- ingress remaping of DSCP\n");
	printf("\tremap <dscp_val> <dscp_val>\t\t");
	printf("- egress remaping of DSCP\n");
	printf("\ttrust <dscp_val> <enable|disable>\t\t");
	printf("- set trust mode for dscp value\n");
	printf("\thelp\t\t\t- print this message\n");

	return 0;
}

/* command started with "port <nr> qos " */
static int parse_port_qos_command(const char *command, int port_no)
{
	BOOL enable;

	if (strcmp(command, "classify dscp enable") == 0)
		enable = TRUE;
	else  if (strcmp(command, "classify dscp disable") == 0)
		enable = FALSE;
	else
		goto __print_help;

	if (port_qos_dscp_set(port_no, enable))
		printf("Cannot enable QoS classification based on DSCP");

	return 0;

__print_help:
	printf("port <ingress_port> qos classify dscp <enable|disable>\n\n");

	return -1;
}

/* command started with "port <nr> pcp " */
static int parse_port_pcp_command(const char *command, int port_no)
{
	vtss_tag_remark_mode_t emode;
	vtss_tagprio_t pcp;
	vtss_prio_t qos;
	vtss_dp_level_t dp;
	int aux;
	char *endptr;

	if (strncmp(command, "egress ", strlen("egress ")) == 0) {
		command += strlen("egress ");

		if (strncmp(command, "default ", strlen("default ")) == 0) {
			command += strlen("default ");
			aux = strtol(command, &endptr, 10);
			if (command == endptr || *endptr != '\0' ||  aux < 0 ||
					aux > 7)
				goto __print_help;
			pcp = aux;
			if (port_pcp_egress_default_set(port_no, pcp)) {
				printf("Can't set default egress pcp value ");
				printf("for port %d\n",
						port_no);
			}

			return 0;
		}

		if (strncmp(command, "map ", strlen("map ")) == 0) {
			command += strlen("map ");

			/* get QoS */
			aux = strtol(command, &endptr, 10);
			if (command == endptr || *endptr != ' ' ||  aux < 0 ||
					aux > 7)
				goto __print_help;
			qos = aux;
			command = endptr;

			/* get DP */
			aux = strtol(command, &endptr, 10);
			if (command == endptr || *endptr != ' ' ||  aux < 0 ||
					aux > 1)
				goto __print_help;
			dp = aux;
			command = endptr;

			/* get PCP */
			aux = strtol(command, &endptr, 10);
			if (command == endptr || *endptr != '\0' ||  aux < 0 ||
					aux > 7)
				goto __print_help;
			pcp = aux;
			command = endptr;

			if (port_pcp_egress_map_set(port_no, qos, dp, pcp))
				printf("Can't map %d and %d to %d\n",
						qos, dp, pcp);
			return 0;
		}

		if (strcmp(command, "remark classified") == 0)
			emode = VTSS_TAG_REMARK_MODE_CLASSIFIED;
		else if (strcmp(command, "remark default") == 0)
			emode = VTSS_TAG_REMARK_MODE_DEFAULT;
		else if (strcmp(command, "remark mapped") == 0)
			emode = VTSS_TAG_REMARK_MODE_MAPPED;
		else
			goto __print_help;

		if (port_pcp_remark_emode_set(port_no, emode))
			printf("Can't set egress mode for pcp remarking\n");

		return 0;
	}

__print_help:
	printf("port <port_nr> pcp egress remark <classified|default|mapped>");
	printf(" - set pcp egress remark mode\n");
	printf("port <port_nr> pcp egress default <pcp_val> - ");
	printf("set the default pcp value to remark egress traffic with\n");
	printf("port <port_nr> pcp egress map <qos> <dp> <pcp> - ");
	printf("map classified qos and dp to a new pcp value\n\n");

	return -1;
}

/* command started with "port <nr> dscp " */
static int parse_port_dscp_command(const char *command, int port_no)
{
	vtss_dscp_emode_t mode;
	vtss_dscp_mode_t imode;
	BOOL enable;

	if (strncmp(command, "egress ", strlen("egress ")) == 0) {
		command += strlen("egress ");
		if (strcmp(command, "remark off") == 0)
			mode = VTSS_DSCP_EMODE_DISABLE;
		else if (strcmp(command, "remark on") == 0)
			mode = VTSS_DSCP_EMODE_REMARK;
		else if (strcmp(command, "remark remap") == 0)
			mode = VTSS_DSCP_EMODE_REMAP;
		else if (strcmp(command, "remark remap dp") == 0)
			mode = VTSS_DSCP_EMODE_REMAP_DPA;
		else {
			printf("\tport <nr> dscp egress remark <on|off|remap|");
			printf("remap dp>\t\t- egress remarking type of DSCP\n");
			printf("\tport <nr> dscp ingress help\t\t\t- print this message\n");

			return 0;
		}

		if (port_dscp_remark_emode_set(port_no, mode))
			printf("Can't set dscp remark egress mode on port %d\n",
					port_no);
		return 0;
	}

	if (strncmp(command, "ingress ", strlen("ingress ")) == 0) {
		command += strlen("ingress ");

		if (strncmp(command, "translate ",
				strlen("translate ")) == 0) {
			command += strlen("translate ");

			if (strcmp(command, "enable") == 0)
				enable = TRUE;
			else if (strcmp(command, "disable") == 0)
				enable = FALSE;
			else {
				printf("\tport <nr> dscp ingress translate ");
				printf("enable|disable");

				return 0;
			}

			if (port_dscp_translate_set(port_no, enable))
				printf("Can't set dscp tranlate on port %d\n",
							port_no);
			return 0;
		}

		if (strcmp(command, "remark off") == 0)
			imode = VTSS_DSCP_MODE_NONE;
		else if (strcmp(command, "remark zero") == 0)
			imode = VTSS_DSCP_MODE_ZERO;
		else if (strcmp(command, "remark selected") == 0)
			imode = VTSS_DSCP_MODE_SEL;
		else if (strcmp(command, "remark all") == 0)
			imode = VTSS_DSCP_MODE_ALL;
		else {
			printf("\tport <nr> dscp ingress remark <off|zero|");
			printf("selected|all>\t\t- ingress remarking type of DSCP\n");
			printf("\tport <nr> dscp ingress help\t\t\t- print this message\n");

			return 0;
		}

		if (port_dscp_remark_imode_set(port_no, imode))
			printf("Can't set dscp remark ingress mode on port %d\n",
					port_no);
		return 0;
	}

	/* unknown command */
	return -1;
}

/* command started with "vlan " */
static int parse_vlan_command(const char *command)
{
	unsigned int port_no, vid, trans_vid, group;
	BOOL add;
	int ret = -1;

	if (strcmp(command, "trans show") == 0) {
		vlan_trans_show();
		return 0;
	}

	if (strcmp(command, "all isolation show") == 0) {
		vlan_isolation_show();
		return 0;
	}

	if (strcmp(command, "show") == 0) {
		vlan_members_show_all();
		return 0;
	}

	if (strcmp(command, "help") == 0) {
		ret = 0;
		goto __vlan_help_out;
	}
	/* VLAN translation commands */
	if (strncmp(command, "group ", strlen("group ")) == 0) {
		command += strlen("group ");

		/* get Translation group */
		if (get_next_value(&command, &group))
			goto __vlan_help_out;

		if (strncmp(command, " add ", strlen(" add ")) == 0) {
			add = TRUE;
			command += strlen(" add ");
		} else if (strncmp(command, " remove ",
				strlen(" remove ")) == 0) {
			add = FALSE;
			command += strlen(" remove ");
		} else
			goto __vlan_help_out;

		/* Add/remove a VLAN translation for a group */
		if (strncmp(command, "trans ", strlen("trans ")) == 0) {
			command += strlen("trans ");

			/* get VLAN */
			if (get_next_value(&command, &vid))
				goto __vlan_help_out;

			if (add == TRUE) {
				if (strncmp(command, " to ", strlen(" to "))
						!= 0)
					goto __vlan_help_out;

				command += strlen(" to ");
				/* get new classified VLAN */
				if (get_next_value(&command, &trans_vid))
					goto __vlan_help_out;

				if (command[0] != '\0')
					goto __vlan_help_out;

				if (vlan_trans_group_vlan_add(group, vid,
						trans_vid) != VTSS_RC_OK)
					printf("ERROR: Failed to add VLAN translation on group %d\n",
							group);
				return 0;
			} else {
				if (command[0] != '\0')
					goto __vlan_help_out;

				if (vlan_trans_group_vlan_del(group, vid)
						!= VTSS_RC_OK)
					printf("ERROR: Failed to remove VLAN translation from group %d\n",
							group);
				return 0;
			}
		}

		if (strncmp(command, "port ", strlen("port ")) == 0) {
			command += strlen("port ");

			/* get port */
			if (get_next_value(&command, &port_no))
				goto __vlan_help_out;

			if (command[0] != '\0')
				goto __vlan_help_out;

			if (vlan_trans_group_port_set(group, port_no, add))
				printf("ERROR: Failed to configure port %d for Translation group %d\n",
						port_no, group);

			return 0;
		}

		goto __vlan_help_out;
	}

	if (!isdigit(command[0]))
		goto __vlan_help_out;

	/* get VLAN */
	if (get_next_value(&command, &vid))
		goto __vlan_help_out;

	/* get port */
	if (strncmp(command, " add ", strlen(" add ")) == 0) {
		command += strlen(" add ");
		if (!isdigit(command[0]))
			goto __vlan_help_out;

		if (get_next_value(&command, &port_no))
			goto __vlan_help_out;
		if (command[0] != '\0')
			goto __vlan_help_out;

		if (vlan_member_add(port_no, vid) != 0)
			printf("Couldn't add port %d to VLAN %d\n",
					port_no, vid);
		return 0;
	}

	if (strncmp(command, " remove ", strlen(" remove ")) == 0) {
		command += strlen(" remove ");
		if (!isdigit(command[0]))
			goto __vlan_help_out;

		if (get_next_value(&command, &port_no))
			goto __vlan_help_out;
		if (command[0] != '\0')
			goto __vlan_help_out;

		if (vlan_member_remove(port_no, vid) != 0)
			printf("Couldn't remove port %d from VLAN %d\n",
					port_no, vid);

		return 0;
	}

	if (strcmp(command, " isolation enable") == 0) {
		vlan_isolation_set(vid, TRUE);
		return 0;
	}

	if (strcmp(command, " isolation disable") == 0) {
		vlan_isolation_set(vid, FALSE);
		return 0;
	}

__vlan_help_out:
	printf("vlan commands:\n");
	printf("vlan show - show all VLANs and their members\n");
	printf("vlan <vid> add/remove <port_nr>\t"
			" - add/remove a port to/from a VLAN\n");
	printf("vlan all isolation show\t"
			" - show all isolated VLANs\n");
	printf("vlan <vid> isolation enable/disable\t"
			" - enable/disable isolation for a VLAN\n");
	printf("vlan trans show\t"
			" - print all configurations for VLAN translation\n");
	printf("vlan group <group_id> add/remove port <port_no>\t"
			" - map a port to a group of VLAN Translations\n");
	printf("vlan group <group_id> add/remove trans <old_vid> to <new_vid>\t"
			" - map a VLAN Translation to a group\n");
	printf("vlan help\t"
			" - print this message\n\n");

	return ret;
}

/* command started with "private vlan " */
static int parse_private_vlan_command(const char *command)
{
	unsigned int port_no, pvid;
	int ret;

	if (strcmp(command, "show") == 0) {
		private_vlan_members_show_all();
		return 0;
	}

	/* get Private VLAN */
	if (get_next_value(&command, &pvid)) {
		ret = -1;
		goto __out;
	}

	/* get port */
	if (strncmp(command, " add ", strlen(" add ")) == 0) {
		command += strlen(" add ");

		if (get_next_value(&command, &port_no)) {
			ret = -1;
			goto __out;
		}
		if (command[0] != '\0') {
			ret = -1;
			goto __out;
		}

		if (private_vlan_member_add(port_no, pvid) != 0)
			printf("Couldn't add port %d to PVLAN %d\n",
					port_no, pvid);
		return 0;
	}

	if (strncmp(command, " remove ", strlen(" remove ")) == 0) {
		command += strlen(" remove ");

		if (get_next_value(&command, &port_no)) {
			ret = -1;
			goto __out;
		}
		if (command[0] != '\0') {
			ret = -1;
			goto __out;
		}

		if (private_vlan_member_remove(port_no, pvid) != 0)
			printf("Couldn't remove port %d from VLAN %d\n",
					port_no, pvid);

		return 0;
	}

	if (strcmp(command, "help") == 0) {
		ret = 0;
		goto __out;
	}

	ret = -1;
__out:
	printf("Commands:\nprivate vlan show\t- show all Private VLANs\n");
	printf("private vlan <private vlan nr> add/remove <port nr>");
	printf("\t- add/remove a port to/from a Private VLAN\n\n");

	return ret;
}

/* command started with "port stats " */
static int parse_port_stats_command(const char *command)
{
	int i, port;

	if (strstr(command, "all") != NULL) {
		command += strlen("all");
		/* Print statistics for all ports */
		for (i = VTSS_PORT_NO_START;
		     i < VTSS_PORT_NO_END; i++)
			port_counters(i, command);
	} else {
		port = atoi(command);
		if ((port >= VTSS_PORT_NO_START) &&
		    (port < VTSS_PORT_NO_END))
			port_counters(port, command);
		else
			printf("Please specify a port\n");
	}
	return 0;
}

/* command started with "port all " */
static int parse_port_all_command(const char *command)
{
	/* print PVID for all ports */
	if (strncmp(command, "vlan show", strlen("vlan show")) == 0) {
		printf("Vlan port-based configuration (port - VID):\n");
		vlan_pvid_show_all();
		return 0;
	}

	/* print ports status, speed and duplexity */
	if (strcmp(command, "config show") == 0) {
		port_config_show_all();
		return 0;
	}

	/* print Flow control configuration for all ports */
	if (strcmp(command, "flow control show") == 0) {
		port_flow_control_show_all();
		return 0;
	}

	/* print VLAN type for all ports */
	if (strcmp(command, "vlan type show") == 0) {
		port_vlan_type_show_all();
		return 0;
	}

	/* print native VLAN for all ports */
	if (strcmp(command, "vlan untagged show") == 0) {
		port_vlan_native_show_all();
		return 0;
	}

	/* print VLAN tag filter for all ports */
	if (strcmp(command, "filter vlan show") == 0) {
		port_vlan_tag_filter_show_all();
		return 0;
	}

	return -1;
}

/* command started with "port <nr_port> pause " */
static int parse_pause_command(const char *command, int port_no)
{
	vtss_port_flow_control_conf_t fc_conf;
	u8 addr[6];

	/* generate PAUSE frames */
	if (strncmp(command, "generate ", strlen("generate ")) == 0) {

		if (port_flow_control_get(port_no, &fc_conf) != 0)
			return -1;

		command += strlen("generate ");
		if (strcmp(command, "enable") == 0) {
			fc_conf.generate = TRUE;
			port_flow_control_set(port_no, &fc_conf);
			return 0;
		}

		if (strcmp(command, "disable") == 0) {
			fc_conf.generate = FALSE;
			port_flow_control_set(port_no, &fc_conf);
			return 0;
		}

		return -1;
	}

	/* obey PAUSE frames */
	if (strncmp(command, "obey ", strlen("obey ")) == 0) {

		if (port_flow_control_get(port_no, &fc_conf) != 0)
			return -1;

		command += strlen("obey ");
		if (strcmp(command, "enable") == 0) {
			fc_conf.obey = TRUE;
			port_flow_control_set(port_no, &fc_conf);
			return 0;
		}
		if (strcmp(command, "disable") == 0) {
			fc_conf.obey = FALSE;
			port_flow_control_set(port_no, &fc_conf);
			return 0;
		}
		return -1;
	}
	/* configure Flow Control based on
	 * autonegotiation results */
	if (strncmp(command, "autoneg ", strlen("autoneg ")) == 0) {
		command += strlen("autoneg ");

		if (strcmp(command, "on") == 0) {
			port_flow_control_autoneg_set(port_no, TRUE);
			return 0;
		}
		if (strcmp(command, "off") == 0) {
			port_flow_control_autoneg_set(port_no, FALSE);
			return 0;
		}
		return -1;
	}

	/* change SMAC of generated PAUSE frames */
	if (strncmp(command, "smac ", strlen("smac ")) == 0) {
		command += strlen("smac ");

		if (parse_mac(command, addr) != MAC_ADDR_COUNT) {
			printf("ERROR:Invalid MAC address\n");
			return -1;
		}

		if (port_flow_control_get(port_no, &fc_conf) != 0) {
			printf("Can't get FC conf for port %d\n", port_no);
			return -1;
		}

		memcpy(fc_conf.smac.addr, addr, sizeof(fc_conf.smac.addr));

		if (port_flow_control_set(port_no, &fc_conf) != 0) {
			printf("Can't set FC conf for port %d\n", port_no);
			return -1;
		}

		return 0;
	}
	return -1;
}

/* command started with "port <nr_port> vlan " */
static int parse_port_vlan_command(const char *command, int port_no)
{
	unsigned int vid;
	int ret;

	if (strcmp(command, "help") == 0) {
		ret = 0;
		goto __out;
	}
	if (strcmp(command, "show") == 0) {
		/* show the PVID for the port */
		vlan_pvid_show(port_no);
		return 0;
	}
	if (strcmp(command, "type show") == 0) {
		/* show the VLAN type for the port */
		port_vlan_type_show(port_no);
		return 0;
	}

	if (strncmp(command, "untagged ", strlen("untagged ")) == 0) {
		/* change the untagged VLAN for a port */
		command += strlen("untagged ");

		if (strcmp(command, "show") == 0) {
			port_vlan_native_show(port_no);
			return 0;
		}

		if (strcmp(command, "all") == 0) {
			if (port_vlan_native_set(port_no, VTSS_VID_ALL))
				printf("Couldn't disable tagging for port %d\n",
						port_no);
			return 0;
		}

		/* get VID */
		if (get_next_value(&command, &vid)) {
			ret = -1;
			goto __out;
		}

		if (port_vlan_native_set(port_no, vid))
			printf("Couldn't set untagged vlan %d for port %d\n",
					vid, port_no);
		return 0;
	}

	/* commnads to change a port's VLAN type */
	if (strncmp(command, "type ", strlen("type ")) == 0) {
		vtss_vlan_port_type_t type;

		command += strlen("type ");

		if (strcmp(command, "unaware") == 0)
			type = VTSS_VLAN_PORT_TYPE_UNAWARE;
		else if (strcmp(command, "C") == 0)
			type = VTSS_VLAN_PORT_TYPE_C;
		else if (strcmp(command, "S") == 0)
			type = VTSS_VLAN_PORT_TYPE_S;
		else if (strcmp(command, "S-custom") == 0)
			type = VTSS_VLAN_PORT_TYPE_S_CUSTOM;
		else
			return -1;

		if (port_vlan_type_set(port_no, type))
			printf("ERROR: Failed to set VLAN type for port %d\n",
					port_no);

		return 0;
	}

	if (!isdigit(command[0])) {
		ret = -1;
		goto __out;
	}

	/* get VID */
	if (get_next_value(&command, &vid)) {
		ret = -1;
		goto __out;
	}

	if (command[0] != ' ' && command[0] != '\0') {
		ret = -1;
		goto __out;
	}

	/* set VID for the requested port */
	if (vlan_pvid_set(port_no, vid) != 0)
		printf("Couldn't set PVID %d for port %d\n", vid, port_no);

	/* print port's pvid */
	vlan_pvid_show(port_no);

	return 0;
__out:
	printf("Commands are:\n");
	printf("port <nr_port>/all vlan show\t - show PVID for a port\n");
	printf("port <nr_port> vlan <vid>\t - set PVID for a port\n");
	printf("port <nr_port>/all vlan untagged show\t"
			" - show native VLAN for a port\n");
	printf("port <nr_port> vlan untagged <vid>/all\t - ");
	printf("set native VLAN for a port; ");
	printf("use \"all\" to always send the frame untagged\n");
	printf("port <nr_port>/all vlan type show\t"
			" - show the VLAN type of a port\n");
	printf("port <port_nr> vlan type <unaware|C|S|S-custom>\t"
			" - set the VLAN type for a port\n");
	printf("port <nr_port> vlan help\t - print this message\n\n");

	return ret;
}

/* command started with "port <nr_port> speed " */
static int parse_port_speed_command(const char *command, int port_no)
{
	unsigned int speed, duplexity;

	if (!isdigit(command[0]))
		return -1;

	if (get_next_value(&command, &speed))
		return -1;

	if (
		speed != 10 &&
		speed != 100 &&
		speed != 1000 &&
		speed != 2500
		) {
		printf("Link speed %d not supported\n", speed);
		return -1;
	}

	if (command[0] == '\0') {
		if (port_config_set(port_no, speed, -1) != 0)
			printf("Can't set config for port %d\n", port_no);
		return 0;
	}

	if (command[0] != ' ')
		return -1;

	command++;

	duplexity = -1;
	if (strcmp(command, "duplex full") == 0)
		duplexity = 1;
	else if (strcmp(command, "duplex half") == 0)
		duplexity = 0;
	else
		return -1;

	/* set config port */
	if (port_config_set(port_no, speed, duplexity) != 0)
		printf("Can't set config for port %d\n", port_no);

	return 0;
}

/* command started with "port <nr_port> filter " */
static int parse_port_filter_command(const char *command, int port_no)
{
	int ret;

	if (strcmp(command, "help") == 0) {
		ret = 0;
		goto __port_filter_help_out;
	}

	if (strncmp(command, "vlan ", strlen("vlan ")) == 0) {
		command += strlen("vlan ");

		if (strcmp(command, "show") == 0) {
			port_vlan_tag_filter_show(port_no);
			return 0;
		}

		if (strncmp(command, "allow ", strlen("allow ")) == 0) {
			vtss_vlan_frame_t allow_frame_tag;
			command += strlen("allow ");

			if (strcmp(command, "all") == 0)
				allow_frame_tag = VTSS_VLAN_FRAME_ALL;
			else if (strcmp(command, "tagged") == 0)
				allow_frame_tag = VTSS_VLAN_FRAME_TAGGED;
			else if (strcmp(command, "untagged") == 0)
				allow_frame_tag = VTSS_VLAN_FRAME_UNTAGGED;
			else {
				ret = -1;
				goto __port_filter_help_out;
			}

			if (port_vlan_tag_filter_set(port_no, allow_frame_tag))
				printf("ERROR: Failed to set filter for port %d",
						port_no);

			return 0;
		}
	}

	ret = -1;

__port_filter_help_out:
	printf("Commands starting with \"port <port_nr> filter \" are\n");
	printf("port <port_nr>/all filter vlan show\t"
			" - show VLAN tag filter for a port\n");
	printf("port <port_nr> filter vlan allow <all | tagged | untagged>\t"
			" - set VLAN tag filter type for a port\n\n");

	return ret;
}

/* command started with "port " followed by a number */
static int parse_port_nr_command(const char *command)
{
	unsigned int port_no = 0;

	if (get_next_value(&command, &port_no))
		return -1;
	if (command[0] != ' ')
		return -1;
	command++;

	/* change port state */
	if (strcmp(command, "state enable") == 0) {
		vtss_port_state_set(NULL, port_no, TRUE);
		return 0;
	}

	if (strcmp(command, "state disable") == 0) {
		vtss_port_state_set(NULL, port_no, FALSE);
		vtss_mac_table_port_flush(NULL, port_no);
		return 0;
	}

	/* flow control configuration */
	if (strncmp(command, "pause ", strlen("pause ")) == 0) {
		command += strlen("pause ");
		return parse_pause_command(command, port_no);
	}

	/* show Flow Control configuration */
	if (strcmp(command, "flow control show") == 0) {
		port_flow_control_show(port_no);
		return 0;
	}

	if (strncmp(command, "vlan ", strlen("vlan ")) == 0) {
		command += strlen("vlan ");
		return parse_port_vlan_command(command, port_no);
	}

	if (strncmp(command, "config ", strlen("config ")) == 0) {
		command += strlen("config ");

		if (strcmp(command, "show") == 0) {
			/* show port status */
			port_config_show(port_no);
			return 0;
		}

		if (strcmp(command, "auto") == 0) {
			/* set autoneg */
			port_autoneg_set(port_no, TRUE);
			return 0;
		}
		return -1;
	}

	if (strncmp(command, "speed ", strlen("speed ")) == 0) {
		command += strlen("speed ");
		return parse_port_speed_command(command, port_no);
	}

	if (strncmp(command, "dscp ", strlen("dscp ")) == 0) {
		command += strlen("dscp ");
		return parse_port_dscp_command(command, port_no);
	}

	if (strncmp(command, "pcp ", strlen("pcp ")) == 0) {
		command += strlen("pcp ");
		return parse_port_pcp_command(command, port_no);
	}

	if (strncmp(command, "qos ", strlen("qos ")) == 0) {
		command += strlen("qos ");
		return parse_port_qos_command(command, port_no);
	}

	if (strncmp(command, "filter ", strlen("filter ")) == 0) {
		command += strlen("filter ");
		return parse_port_filter_command(command, port_no);
	}

	return -1;
}

/* command started with "set " */
static int parse_set_command(const char *command)
{
	int i;

	if (strstr(command, "max_frame_length ") != NULL) {
		unsigned int max_frame_length;
		command += strlen("max_frame_length ");
		max_frame_length = atoi(command);

		/* For now, we change the maximum frame length for all ports */
		for (i = VTSS_PORT_NO_START; i < VTSS_PORT_NO_END; i++)
			if (set_max_frame_length(i, max_frame_length) != 0)
				printf("Failed to set max frame size on port %d\n",
						i);

		return 0;
	} else if (strstr(command, "unicast ") != NULL) {
		command += strlen("unicast ");
		return unicast_command(command);
	} else if (strstr(command, "learn ") != NULL) {
		command += strlen("learn ");
		return learn_command(command);
	} else if (strstr(command, "multicast ") != NULL) {
		command += strlen("multicast ");
		return multicast_command(command);
	} else if (strstr(command, "lag ") != NULL) {
		command += strlen("lag ");
		return lag_command(command);
	} else if (strstr(command, "qos pcp port ") != NULL) {
		command += strlen("qos pcp port ");
		return qos_command_pcp(command);
	} else if (strstr(command, "qos default port ") != NULL) {
		command += strlen("qos default port ");
		return qos_command_default(command);
	} else if (strstr(command, "mirror ") != NULL) {
		command += strlen("mirror ");
		return mirror_command(command);
	} else if (strstr(command, "auth ") != NULL) {
		command += strlen("auth ");
		return auth_command(command);
	}

	return -1; /* Unknown command */
}

/* command started with "isolated member" */
static int parse_isolated_member_command(const char *command)
{
	unsigned int port_no;
	BOOL enable;

	if (strcmp(command, "s show") == 0) {
		isolated_members_show();
		return 0;
	}

	if (strcmp(command, "s clear") == 0) {
		if (isolated_members_clear())
			printf("Can't clear the isolated members list\n");
		return 0;
	}

	if (strncmp(command, " remove ", strlen(" remove ")) == 0) {
		command += strlen(" remove ");
		enable = FALSE;
	} else if (strncmp(command, " add ", strlen(" add ")) == 0) {
		command += strlen(" add ");
		enable = TRUE;
	} else
		return -1;

	if (get_next_value(&command, &port_no))
		return -1;
	if (command[0] != '\0' && command[0] != '\n')
		return -1;

	if (isolated_member_set(port_no, enable))
		printf("Can't set/clear port %d as isolated\n", port_no);

	return 0;
}

/* command started with "storm control " */
static int parse_storm_control_command(const char *command)
{
	vtss_packet_rate_t rate;

	/* show storm control */
	if (strcmp(command, "show") == 0) {
		storm_control_show();
		return 0;
	}

	/* disable storm control */
	if (strcmp(command, "unicast off") == 0) {
		if (storm_control_set(STORM_CONTROL_UC, VTSS_PACKET_RATE_DISABLED))
			printf("Can't disable unicast storm control\n");
		return 0;
	}

	if (strcmp(command, "multicast off") == 0) {
		if (storm_control_set(STORM_CONTROL_MC, VTSS_PACKET_RATE_DISABLED))
			printf("Can't disable multicast storm control\n");
		return 0;
	}

	if (strcmp(command, "broadcast off") == 0) {
		if (storm_control_set(STORM_CONTROL_BC, VTSS_PACKET_RATE_DISABLED))
			printf("Can't disable broadcast storm control\n");
		return 0;
	}

	if (strncmp(command, "unicast ", strlen("unicast ")) == 0) {
		command += strlen("unicast ");

		if (get_next_value(&command, &rate))
			return -1;
		if (command[0] != '\0')
			return -1;

		if (storm_control_set(STORM_CONTROL_UC, rate))
			printf("Can't set uc storm control rate %u\n", rate);
		return 0;
	}

	if (strncmp(command, "multicast ", strlen("multicast ")) == 0) {
		command += strlen("multicast ");

		if (get_next_value(&command, &rate))
			return -1;
		if (command[0] != '\0')
			return -1;

		if (storm_control_set(STORM_CONTROL_MC, rate))
			printf("Can't set mc storm control rate %u\n", rate);
		return 0;
	}

	if (strncmp(command, "broadcast ", strlen("broadcast ")) == 0) {
		command += strlen("broadcast ");

		if (get_next_value(&command, &rate))
			return -1;
		if (command[0] != '\0')
			return -1;

		if (storm_control_set(STORM_CONTROL_BC, rate))
			printf("Can't set bc storm control rate %u\n", rate);
		return 0;
	}

	return -1;
}

/* command started with "vce " */
static int parse_vce_command(const char *command)
{
	int ret;
	unsigned int vce_id, vid, aux;
	u8 oui[OUI_ADDR_COUNT];
	u8 mac[MAC_ADDR_COUNT];
	u8 mac_mask[MAC_ADDR_COUNT];
	u16 ethtype;
	vtss_vce_type_t type;
	u32 ip;
	u32 mask;


	if (strncmp(command, "add ", strlen("add ")) == 0) {
		command += strlen("add ");

		/* Get VCE entry id */
		if (get_next_value(&command, &vce_id)) {
			ret = -1;
			goto __out;
		}
		if (command[0] != ' ') {
			ret = -1;
			goto __out;
		}
		command++;

		/* Get classified VLAN */
		if (get_next_value(&command, &vid)) {
			ret = -1;
			goto __out;
		}
		if (command[0] != ' ') {
			ret = -1;
			goto __out;
		}
		command++;

		if (strncmp(command, "mac ", strlen("mac ")) == 0) {
			command += strlen("mac ");
			if (parse_mac(command, mac) != MAC_ADDR_COUNT) {
				printf("ERROR:Invalid MAC address\n");
				ret = -1;
				goto __out;
			}

			/* Set mask to match the exact MAC address */
			memset(mac_mask, 0b11111111, sizeof(mac_mask));

			/* Add VCE entry */
			if (vce_mac_entry_add(vce_id, mac, mac_mask, vid))
				printf("Couldn't add VCE entry %d\n", vce_id);

			return 0;
		}

		if (strncmp(command, "oui ", strlen("oui ")) == 0) {
			command += strlen("oui ");
			if (parse_oui(command, oui) != OUI_ADDR_COUNT) {
				printf("ERROR:Invalid OUI address\n");
				ret = -1;
				goto __out;
			}

			/* convert OUI to MAC */
			bzero(mac, sizeof(mac));
			memcpy(mac, oui, sizeof(oui));

			/* set MAC address mask to match only OUI */
			bzero(mac_mask, sizeof(mac_mask));
			memset(mac_mask, 0b11111111, sizeof(oui));

			/* Add VCE entry */
			if (vce_mac_entry_add(vce_id, mac, mac_mask, vid))
				printf("Couldn't add VCE entry %d\n", vce_id);

			return 0;
		}

		if (strncmp(command, "proto ", strlen("proto ")) == 0) {
			command += strlen("proto ");
			if (strncmp(command, "0x", strlen("0x")) == 0) {
				if (get_next_value_base(&command, &aux,
						16)) {
					ret = -1;
					goto __out;
				}
				ethtype = (u16)aux;

				if (vce_proto_ethtype_entry_add(vce_id,
						ethtype, vid))
					printf("Couldn't add VCE entry %d\n",
							vce_id);
				return 0;

			}

			if (strcmp(command, "any") == 0)
				type = VTSS_VCE_TYPE_ANY;
			else if (strcmp(command, "ethernet") == 0)
				type = VTSS_VCE_TYPE_ETYPE;
			else if (strcmp(command, "llc") == 0)
				type = VTSS_VCE_TYPE_LLC;
			else if (strcmp(command, "snap") == 0)
				type = VTSS_VCE_TYPE_SNAP;
			else if (strcmp(command, "ipv4") == 0)
				type = VTSS_VCE_TYPE_IPV4;
			else if (strcmp(command, "ipv6") == 0)
				type = VTSS_VCE_TYPE_IPV6;
			else {
				ret = -1;
				goto __out;
			}

			if (vce_proto_entry_add(vce_id, type, vid))
				printf("Couldn't add VCE entry %d\n", vce_id);

			return 0;
		}

		if (strncmp(command, "subnet ", strlen("subnet ")) == 0) {
			command += strlen("subnet ");

			/* get IP address */
			if (get_next_ip_address(&command, &ip)) {
				ret = -1;
				goto __out;
			}
			if (command[0] != ' ') {
				ret = -1;
				goto __out;
			}
			command++;

			/* get IP subnet mask */
			if (get_next_ip_address(&command, &mask)) {
				ret = -1;
				goto __out;
			}
			if (command[0] != 0) {
				ret = -1;
				goto __out;
			}

			if (vce_subnet_entry_add(vce_id, ip, mask, vid))
				printf("Failed to add VCE subnet entry\n");
			return 0;
		}

		ret = -1;
		goto __out;
	}

	if (strncmp(command, "remove ", strlen("remove ")) == 0) {
		command += strlen("remove ");

		/* Get VCE entry id */
		if (get_next_value(&command, &vce_id)) {
			ret = -1;
			goto __out;
		}
		if (command[0] != '\0') {
			ret = -1;
			goto __out;
		}

		if (vce_entry_remove(vce_id))
			printf("Couldn't remove VCE entry %d\n", vce_id);

		return 0;
	}

	if (strcmp(command, "help") == 0) {
		ret = 0;
		goto __out;
	}

	ret = -1;
__out:
	printf("Commands:\n");
	printf("vce help\t- print this message;\n");
	printf("vce add/remove <vce_id> <new_vid> mac <mac_addr>\t- ");
	printf("add/remove a VCE entry that classifys frames with a SMAC to a VID");
	printf("\n");
	printf("vce add/remove <vce_id> <new_vid> oui <oui_addr>\t- ");
	printf("add/remove a VCE entry that classifys frames with a SMAC's OUI to a VID");
	printf("\n");
	printf("vce add/remove <vce_id> <new_vid> proto [");
	printf("any|ethernet|llc|snap|ipv4|ipv6|0xXXXX]\t- ");
	printf("add/remove a VCE entry that classifies all frames with a certain (ether)type to a VID");
	printf("\n");
	printf("vce add/remove <vce_id> <new_vid> subnet <ip_addr> <ip mask>");
	printf(" - add/remove a VCE entry that classifys all IPv4 frames from the same subnet to a VID");
	printf("\n\n");

	return ret;
}

static int list_commands()
{
	printf("Available commands:\n" \
		"\tshow version\t\t- Shows software version\n"
		"\thelp | ?\t\t- List available commands\n\n"
		"\tcontrol traffic\t- Control traffic\n"
		"\tdscp\t\t\t- DSCP remap settings\n"
		"\tisolated member\t\t- isolated port settigns\n"
		"\tmac\t\t\t- MAC address table\n"
		"\tport <port_no>\t\t- Per port settings\n"
		"\tport all [vlan|config|flow control] show\n"
		"\t\t\t\t- show specified settings for ports\n"
		"\tport stats\t\t- Port statistics\n"
		"\tport_npi\t\t- NPI port configuration\n"
		"\tprivate vlan\t\t- Private VLAN\n"
		"\tqos\t\t\t- QoS settings\n\n"
		"\tset lag\t\t\t- LAG settings\n"
		"\tset learn <off|auto> <port>\n"
		"\t\t\t\t- set learning per port\n"
		"\tset max_frame_length <len>\n"
		"\t\t\t\t- set max frame length on all ports\n"
		"\tset mirror\t\t- set mirroring options\n"
		"\tset multicast <mac_address> ports [<port list>]\n"
		"\t\t\t\t- set multicast groups\n"
		"\tset unicast flood_member true port <port>\n"
		"\t\t\t\t- set flood members for unicast packets\n"
		"\tset qos pcp port <port> <pcp> <dei> <classified_QoS>\n"
		"\t\t\t\t- set per port QoS classification on PCP/DEI\n"
		"\tset qos default port <port> <classified_QoS>\n"
		"\t\t\t\t- set default QoS classification per port\n\n"
		"\tstorm control\t\t- Storm control\n"
		"\tvce\t\t\t- VLAN classification\n"
		"\tvlan\t\t\t- VLAN port membership settings\n"
		"\tpoll <on|off>\t\t- enable/disable polling for PHY changes\n"
		"\tcheck\t\t\t- check for dropped or invalid frames; if any,\n"
		"\t\t\t\t  file "DEBUG_LOG_TEMP" is created with details\n"
		"\t\ttable <vid>\t\t read\n"
		"\tprompt [new_prompt]\t- sets a custom application prompt\n\n"
		
		);

	return 0;
}
/* command started with "table " */
static int vlan_table_command(const char *command){
	unsigned int vid = 0;
//printf("11111111111");
	if (get_next_value(&command, &vid))
		return -1;
	if (command[0] != ' ')
		return -1;
	command++;
//printf("2222222222");
//printf("%d",vid);
	if (strncmp(command, "read ", strlen("read ")) == 0) {
		command += strlen("read ");
		//printf("$read$");
		return vlan_table_read(vid);
	}
//printf("3333333333");
	return 0;

}
static int vlan_range(const char *command){
	if (strncmp(command, "add ", strlen("add ")) == 0) {
		command += strlen("add ");
		printf("$read$");
		vlan_range_add();
	}
return 0;
}

int process_command(const char *command)
{
	//printf("%s",command);
	/* parse command */
	if (strncmp(command, "vlan ", strlen("vlan ")) == 0) {
		//printf("00000000000000");
		command += strlen("vlan ");
		return parse_vlan_command(command);
	}

	if (strncmp(command, "private vlan ", strlen("private vlan ")) == 0) {
		command += strlen("private vlan ");
		return parse_private_vlan_command(command);
	}

	if (strncmp(command, "port ", strlen("port ")) == 0) {
		command += strlen("port ");
		/* ----- Port statistics command */
		if (strstr(command, "stats") != NULL) {
			command += strlen("stats ");
			return parse_port_stats_command(command);
		}
		/* ----- End Port statistics command */

		if (strncmp(command, "all ", strlen("all ")) == 0) {
			command += strlen("all ");
			return parse_port_all_command(command);
		}

		if (!isdigit(command[0]))
			return -1;

		return parse_port_nr_command(command);
	}

	/* Set command */
	if (strncmp(command, "set " , strlen("set ")) == 0) {
		command += strlen("set ");
		return parse_set_command(command);
	}

	if (strncmp(command, "reset output", strlen("reset output")) == 0) {
		(void)ftruncate(STDOUT_FILENO, 0);
		(void)lseek(STDOUT_FILENO, 0, SEEK_SET);
		return 0;
	}

	/* NPI port commands */
	if (strncmp(command, "port_npi ", strlen("port_npi ")) == 0) {
		command += strlen("port_npi ");
		return port_npi_command(command);
	}

	/* Control traffic command */
	if (strncmp(command, "control traffic ",
			strlen("control traffic ")) == 0) {
		command += strlen("control traffic ");
		return control_traffic_command(command);
	}

	/* MAC command */
	if (strncmp(command, "mac ", strlen("mac ")) == 0) {
		command += strlen("mac ");
		return mac_command(command);
	}

	/* isolated ports command */
	if (!strncmp(command, "isolated member", strlen("isolated member"))) {
		command += strlen("isolated member");
		return parse_isolated_member_command(command);
	}

	/* storm control commands */
	if (!strncmp(command, "storm control ", strlen("storm control "))) {
		command += strlen("storm control ");
		return parse_storm_control_command(command);
	}

	/* dscp commands */
	if (!strncmp(command, "dscp ", strlen("dscp "))) {
		command += strlen("dscp ");
		return parse_dscp_command(command);
	}

	/* QoS commands */
	if (!strncmp(command, "qos ", strlen("qos "))) {
		command += strlen("qos ");
		return parse_qos_command(command);
	}

	/* VCE entries command */
	if (!strncmp(command, "vce ", strlen("vce "))) {
		command += strlen("vce ");
		return parse_vce_command(command);
	}

	/* test commands */
	if (!strncmp(command, "test", strlen("test"))) {
		command += strlen("test");
		return process_test_command(command);
	}

	if (!strncmp(command, "help", strlen("help")) ||
			!strncmp(command, "?", strlen("?"))) {
		return list_commands();
	}

	if (!strcmp(command, "show version"))
		return show_version_command();

	/* Set prompt */
	if (strncmp(command, "prompt" , strlen("prompt")) == 0) {
		command += strlen("prompt");
		if (strlen(command) > 1) {
			if (command[0] != ' ')
				return -1;
			command++;
			strncpy(prompt, command, PROMPT_LEN_MAX);
		} else
			memset(prompt, '\0', PROMPT_LEN_MAX);
		return 0;
	}
/* Vlan Table read*/
	if (strncmp(command, "table ", strlen("table ")) == 0) {
		command += strlen("table ");
	//printf("00000000000000");
		return vlan_table_command(command);
	}
/*vlan access range filling*/
if (strncmp(command, "range ", strlen("range ")) == 0) {
		command += strlen("range ");
	//printf("00000000000000");
		return vlan_range(command);
	}


	return -1;
}
