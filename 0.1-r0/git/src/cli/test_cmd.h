/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#ifndef _TEST_CMD_H_
#define _TEST_CMD_H_

#include <stdbool.h>

#define LABEL_LEN	32
#define DESC_LEN	64

/** \brief Static test structure */
struct l2sw_test {
	char	label[LABEL_LEN];	/* test command identifier */
	char	desc[DESC_LEN];		/* short description of the test */
	bool	restore;		/* toggle for restoring values */
	int	(*handle)(bool);	/* pointer to test handle	*/
};

int process_test_command(const char *command);

#endif /* _TEST_CMD_H_ */
