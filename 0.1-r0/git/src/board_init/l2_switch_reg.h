/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#ifndef _L2_SWITCH_REG_H_
#define _L2_SWITCH_REG_H_

//#include <utils.h>
#include <fsl_utils/fsl_utils.h>


#define ES0			0
#define IS1			1
#define IS2			2


// IS1
#define IS1_CAP_VERSION		0x850398
#define IS1_ENTRY_WIDTH		0X85039c
#define IS1_ENTRY_CNT		0X8503a
#define IS1_ENTRY_SWCNT		0X8503a4
#define IS1_ENTRY_TG_WIDTH	0X8503a8
#define IS1_ACTION_DEF_CNT	0X8503ac
#define IS1_ACTION_WIDTH	0X8503b0
#define IS1_CNT_WIDTH		0X8503b4

#define IS2_CAP_VERSION		0x860398
#define IS2_ENTRY_WIDTH		0X86039c
#define IS2_ENTRY_CNT		0X8603a0
#define IS2_ENTRY_SWCNT		0X8603a4
#define IS2_ENTRY_TG_WIDTH	0X8603a8
#define IS2_ACTION_DEF_CNT	0X8603ac
#define IS2_ACTION_WIDTH	0X8603b0
#define IS2_CNT_WIDTH		0X8603b4

#define ES0_CAP_VERSION		0x840398
#define ES0_ENTRY_WIDTH		0x84039c
#define ES0_ENTRY_CNT		0x8403a0
#define ES0_ENTRY_SWCNT		0x8403a4
#define ES0_ENTRY_TG_WIDTH	0x8403a8
#define ES0_ACTION_DEF_CNT	0x8403ac
#define ES0_ACTION_WIDTH	0x8403b0
#define ES0_CNT_WIDTH		0x8403b4

// TCAM CONTROL
#define ES0_TCAM_CONTROL_REG	0X8403C0
#define ES0_TCAM_STATUS_REG	0x8403C8
#define ES0_CAP_UPDATE_CTRL 	0x840000
#define ES0_CAP_MOV_INIT	0x840004





#define IS1_TCAM_CONTROL_REG    0X8503C0
#define IS1_TCAM_STATUS_REG     0x8503C8
#define IS1_CAP_UPDATE_CTRL 	0x850000
#define IS1_CAP_MOV_INIT		0x850004


#define IS2_TCAM_CONTROL_REG    0X8603C0
#define IS2_TCAM_STATUS_REG     0x8603C8
#define IS2_CAP_UPDATE_CTRL 	0x860000
#define IS2_CAP_MOV_INIT		0x860004

#define _CAP_UPDATE_CMD_WRITE_	0x00000000
#define _CAP_UPDATE_SHOT_		0x00000004
#define _CAP_UPDATE_CMD_READ_	0x00400000



#define STAT_CNT_CFG_REG		0xA0365C
#define CAP_S1_CFG_REG			0xA8000C

#define CAP_CFG_ENA_S1	        0X00004000 
#define CAP_CFG_ENA_DMAC		0x00003800
#define CAP_IS1_S1_NORMAL		0X00

#define CAP_IS1_CFG_LOOKUP1_REG	0xA80010
#define CAP_IS1_CFG_LOOKUP2_REG	0xA80014
#define CAP_IS1_CFG_LOOKUP3_REG	0xA80018

#define CAP_S2_CFG_REG			0xA8001C
#define CAP_CFG_ENA_S2	        0X00004000 
#define CAP_CFG_ENA_MAC_ETYPE	0x00003FFC


#define CAP_IS1_ENTRY_0         0x850008
#define CAP_IS1_ENTRY_1         0x85000C
#define CAP_IS1_ENTRY_2         0x850010
#define CAP_IS1_ENTRY_3         0x850014
#define CAP_IS1_ENTRY_4         0x850018
#define CAP_IS1_ENTRY_5         0x85001C
#define CAP_IS1_ENTRY_6         0x850020
#define CAP_IS1_MASK_0         0x850108
#define CAP_IS1_MASK_1         0x85010C
#define CAP_IS1_MASK_2         0x850110
#define CAP_IS1_MASK_3         0x850114
#define CAP_IS1_MASK_4         0x850118
#define CAP_IS1_MASK_5         0x85011C
#define CAP_IS1_ACT_0         	0x850208
#define CAP_IS1_TG         	0x850388



#define CAP_IS2_ENTRY_0         0x860008
#define CAP_IS2_ENTRY_1         0x86000C
#define CAP_IS2_ENTRY_2         0x860010
#define CAP_IS2_ENTRY_3         0x860014
#define CAP_IS2_ENTRY_4         0x860018
#define CAP_IS2_ENTRY_5         0x86001C
#define CAP_IS2_ENTRY_6         0x860020
#define CAP_IS2_MASK_0         0x850108
#define CAP_IS2_MASK_1         0x85010C
#define CAP_IS2_MASK_2         0x850110
#define CAP_IS2_MASK_3         0x850114
#define CAP_IS2_MASK_4         0x850118
#define CAP_IS2_MASK_5         	0x85011C
#define CAP_IS2_ACT_0         	0x850208
#define CAP_IS2_ACT_1         	0x85020C
#define CAP_IS2_ACT_2         	0x850210
#define CAP_IS2_ACT_3         	0x850214
#define CAP_IS2_TG         	0x850388

#define _READ_    				0
#define _WRITE_    				1

#define PORT_CFG_0			0xA80070
#define PORT_CFG_1			0xA80170
#define PORT_CFG_2			0xA80270
#define PORT_CFG_3			0xA80370
#define PORT_CFG_4			0xA80470
#define PORT_CFG_5			0xA80570
#define PORT_CFG_6			0xA80670
#define PORT_CFG_7			0xA80770
#define PORT_CFG_8			0xA80870
#define PORT_CFG_9			0xA80970



#define PORT_VLAN_CFG		0xA80000
#define PORT_CFG_ID			0xA80070


#define VLAN_DROP_CFG		0xA80004

//#define PORTID 				

#if 0

static struct REG_ENTRIES reg_ [] = 
{


/* Tested for read and write
	{0xA8B504, 1, 0, 0x7F, 0x40}, // vlanMASK
	{0XA8B50C, 1, 0, 0x03, 0x2000}, // AGEFIL
	{0XA8B510, 1, 0, 0, 0x04, 0x00}, // ANEVENTS
	{0XA8B530, 1, 0, 0x00, 0x05}, // AUTOAGE
	{0XA8B600, 1, 0, 0x7ff, 0x312}, // PGID
	{0XA8B4C0, 1, 0, 0x1fFC, 0x1cFC}, // VLANACCESS
	{0XA8B4C4, 1, 0, 0x0, 0x01}, // VLANTIDX
	{0XA80000, 29, 0x04, 0x00, 0x7}, // port configuration
	{0X8403a8, 1, 0, 0, 0x55}, // pol
	{0X8403ac, 1, 0, 0, 0x55}, // pol
	{0X8403b0, 1, 0, 0, 0x55}, // polL
	{0X8403b4, 1, 0, 0, 1},// pol
*/


/* Tested for read and write
	{0XA8C000, 1, 0, 0x01, 0}, // AGEFIL
	{0XA8C7D8, 1, 0, 0x03, 0x00}, // discard
	{0XA8A000, 1, 0, 0, 0x02}, // pol
	{0XA8A004, 1, 0, 0, 0x02}, // pol
	{0XA8A008, 1, 0, 0x281, 0x289}, // pol
	{0XA8A00C, 1, 0, 0, 0}, // polL
	{0XA8A010, 1, 0, 0, 0},// pol
	{0XA8C280, 10, 0x04, 0, 1}, // Flow COntrolL
	{0XA8C2EC, 1, 	0, 0x20, 1}, // AGEFIL
	{0X900000, 1, 0, 0x3C, 0X3d}, // AGEFIL
	{0X900004, 1, 0, 0, 1}, // AGEFIL
	
*/


//	{0X900000, 1, 0, 0x3C, 0X3d}, // AGEFIL
//	{0X840008, 4, 4, 0, 0x55}, // discarda
//		{0X840000, 1, 0, 0, 0}, // enable update
//		{0X840008, 64, 4, 1, 0Xff}, // polL
//		{0X84000c, 1, 0, 1, 0Xff}, // polL
//		{0X840010, 1, 0, 1, 0XFF}, // polL
//		{0X840014, 1, 0, 1, 0XFF}, // polL


//reg count inc def value
/* Tested RW
	{0X900010, 1, 0, 0x0, 0x11}, // AGEFIL
	{0X900014, 1, 0, 0x10, 0x11}, // discard
	{0X900018, 1, 0, 0x5ee, 0x400}, // pol
	{0X90001c, 1, 0, 0x81000004, 0x00}, // pol
	{0X900020, 1, 0, 0, 1}, // pol
	{0X900024, 1, 0, 0x715, 0x717}, // polL
	{0X900028, 1, 0, 0x43, 0x143},// pol
	{0X900030, 1, 0, 0, 1}, // AGEFIL
	{0X900034, 1, 0, 0, 1}, // AGEFIL
	{0X900038, 1, 0, 0, 0x3F}, // AGEFIL

	
	{0X840000, 1, 0, 0, 0X80000}, // AGEFIL
	{0X840004, 1, 0, 0, 0x05}, // AGEFIL
	{0X840008, 4, 4, 0, 0x55}, // discard
	{0X840108, 1, 0, 0, 0x55}, // pol
	{0X840208, 1, 0, 0, 0x55}, // pol
	{0X840308, 1, 0, 0, 0x55}, // pol
	{0X840388, 1, 0, 0, 0x55}, // polL
	{0X840384, 1, 0, 0, 1},// pol
	{0X840398, 1, 0, 0, 0X80000}, // AGEFIL
	{0X84039c, 1, 0, 0, 0}, // AGEFIL
	{0X8403a0, 4, 4, 0, 0x55}, // discard
	{0X8403a4, 1, 0, 0, 0x55}, // pol
	{0X8403a8, 1, 0, 0, 0x55}, // pol
	{0X8403ac, 1, 0, 0, 0x55}, // pol
	{0X8403b0, 1, 0, 0, 0x55}, // polL
	{0X8403b4, 1, 0, 0, 1},// pol

*/
/*
	{0X840394, 1, 0, 0, 0}, // AGEFIL
	{0X8403c0, 1, 0, 0, 1}, // AGEFIL
	{0Xa03600, 1, 1, 0, 0}, // discard
	{0Xa03630, 1, 0, 0x1a00, 0x3a00}, // pol
	{0Xa0365c, 1, 0, 0x003c, 0x003C}, // pol
d		{0Xa03660, 1, 0, 0, 2}, // pol
	{0Xa03684, 1, 0, 0, 2}, // polL
	{0Xa03688, 1, 0, 0, 0x55},// pol
	{0Xa0368c, 1, 0, 0, 1}, // AGEFIL
	{0Xa03690, 1, 0, 0, 1}, // AGEFIL
	{0Xa03694, 1, 0, 0, 0}, // status
	{0Xa04000, 1, 0, 0, 0x07}, // status
	{0Xa04004, 1, 0, 0, 0}, // status

	{0X8403c8, 1, 0, 0, 0xE0}, // AGEFIL
	{0Xa0376c, 1, 1, 0, 0}, // discard
	{0X810318, 1, 0, 0, 0x0c}, // pol
	{0X810324, 1, 0, 0, 1}, // pol
	{0X810354, 1, 0, 0, 2}, // pol
	{0X81037c, 1, 0, 0x00800000, 0x00900000}, // polL
	{0X81047c, 1, 0, 0x3ff, 0x3f0},// pol
	{0X8104a8, 1, 0, 0x3ff, 0x3f0}, // AGEFIL
	{0X8104ac, 1, 0, 0x04700000, 0x8700000}, // AGEFIL
	{0X810000, 1, 0, 0, 0}, // status
*/

/*		{0X840000, 1, 0, 0, 0X80000}, // AGEFIL
	{0X840004, 1, 0, 0, 0x05}, // AGEFIL
	{0X840008, 4, 4, 0, 0x55}, // discard
	{0X840108, 1, 0, 0, 0x55}, // pol
	{0X840208, 1, 0, 0, 0x55}, // pol
	{0X840308, 1, 0, 0, 0x55}, // pol
	{0X840388, 1, 0, 0, 0x55}, // polL
	{0X840384, 1, 0, 0, 1},// pol
*/




/*		// IS1 const registers
	{0X850398, 1, 0, 0, 0}, // AGEFIL
	{0X85039c, 1, 0, 0, 0}, // AGEFIL
	{0X8503a0, 4, 4, 0, 0}, // discard
	{0X8503a4, 1, 0, 0, 0}, // pol
	{0X8503a8, 1, 0, 0, 0}, // pol
	{0X8503ac, 1, 0, 0, 0}, // pol
	{0X8503b0, 1, 0, 0, 0}, // polL
	{0X8503b4, 1, 0, 0, 0},// pol

	// IS2 const regi9sters
	{0X860398, 1, 0, 0, 0}, // AGEFIL
	{0X86039c, 1, 0, 0, 0}, // AGEFIL
	{0X8603a0, 4, 4, 0, 0}, // discard
	{0X8603a4, 1, 0, 0, 0}, // pol
	{0X8603a8, 1, 0, 0, 0}, // pol
	{0X8603ac, 1, 0, 0, 0}, // pol
	{0X8603b0, 1, 0, 0, 0}, // polL
	{0X8603b4, 1, 0, 0, 0},// pol

	// ES0 const registers

	{0X840398, 1, 0, 0, 0X80000}, // AGEFIL
	{0X84039c, 1, 0, 0, 0}, // AGEFIL
	{0X8403a0, 4, 4, 0, 0x55}, // discard
	{0X8403a4, 1, 0, 0, 0x55}, // pol
	{0X8403a8, 1, 0, 0, 0x55}, // pol
	{0X8403ac, 1, 0, 0, 0x55}, // pol
	{0X8403b0, 1, 0, 0, 0x55}, // polL
	{0X8403b4, 1, 0, 0, 1},// pol

*/

	{0xA0365C, 1, 0, 0, 0x00}, // Statistics Confgiuration


	{0xA8000C, 1, 0, 0, 0X00007800}, // enable IS1 lookup S1_ENA, S1_DMAC_DIP_ENA
	{0xA80010, 3, 4, 0,  0x00000000}, // S1_NORMAL key

	// F0F0F0F0 F0F0F0F0  F0F0F0F0  F0F0F0F0 F0F0F0F0 F0F0F0F0 
	//{0x850008, 12, 4, 0,  0xf0f0f0f0}, // Entry Data Cache id0 to id12

/*	// MAC b8:ac:6f:78
 	// Entry  DAta id0 to id12
	{0x850008, 2, 4, 0,  0x00},  // R0 R1
	{0x850010, 1, 0, 0,  0x0D800000}, // R2
	{0x850014, 1, 0, 0,  0x5637bc11}, // R3
	{0x850018, 1, 0, 0,  0x0000005c}, // R4
	{0x85001c, 7, 4, 0,  0x00000000}, // R5-11
*/
	
 	// Entry  DAta id0 to id12 IS1
	// MAC c8:9c:dc:a3:87:32  
	{0x850008, 2, 4, 0,  0x00},  // R0 R1
	{0x850010, 1, 0, 0,  0x99000000}, // R2
	{0x850014, 1, 0, 0,  0x4e6e51c3}, // R3
	{0x850018, 1, 0, 0,  0x00000064}, // R4
	{0x85001c, 7, 4, 0,  0x00000000}, // R5-11
	
	
	// MAC b8:ac:6f:78 MATCH ONLY DMAC
	// Match exact - 0, match any - 1
 	// Entry  Mask id0 to id12
	{0x850108, 2, 4, 0,  0xFFFFFFFF},  // R0 R1  //Match any
	{0x850110, 1, 0, 0,  0x007FFFFF}, // R2 23 bit match any last 9 match exact
	{0x850114, 1, 0, 0,  0x00000000}, // R3
	{0x850118, 1, 0, 0,  0xFFFFFF80}, // R4 // 7 bit match exact, remaining match any
	{0x85011c, 7, 4, 0,  0xFFFFFFFF}, // R5-11
	


	{0x850208, 5, 4, 0,  0x00}, // Action cache
	{0x850388, 1, 0, 0, 0x0000000A} // Type Group - s1_normal 2 bits for each 1010
		
	
};

#endif

#endif /* _L2_SWITCH_REG_H_ */
