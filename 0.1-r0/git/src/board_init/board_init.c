/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

#include "board_init.h"
#include <fsl_utils/fsl_utils.h>
#include "l2_switch_reg.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>


#define MMAP_SIZE   0x290000
#define CHIPID_OFF (0x070000 >> 2)
#define ENDIAN_OFF (0x000000)

#define HWSWAP_LE 0x00000000       /* Little-endian */

int	iodev_fd;	/* iodev file descriptor */
//we are programming 12 registers of 32 bit each beacuse an entry needs 384 bits

static void board_io_init(void)
{
	u32		value;
	const int	enable = 1;
	char		iodev_path[64];	/* iodev path */

	if (!uio_device_exists(iodev_path, sizeof(iodev_path))) {
		perror("Unable to locate UIO device\n");
		exit(1);
	}

	/* Open the UIO device file */
	fprintf(stderr, "Using UIO: %s\n", iodev_path);
	iodev_fd = open(iodev_path, O_RDWR);
	if (iodev_fd < 1) {
		perror(iodev_path);
		exit(1);
	}

	/* mmap the UIO device */
	base_mem = mmap(NULL,
			MMAP_SIZE,
			PROT_READ|PROT_WRITE,
			MAP_SHARED,
			iodev_fd,
			0);

	if (base_mem != MAP_FAILED) {
		fprintf(stderr, "Mapped register memory @ %p\n", base_mem);

		l2sw_reg_write(0, ENDIAN_OFF, HWSWAP_LE);

		l2sw_reg_read(0, CHIPID_OFF, &value);
		fprintf(stderr, "Chipid: %08x\n", value);

		value = (value >> 12) & 0xffff;
		if (value != VTSS_TARGET_SEVILLE) {
			fprintf(stderr, "Unexpected Chip ID, expected 0x%08x\n",
					VTSS_TARGET_SEVILLE);
			exit(1);
		}
	} else
		perror("mmap");

	if (write(iodev_fd, &enable, sizeof(enable)) != sizeof(enable))
		perror("write() failed, unable to enable IRQs");

	phy_init();
}

/* Board port map */
static vtss_port_map_t port_map[VTSS_PORT_ARRAY_SIZE];

/* ================================================================= *
 *  Board init.
 * ================================================================= */

int board_init(void)
{
	vtss_port_no_t		port_no;
	vtss_port_map_t		*map;
	vtss_init_conf_t	init_conf;

	board_io_init();

	vtss_init_conf_get(NULL, &init_conf);
	init_conf.reg_read = l2sw_reg_read;
	init_conf.reg_write = l2sw_reg_write;
	init_conf.miim_read = l2sw_miim_read;
	init_conf.miim_write = l2sw_miim_write;

	if (vtss_init_conf_set(NULL, &init_conf) == VTSS_RC_ERROR)
	{
		perror("Could not initialize");
		exit(1);
	}

	/* Setup port map */
	if (vtss_port_map_get(NULL, port_map) != VTSS_RC_OK)
		printf("ERROR: Failed to get port map\n");

	for (port_no = VTSS_PORT_NO_START;
			port_no < VTSS_PORT_NO_END;
			port_no++) {
		map = &port_map[port_no];

		if (port_no < 10)
			map->chip_port = port_no;

		map->miim_controller = VTSS_MIIM_CONTROLLER_NONE;
	}

	vtss_port_map_set(NULL, port_map);

	/* Enable forwarding on internal ports */
	for (port_no = VTSS_PORT_NO_START;
			port_no < VTSS_PORT_NO_END;
			port_no++) {
		if (is_internal_port(port_map[port_no].chip_port))
			vtss_port_state_set(NULL, port_no, TRUE);
	}

	/* Reset the PHYs for the external ports */
	/* For QSGMII only the first PHY has to be reset */
	if (is_valid_port(0))
		vtss_phy_post_reset(NULL, 0);
	if (is_valid_port(4))
		vtss_phy_post_reset(NULL, 4);

	vtss_irq_enable(NULL, VTSS_IRQ_XTR, TRUE);

	return 0;
}
