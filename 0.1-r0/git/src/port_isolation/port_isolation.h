/*
# ***********************< BEGIN COPYRIGHT >************************
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#
#
# ***********************< END COPYRIGHT >**************************
*/

/*
 * port_isolation.h
 */

#ifndef PORT_ISOLATION_H_
#define PORT_ISOLATION_H_

#include <fsl_utils/fsl_utils.h>

/* a frame cannot be forwarded to other isolated ports
 * only if the ingress port is an isolated member and
 * it's classfied VID is an isolated VLAN.
 * By default, there are no isolated ports or VLANs
 */

/* (un)set isolation for a VLAN */
int vlan_isolation_set(int vid, BOOL isolate);
int vlan_isolation_get(int vid, BOOL *isolate);
void vlan_isolation_show(void);

/* add/remove/clear isolated members function */
int isolated_member_set(int port_no, BOOL enable);
int isolated_members_get(BOOL member[VTSS_PORT_NO_END]);
int isolated_members_clear(void);
void isolated_members_show(void);

#endif /* PORT_ISOLATION_H_ */
