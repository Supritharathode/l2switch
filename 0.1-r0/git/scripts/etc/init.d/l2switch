#!/bin/sh
#
# l2switch        Control T1040 L2 Switch user space driver
#
#   Copyright 2014, Freescale Semiconductor, Inc.  All Rights Reserved.
#
#    NOTICE: The information contained in this file is proprietary
#    to Freescale Semiconductor and is being made available to
#    Freescale's customers under a specific license agreement.
#    Use or disclosure of this information is permissible only
#    under the terms of the license agreement.
#

# chkconfig: 2345 10 90
### BEGIN INIT INFO
# Provides:          l2switch
# Required-Start:    networking
# Short-Description: Control T1040 L2 Switch user space driver
#
### END INIT INFO

# Load L2 Switch env variables
#===================================================================
L2SW_SYSCONFIG=/etc/sysconfig/l2switch
if [ -f $L2SW_SYSCONFIG ];
then
	. $L2SW_SYSCONFIG
else
	echo "Missing ${L2SW_SYSCONFIG}."
	exit 1
fi

if [ ! -w $L2SW_USDRV_LOG ]
then
	touch $L2SW_USDRV_LOG
	echo "Created $L2SW_USDRV_LOG"
fi

#Helper functions
#===================================================================
#
# Wait with timeout for specified application to terminate
#
function wait_appl_term()
{
  local timeout=10
  while [ $(ps -e | grep $1 | wc -l) -ne 0 ] && [ $timeout -gt 0 ]
  do
	sleep 0.5;
	((timeout -= 1))
  done

  if [ $timeout -eq 0 ]
  then
	echo "$1 cannot be stopped. End it manually."
	exit 1
  fi
}

#
# Perform application start
#
function do_start()
{
	# Check if it is running
	if [ $(ps -e | grep $L2SW_USDRV | wc -l) -ne 0 ]
	then
		logger -s -t "l2switch" "Already running. Use 'restart' to restart the switch."
		exit 1
	fi

	# Cleanup
	if [ -p $L2SW_USDRV_IN ]
	then
		rm -rf $L2SW_USDRV_IN
	fi

	if [ -f $L2SW_USDRV_OUT ]
	then
		rm -rf $L2SW_USDRV_OUT
	fi

	# Start the user space driver
	$L2SW_USDRV -i $L2SW_USDRV_IN -o $L2SW_USDRV_OUT >>$L2SW_USDRV_LOG 2>&1 &
	if [ $(ps -e | grep $L2SW_USDRV | wc -l) -lt 1 ]
	then
		logger -s -t "l2switch" "Error: $L2SW_USDRV not running"
		exit 1
	fi

	echo 'prompt' > $L2SW_USDRV_IN
	echo 'reset output' > $L2SW_USDRV_IN

	# Show version when starting
	echo 'show version' > $L2SW_USDRV_IN
	sleep 0.5
	VERSION=`cat $L2SW_USDRV_OUT`
	logger -s -t "l2switch" "Started" "$VERSION"
	echo 'reset output' > $L2SW_USDRV_IN
}

#
# Perform application stop
#
function do_stop()
{
	# Check it is running
	if [ $(ps -e | grep $L2SW_USDRV | wc -l) -ne 1 ]
	then
		logger -s -t "l2switch" "Not running. Nothing to do."
		exit 1
	fi

	# Send exit command
	echo "exit" > $L2SW_USDRV_IN

	wait_appl_term $L2SW_USDRV
	logger -s -t "l2switch" "Stopped"
}

# Perform the commands
#===================================================================

# Check that the user space driver is running
MODULE=$(lsmod | grep ${L2SW_MODULE_NAME} | wc -l)
if [ $MODULE -ne 1 ]
then
	echo $"L2 Switch module <${L2SW_MODULE_NAME}> not inserted."
	#TODO: insert module
	exit 1
fi

# Handle the calls
case "$1" in
  start)
	do_start
	;;
  stop)
	do_stop
	;;
  status)
	echo -n "   $L2SW_USDRV is "
	case "$(ps -e | grep $L2SW_USDRV | wc -l)" in
	  0) echo "stopped" ;;
	  1) echo "running" ;;
	  *) echo "running multiple times" ;;
	esac
	;;
  restart|reload)
	$0 stop
	$0 start
	;;
  *)
	echo $"Usage: $0 {start|stop|restart|reload|status}"
	exit 1
esac

exit 0
