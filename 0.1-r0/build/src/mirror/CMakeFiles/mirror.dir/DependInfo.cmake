# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/src/mirror/mirror.c" "/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/mirror/CMakeFiles/mirror.dir/mirror.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/include/vtss_api"
  "/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/include"
  "/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/utils"
  "/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/src"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
