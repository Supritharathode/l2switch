# Install script for directory: /home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/git/src

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "0")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/acl/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/board_init/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/cli/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/control_traffic/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/flow_control/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/fsl_utils/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/lag/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/mac/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/mirror/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/multicast/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/port_counters/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/port_filters/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/port_isolation/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/port_setup/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/qos/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/vlan/cmake_install.cmake")
  INCLUDE("/home/sirius/yocto/poky/build_t1040rdb_release/tmp/work/ppce5500-fsl-linux/l2switch/0.1-r0/build/src/main/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

